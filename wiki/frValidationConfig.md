# frValidationConfig
*service to configure validators*

## SetValidator
Allows you to create a custom validator. You can use this validator in your form configuration

## Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| type            | String (required)    | This is the validator type(name). Use this **type** to set a validator inside the field configuration.                                    |
| expression      | Function (required)  | This is the validator function. Always return a `promise`.                                                                                |
| options         | Object               | Use the **options** to receive validator options created by the form owner. You can find the options inside the `config` argument.        |
| defaultMessage  | String               | Set a default error message. show this message when there is no errorMessage provided inside the validator configuration.                 |                                                                             

## Example
```javascript
frValidationProviderObject.setValidator({
    type: 'customValidator',
    expression: function customValidator(fieldValue, config) {

        // get the custom object inside the options.
        console.log(config.options.customObject);

        var deferred = $q.defer();
        if (fieldValue !== '') {
            $timeout(function() {
                deferred.resolve(config);
            }, 100)
        } else {
            deferred.reject(config);
        }
        return deferred.promise
    },
    options: {
        customObject: {
            pattern: '/^[1-9]|[1-9][0-9]+$/'
        }
    },
    defaultMessage: 'test'
});
```

## Form configuration
```
{
   "formId": "formId",
   "info": {
      "title": "Form title"
   },
   "name": "Form name",
   "rendererVersion": 1,
   "canSaveDraft": false,
   "fields": [
      {
         "name": "myCustomRadio",
         "spec": {
            "attributes": {
               "type": "customRadio",
               "value": ""
            },
            "options": {
               "layout": {
                  "fieldLayout": "medium"
               }
            },
            "state": {
               "editable": true,
               "editMode": true,
               "disabled": false,
               "extraInfoCollapsed": true
            },
            "inputFilter": {
               "validators": [
                    {
                        "name": "validatorName",
                        "type": "customValidator",
                        "options": {
                            "customObject": {
                                "pattern": "/^[1-9]|[1-9][0-9]+$/"
                            }
                        },
                        "errorMessage": "My custom error message"
                    }
                ]
            },
            "prerequisites": {}
         }
      }
   ]
}
```

# Build in validators

## Validators list

| Type            | Description                                                                                                                                 |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| required        | Use this validator for a required field.                                                                                                  |
| regexp          | Use this validator if you want to check if a field value match a given pattern.                                                           |
| length          | Use this validator to check if the length from a value is greater than a minimum length and less then the maximum length.                 |
| number          | Use this validator to check if the value is a number                                                                                      |
| url             | Use this validator to check if the value is a valid url.                                                                                  |
| email           | The field under validation must a valid e-mail address.                                                                                   |
| between         | The field under validation must have a number between the given *min* and *max*.                                                          |
| date            | Compare the field under validation with other dates.                                                                                      |



## Required

The field under validation must be present in the input data and not empty. A field is considered *empty* if one of the following conditions are true.

- The value is null
- The value is an empty String
- The value is an empty array
- The value is a boolean set to false


### Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name            | String               | Choose a name for the validator.                                                                                                          |
| type            | String (required)    | The type must have the value **required**. If you fill in a type that does not exist the validator will not work.                         |                                                                 | errorMessage    | String               | The error message visible to the user when the input data does not match with the validator.                                              |


#### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "requiredField",
                    "type": "required",
                    "errorMessage": "Please enter a value"
                }
            ]
        }
    }

```

## Regexp

The field under validation must match the given regular expression.


### Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name            | String               | Choose a name for the validator.                                                                                                          |
| type            | String (required)    | The type must have the value **regexp**. If you fill in a type that does not exist the validator will not work.                           |
| options         | Object (required)    | The options object must contain the property **pattern**.                                                                                 |
| errorMessage    | String               | The error message visible to the user when the input data does not match with the validator.                                              |


### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "email",
                    "type": "regexp",
                    "options": {
                        "pattern": "/\S+@\S+\.\S+/"
                    },
                    "errorMessage": "Please enter a valid e-mail address"
                }
            ]
        }
    }

```

## Length

The field under validation must have a value with a length less then ore equal to a max value and more then or equal to a min value.

- The min value is 0 when not defined
- the max value is set to POSITIVE_INFINITY when not defined.

### Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name            | String               | Choose a name for the validator.                                                                                                          |
| type            | String (required)    | The type must have the value **length**. If you fill in a type that does not exist the validator will not work.                           |
| options         | Object (required)    | The options object must contain the properties **min** or **max**.                                                                        |
| errorMessage    | String               | The error message visible to the user when the input data does not match with the validator.                                              |


### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "PasswordMinLength",
                    "type": "length",
                    "options": {
                        "min": 5,
                        "max": 10
                    },
                    "errorMessage": "A password must be at least five characters long."
                }
            ]
        }
    }

```

## Number

The field under validation must be a number. The validator will be fulfilled when the input data is empty.

### Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name            | String               | Choose a name for the validator.                                                                                                          |
| type            | String (required)    | The type must have the value **number**. If you fill in a type that does not exist the validator will not work.                           |
| errorMessage    | String               | The error message visible to the user when the input data does not match with the validator.                                              |

### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "checkNumber",
                    "type": "number",
                    "errorMessage": "Please enter a number."
                }
            ]
        }
    }

```

## url

The field under validation must be a url. A field has a valid *url* if one of the following conditions are true.

- A url contains http://www., https://www. or ftp://www.
- A url has a valid Top-level domain like .be

### Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name            | String               | Choose a name for the validator.                                                                                                          |
| type            | String (required)    | The type must have the value **urlr**. If you fill in a type that does not exist the validator will not work.                             |
| errorMessage    | String               | The error message visible to the user when the input data does not match with the validator.                                              |

### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "checkUrl",
                    "type": "url",
                    "errorMessage": "Please enter a valid url."
                }
            ]
        }
    }

```

## E-mail

The field under validation must be a valid e-mail. This is the regular expression used by this validator : */\S+@\S+\.\S+/*

### Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name            | String               | Choose a name for the validator.                                                                                                          |
| type            | String (required)    | The type must have the value **email**. If you fill in a type that does not exist the validator will not work.                             |
| errorMessage    | String               | The error message visible to the user when the input data does not match with the validator.                                              |

### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "checkemail",
                    "type": "email",
                    "errorMessage": "Please enter a valid e-mail address."
                }
            ]
        }
    }

```

## Between

The field under validation must have a size between the given min and max.

- The min value is 0 when not defined
- the max value is set to POSITIVE_INFINITY when not defined.

### Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name            | String               | Choose a name for the validator.                                                                                                          |
| type            | String (required)    | The type must have the value **between**. If you fill in a type that does not exist the validator will not work.                           |
| options         | Object (required)    | The options object must contain the properties **min** or **max**.                                                                        |
| errorMessage    | String               | The error message visible to the user when the input data does not match with the validator.                                              |

### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "checkBetween",
                    "type": "between",
                    "options": {
                        "min": 3,
                        "max": 5
                    }
                    "errorMessage": "Please enter a value between 3 and 5."
                }
            ]
        }
    }

```

## Date

What can you do with a date validator

- compare date strings with each other
- check if the input date preceding the given date
- check if the input date comes after a given date

### Options

| Name            | Type                 | Description                                                                                                                               |
|-----------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| name            | String               | Choose a name for the validator.                                                                                                          |
| type            | String (required)    | The type must have the value **date**. If you fill in a type that does not exist the validator will not work.                             |
| options         | Object (required)    | The options object must contain the properties **compare**, **before** or **after**.                                                      |
| errorMessage    | String               | The error message visible to the user when the input data does not match with the validator.                                              |

### Compare Dates

Compare a given data + 20 days  with the value from the field under validation.

#### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "compareDate",
                    "type": "date",
                    "options": {
                        "compare": {
                            "date": "1/06/1988",
                            "operator": ">",
                            "addDays": 20
                        }
                    }
                    "errorMessage": "Please enter a valid date"
                }
            ]
        }
    }
```

### Before

The field under validation must be a value preceding the given date

#### Example
```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "compareDate",
                    "type": "date",
                    "options": {
                        "before": '15/06/1988'
                    }
                    "errorMessage": "Please enter a date before 15/06/1988"
                }
            ]
        }
    }
```


### After

The field under validation must be a value after a given date

#### Example

```
    {
        ...
        "inputFilter": {
               "validators": [
                {
                    "name": "compareDate",
                    "type": "date",
                    "options": {
                        "after": '15/06/1988'
                    }
                    "errorMessage": "Please enter a date after 15/06/1988"
                }
            ]
        }
    }
```
