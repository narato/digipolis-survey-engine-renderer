# Form Directives

## fr-form-(viewer)

### Options:

| Name        | Type                 | Description                                                                                            |
|-------------|----------------------|--------------------------------------------------------------------------------------------------------|
| debug       | Bool (default false) | Render the form in debug mode. A debug box with a lot of information about the form should be visible. |
| active      | Bool (Default true)  | Set the form in an active or non active state.                                                         |
| activeStep  | string               | Use the `step id` to render the form in a specific step.                                               |
| schema      | Object (required)    | The form schema.                                                                                       |
| on-submit   | Function             | On submit hook.                                                                                        |
| on-navigate | Function             | On navigate hook.

### Example

```html
<fr-form
	active="true"
	activeStep="form.activeStep"
	schema= "form.schema",
	on-submit="form.onSubmit"
	on-navigate: "form.onNavigate"
	debug="true"
>
</fr-form>

<fr-form-viewer
	schema: "form.schema",
	debug="true"
>
</fr-form>
```

### Resolve promise after navigate an submit hook


| Name       | Type                 | Description                                                                                          |
|------------|----------------------|------------------------------------------------------------------------------------------------------|
| model      | Bool (default false) | A form model copy (you can send this object to the server).                                          |
| activeStep | String               | step id                                                                                              |
| isValid    | Bool                 | True if the form is valid.                                                                           |
| promise    | Object               | You have to resolve or either reject the promise. This way the formrenderer knows that you are done. |


```javascript
$scope.onSubmit = function onSubmit(modelCopy, isValid, promise) {
	promise.resolve();
};
```

```javascript
form.onNavigate = function (model, activeStep, isValid, promise) {
	if (isValidForm) {
		promise.resolve();
	} else {
		promise.reject();
    }
};
```


## fr-form-step-(viewer)

| Name         | Type   | Description |
|--------------|--------|-------------|
| fr-form-step | Object | A step      |
| model        | Object | Form model. |


```html
 <div fr-form-step="getStep(activeStep)"
	 model="model">
 </div>

 <div fr-form-step-viewer="getStep(activeStep)"
	 model="model">
 </div>
```


## fr-form-section-(viewer)

| Name            | Type   | Description |
|-----------------|--------|-------------|
| fr-form-section | Object | A section   |
| model           | Object | Form model. |


```html
 <div fr-form-section="form.section"
	 model="model">
 </div>

 <div fr-form-section-viewer="form.section"
	 model="model">
 </div>
```


## fr-field-(viewer)

| Name     | Type   | Description |
|----------|--------|-------------|
| ng-model | Object | String      |
| options  | Object | A field     |

```html
 <div
 	fr-field
    ng-model="model[field.name]"
    options="field">
  </div>

   <div
 	fr-field-viewer
    ng-model="model[field.name]"
    options="field">
  </div>
```

## fr-validation

| Name     | Type   | Description |
|----------|--------|-------------|
| validation | Object | field validation object      |
| options  | Object | A field     |
| ngModel  | Object | Field model     |

```html
<input
   fr-validation
   validation="field.validation"
   ng-model="ngModel"
   id="{{ field.attrs.id }}"
   name="{{ field.attrs.name }}"
   class="field full"
   type="text"/>

```
