
# frFieldConfig
*Service for configuring formRenderer fields*


## SetType
Allows you to create a custom field type. You can use this field type in your form configuration. You can pass an object of options, or an array of objects of options.


#### Available options:
| Name            | Type                                          | Description                                                                                                                                                                                                                                                                                                                                                                  |
|-----------------|-----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| name            | string(required)                              | Name of the type. Use this `type` to configure a field in the form json                                                                                                                                                                                                                                                                                                      |
| templateUrl     | string(required when *template* option is empty)                              | The template url for the field when the form is renderer with the fr-form directive. The templateUrl will overrule the template when it is set.                                                                                                                                                                                                                                                                                              |
| template        | string(required when *templateUrl* is empty)                              | 
| viewTemplateUrl | string                                        | The template for the field when the form is rendered with the fr-form-viewer directive                                                                                                                                                                                                                                                                                       |
| defaultOptions  | object / function(options)                    | Allow default options for a field. These options are merged with with the field options when we render this field type. All field that extend this type will have these options as default.                                                                                                                                                                                  |
| wrapper         | string / array                                | specify which wrappers the field should use by default, use the wrapper name to define one. Use these wrappers to render a form schema with the fr-form directive.                                                                                                                                                                                                           |
| viewWrapper     | string / array                                | specify which wrappers the field should use by default, use the wrapper name to define one. Use these wrappers to render the form schema with the fr-form-viewer directive.                                                                                                                                                                                                  |
| extend          | string                                        | You can extend functionality of other types. It merges the options with the parent(base type) options. it merges defaultOptions, controllers and link functions                                                                                                                                                                                                              |
| controller      | string(controller name) / injectable function | The function will be invoked using the $controller service after the `frFieldController`. The $scope of the `frField` directive is available plus you can inject anything in the `$injector`. When you use a string, be sure it has a name that was registered with angular. If the type extend another, this function will run after the parent controller function is run. |
| link            | function(scope, element, attrs)               | The function will be invoked after the link function from the `frField` directive. All normal link arguments are available (scope, element, attrs). If the type extend another, this function will run after the parent link function is run.                                                                                                                                |


#### DefaultOptions:
Here is a list with some of the common properties that you can use to set some of the defaultOptions

```javascript
{
    attributes: {},
    options: {},
    validation: {
        validators: []
    }
}
```


#### Example:
```javascript
// Create a single type
frFieldConfig.setType({
    name: 'customType',
    templateUrl: 'views/myCustomTemplate.html'
});

// create multiple types with array
frFieldConfig.setType([
    {
        name:'customSelect',
        templateUrl: 'views/customSelect.html'
    },
    {
        name:'customRadio',
        templateUrl: 'views/customRadio.html'
    }
]);

// Extend type with defaultOptions as a function
frFieldConfig.setType({
    name: 'firstName',
    extend: 'input',
    defaultOptions: function(options) {
        return {
            options: {
                label: 'my label'
            }
        };
    },
    link: function(scope, element, attrs) {
    },
    controller: function($scope, $timeout) {
        $timeout(function() {
            // Do something async
        }, 10);
    }
});

// Extend type with defaultOptions as an object
frFieldConfig.setType({
    name: 'firstName',
    extend: 'input',
    defaultOptions: {
        options: {
            label: 'my label'
        }
    }
});

// Use a type
var schema = {
	formId: 'formId',
    info: {
    	title: 'Form title'
    },
    name: 'Form name',
    rendererVersion: 1,
    canSaveDraft: false,
    steps: [],
    sections: [],
    fields: [{
    	name: 'myCustomRadio',
    	spec: {
    		attributes: {
    			type: 'customRadio', // fieldconfiguration name!
    			value:
    		},
    		options: {
    			layout: {
    				fieldLayout: 'medium'
    			}
    		},
    		state: {
    			editable: true,
                editMode: true,
                disabled: false,
                extraInfoCollapsed: true
            },
            inputFilter: {
                validators: []
            },
            prerequisites: {}
    	}
    }]
};

```

## SetTempWraper

Allows you to create a template wrapper around a field template. You can use a default wrapper (by default wrap all field types) or you can use typed template wrapper (only used by the type).

#### Required fields and rules:

1. Use a `<fr-transclude></fr-transclude>` tag inside your wrapper template to tell the system where is should include the field template.
2. a wrapper should have a `templateUrl`

##### Options:

| Name        | Type             | Description                                                           |
|-------------|------------------|-----------------------------------------------------------------------|
| name        | string           | Name of the wrapper. Use this `name` to set a wrapper to a field type |
| templateUrl | string(required) | The template for the wrapper                                          |
| fieldTypes  | string / array   | define on which field types the wrapper should be applied             |


##### Template's $scope

| Variable name    |  Description                                                                                                                               |
|------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| options                 | The data provided to configure a field.                                                                                             |
| fields                  | All fields in a section step or form.                                                                                               |
| ngModel                 | The model of the form. Use ngModel[fielName] to find the model of the field.                                                        |
| ngModelInit             | This is the initial model value from the field.                                                                                     |
| field.toggleEditMode()  | Use this function to toggle the editMode.                                                                                           |
| field.spec              | This in an `alias` to **options.spec**                                                                                              |
| field.to                | This is an `alias` to **options.spec.options**. If you need custom properties you can add them here.                                |
| field.attrs             | This is an `alias` to **options.spec.attributes**. If you need input attributes like `placeholder` you can add them here.           |
| field.validation        | This is an `alias` to **options.inputFilter**. You can give this validation object to the validation directive to validate a field. |
| field.state             | This is an `alias` to **options.state**. If you need new states you can add them here.                                              |


##### Create a new wrapper:

```javascript
frFieldConfig.setTempWrapper([
    {
        templateUrl: 'views/wrappers/default.html' // this will be the default wrapper
    },
    {
        name: 'customWrapper', // use this name to give a wrapper to a field type
        templateUrl: 'views/wrappers/custom-wrapper.html'
    },
    {
        templateUrl: 'views/wrappers/wrapper-types.html',
        fieldTypes: ['select', 'radio', 'text'] // apply this wrapper to a list of field types
    }
]);

// apply a wrapper to a field type
frFieldConfig.setType({
    name: 'text',
    templateUrl: 'views/input/text.html',
    wrapper: ['customWrapper']
});
```

##### Template wrapper example:
```html
    <div class="field-wrapper">
        <label for="{{ field.attrs.id }}">{{ field.to.label }}</label>
        <!-- use fr-transclude tag to render the field template  -->
        <fr-transclude></fr-transclude>
    </div>

```

## SetFieldLayout

Allows you to create field layouts.
This way you don't need to remember the classes you want to set.


#### Options

| Name       | Type             | Description                                                         |
|------------|------------------|---------------------------------------------------------------------|
| name       | string           | Name of the layout. Use this `name` to set a layout to a field type |
| fieldClass | string(required) | The layout class for the field                                      |

#### Create a new field layout

```javascript
frFieldConfig.setFieldLayout([
	{
		fieldClass: 'span-full' // default field class
	},
	{
		name: 'medium',
		fieldClass: 'span-half'
	}
]);


var schema = {
	formId: 'Id',
    info: {
    	title: 'Form title'
    },
    name: 'FormName',
    rendererVersion: '1',
    canSaveDraft: false,
    steps: [],
    sections: [],
    fields: [{
    	name: 'customField',
    	spec: {
    		options: {
    			layout: {
    				fieldLayout: 'medium'
    			}
    		}
    	}
    }]
};

```
