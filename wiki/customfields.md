# Custom Fields BIAS formulieren

## Bereken uw aanslag

Dit veld toont tekst en een knop om de aanslag te berkenen. Wanneer berekend wordt de value opgevuld, en veranderd het veld van uiterlijk.
Dit kan via de extra properties `title` en `content`. ook de `buttontext` kan gezet worden om de user betere UX te geven. De actie acther de button blijft immers telkens gelijk. Bij een readonly formulier, dienen de states `editable` en `editMode` correct gezet te worden, dan zal de button afhankelijk van de gezette states wel of niet beschikbaar zijn.

```javascript
{
  "name": "calculate",
  "spec": {
    "attributes": {
      "type": "astad-bias-calculate-tax",
      "value": 1038
    },
    "options": {
      "title": "Bereken uw verwachte aanslag",
      "content": "Opgelet dit bedrag is louter informatief."
      "buttontext": "Berekenen"
    }
  },
  "state": {
    "editable": true,
    "editMode": true
  },
  "prerequisites": {},
  "inputFilter": {}
}
```

Properties benodigd om die blok te tonen:

* `spec.attributes.type`: `astad-bias-calculate-tax`  het type veld
* `spec.attributes.value`: dient te worden ingevuld met het bedrag.
  * indien niet ingevuld wordt er geen bedrag getoond
* `spec.options.title`: veld titel, enkele voorbeelden van titels:
  * 'Bereken uw verwachte aanslag'
  * 'Verwachte aanslag'
* `spec.options.content`: veld omschrijving, enkele voorbeelden van omschrijvingen:
  * 'Opgelet dit bedrag is louter informatief.'
  * 'Indien u zich akkoord verklaard met de voorgesteld gegevens binnen de voorziene termijn is dit uw verwachte aanslag. Opgelet dit bedrag is louter informatief.'
* `spec.options.buttontext`:
  * 'Berekenen'
  * 'Herberekenen'
* `state.editable`: true / false
  * true: kan berekenen, de button wordt getoond
  * false: kan niet berkenen, resultaat wordt getoont

## Info block

Dit veld toont html content met een bepaalde status.

```javascript
{
  "name": "statusinfo",
  "spec": {
    "attributes": {
      "type": "astad-message",
      "value": null
    },
    "options": {
      "status": "info",
      "title": "Status specifieke info boodschap",
      "content": "Deze aanvraag werd reeds voor u voorbereid op basis van uw voorgaande aangiftes."
    }
  },
  "state": {
    "editable": true,
    "editMode": true
  },
  "prerequisites": {},
  "inputFilter": {}
}
```

Properties benodigd om die blok te tonen:

* `spec.attributes.type`: `astad-message`  het type veld
* `spec.options.title`: veld titel
  * 'Status specifieke info boodschap'
* `spec.options.content`: veld omschrijving
  * 'Deze aanvraag werd reeds voor u voorbereid op basis van uw voorgaande aangiftes.'

## Te Betalen

Dit veld toont html content. Een table met informatie rond de betaling, maar de content bepaal je zelf...

```javascript
{
  "name": "paymentinfo",
  "spec": {
    "attributes": {
      "type": "astad-html",
      "value": null
    },
    "options": {
      "layout": {
        "fieldClass": "astad-bias-paymentinfo"
      },
      "content": "<div class="info">Gelieve onderstaand bedrag over te maken op rekeningnummer: <span class="highlight">BE32 3948539 39</span> met volgende mededeling: <span class="highlight">+++123/1234/12345+++</span></div><div><table><tr><td>Basisbedrag</td><td class="amount">&eur; 1293</td></tr><tr><td>Ambtelijke verhoging wegens 'soort'</td><td class="amount">&eur; 93</td></tr><tr><td>Betaald</td><td class="amount">&eur; -33</td></tr><tr><td>Blokkering</td><td class="amount">&eur; -12</td></tr><tr><td>Interest</td><td class="amount">&eur; 93</td></tr><tr class="total"><td>Totaal te betalen aanslag:</td><td class="amount">&eur; 1293</td></tr></table></div>"
    }
  },
  "state": {
    "editable": true,
    "editMode": true
  },
  "prerequisites": {},
  "inputFilter": {}
}
```

Properties benodigd om die blok te tonen:

* `spec.attributes.type`: `astad-html`  het type veld
* `spec.options.layout.fieldClass`: 'astad-bias-paymentinfo` css class waarmee we styling kunnen voorzien op de content van dit veld.
* `spec.options.content`: veld content
  <pre>&lt;div class=&quot;info&quot;&gt;Gelieve onderstaand bedrag over te maken op rekeningnummer: &lt;span class=&quot;highlight&quot;&gt;BE32 3948539 39&lt;/span&gt; met volgende mededeling: &lt;span class=&quot;highlight&quot;&gt;+++123/1234/12345+++&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;table&gt;&lt;tr&gt;&lt;td&gt;Basisbedrag&lt;/td&gt;&lt;td class=&quot;amount&quot;&gt;&amp;eur; 1293&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Ambtelijke verhoging wegens 'soort'&lt;/td&gt;&lt;td class=&quot;amount&quot;&gt;&amp;eur; 93&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Betaald&lt;/td&gt;&lt;td class=&quot;amount&quot;&gt;&amp;eur; -33&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Blokkering&lt;/td&gt;&lt;td class=&quot;amount&quot;&gt;&amp;eur; -12&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Interest&lt;/td&gt;&lt;td class=&quot;amount&quot;&gt;&amp;eur; 93&lt;/td&gt;&lt;/tr&gt;&lt;tr class=&quot;total&quot;&gt;&lt;td&gt;Totaal te betalen aanslag:&lt;/td&gt;&lt;td class=&quot;amount&quot;&gt;&amp;eur; 1293&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;</pre>

## disclaimer block

Dit veld toont html content. Een disclaimer met linkjes, maar de content bepaal je zelf...

```javascript
{
  "name": "disclaimer",
  "spec": {
    "attributes": {
      "type": "astad-html",
      "value": null
    },
    "options": {
      "layout": {
        "fieldClass": "astad-bias-disclaimer"
      },
      "content": "Niet akkoord met deze aanslag? U kan bezwaar aantekenen voor 22/04/2015. Let op! Het indienen van een bezwaar ontslaat u niet van de plicht om deze aanslag te betalen.<br />Vragen over deze aanslag? Lees onze algemene factuurvoorwaaden.<br />Vragen over dit reglement? Lees het volledige reglement hier."
    }
  },
  "state": {
    "editable": true,
    "editMode": true
  },
  "prerequisites": {},
  "inputFilter": {}
}
```

Properties benodigd om die blok te tonen:

* `spec.attributes.type`: `astad-html` het type veld
* `spec.options.layout.fieldClass`: 'astad-bias-disclaimer` css class waarmee we styling kunnen voorzien op de content van dit veld.
* `spec.options.content`: veld omschrijving
  <pre>Niet akkoord met deze aanslag? U kan bezwaar aantekenen voor 22/04/2015. Let op! Het indienen van een bezwaar ontslaat u niet van de plicht om deze aanslag te betalen.&lt;br />
  Vragen over deze aanslag? Lees onze algemene factuurvoorwaaden.&lt;br />
  Vragen over dit reglement? Lees het volledige reglement hier.</pre>

## Te corrigeren, accept/reject

Dit veld toont 2 buttons, accept en reject. Bij het accepteren dient het formulier de ganbare 'save' flow te volgen, en dus de onSave hook aan te roepen (mits validatie vooraf)... Bij het verwerpen van het voorstel dient een reden te worden opgegeven. Waarna je ook het formulier kan opslaan (mits validatie) of terug annuleren uit die reject flow.

```javascript
{
  "name": "reject-change",
  "spec": {
    "attributes": {
      "type": "astad-accept-reject",
      "value": null
    },
    "options": {
      content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br /> Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.<br /> Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."
    }
  },
  "state": {
    "editable": true,
    "editMode": true
  },
  "prerequisites": {},
  "inputFilter": {}
}
```

Properties benodigd om die blok te tonen:

* `spec.attributes.type`: `astad-accept-reject` het type veld
* `spec.options.content`: Deze tekst staat bij het veld waar de gebruiker een reden van weigering moet ingeven. Deze tekst kan dus indien gewenst specifiek aan het formulier, of aan de te corrigeren velden worden aangepast.

## Bias file upload field

Met dit veld kan je verschillende type files uploaden naar BIAS.
De value dient null of een lege array te zijn bij start, maar kan vooraf ingeuvld worden met documentum files om reeds weer te
**Opgepast: Gebruik dit type veld enkel en alleen voor formulieren van bias!**

```javascript
{
  "name": "eloket-upload-field",
  "spec": {
    "attributes": {
      "type": "astad-bias-file",
      "value": ["documentum://BIAS//Brieven/2015/1000000000000726", "documentum://BIAS//Brieven/2015/1000000000000727"]
    },
    "options": {
      "fileinfo": {
        "multiple": true,
        "size": {
          "min": "",
          "max": ""
        },
        "allowedFiles": {
          "validMineTypes": [
            "image/jpeg",
            "image/png",
            "image/pjpeg",
            "image/gif",
            "image/svg+xml",
            "application/pdf"
          ],
          "validExtensions": [
            ".jpg",
            ".png",
            ".gif",
            ".svg",
            ".pdf"
          ]
        }
      }
    }
  },
  "state": {
    "editable": true,
    "editMode": true
  },
  "prerequisites": {},
  "inputFilter": {}
}
```

Properties benodigd om die blok te tonen:

* `spec.attributes.value`: indien files reeds meegegeven moeten worden, dient dit een array van documentum URIs te zijn
* `spec.attributes.type`: `astad-bias-file` het type veld
* `spec.options.fileinfo`: Dit object bevat alle file meta data
* `spec.options.fileinfo.size.min`: De minimum toelaatbare file groote
* `spec.options.fileinfo.size.max`: De maximum toelaatbare file groote
* `spec.options.fileinfo.allowedFiles.validMineTypes`: De toegelate mine types
* `spec.options.fileinfo.allowedFiles.validExtensions`: De toegelate extensions

Na submit van het formulier komt bovenstaande field als volgt in de form output:

```
{
  ... other fields,
  "eloket-upload-field": ["documentum://BIAS//Brieven/2015/1000000000000726", "documentum://BIAS//Brieven/2015/1000000000000727"]
}
```

# Custom fields eloket

## eloket file upload field

Met dit veld kan je verschillende type files uploaden naar de asset server van astad.
De files krijgen automatisch een private flag zodat enkel gebruikers met de juiste rechten de files kunnen opvragen.
**Opgepast: Gebruik dit type veld enkel en alleen in het eloket!**

```javascript
{
  "name": "eloket-upload-field",
  "spec": {
    "attributes": {
      "type": "astad-eloket-file",
      "value": ""
    },
    "options": {
      "fileinfo": {
        "multiple": true,
        "size": {
          "min": "",
          "max": ""
        },
        "allowedFiles": {
          "validMineTypes": [
            "image/jpeg",
            "image/png",
            "image/pjpeg",
            "image/gif",
            "image/svg+xml"
          ],
          "validExtensions": [
            ".jpg",
            ".png",
            ".gif",
            ".svg"
          ]
        }
      }
    }
  },
  "state": {
    "editable": true,
    "editMode": true
  },
  "prerequisites": {},
  "inputFilter": {}
}
```

Properties benodigd om die blok te tonen:

* `spec.attributes.type`: `astad-eloket-file` het type veld
* `spec.options.fileinfo`: Dit object bevat alle file meta data
* `spec.options.fileinfo.size.min`: De minimum toelaatbare file groote
* `spec.options.fileinfo.size.max`: De maximum toelaatbare file groote
* `spec.options.fileinfo.allowedFiles.validMineTypes`: De toegelate mine types
* `spec.options.fileinfo.allowedFiles.validExtensions`: De toegelate extensions
