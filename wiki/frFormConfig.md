# frFormConfig
*service for configuring a formrenderer form*

## extendConfig
Allows you to configure the form.

### Options

| Name                               | Type                 | Description                                                  |
|------------------------------------|----------------------|--------------------------------------------------------------|
| scrollToAndFocusFirstErrorOnSubmit | Bool (Default: true) | Scroll to the first error inside the form                    |
| scrollAnimationTime                | Int                  | The time needed to scroll to the first error inside the form |
| scrollOffset                       | Int                  | scroll offset                                                |
| modelOutput                        | Object               | Define the model output on submit and navigate               |
| formClass                          | String               | Add a form class to add custom styling                       |


### model output

When you set PrerequisitesMatch to true the form model object will only include the values from the fields whose prerequistes match.

### Example
```javascript
frFormConfigP.extendConfig({
	scrollToAndFocusFirstErrorOnSubmit: true,
    scrollAnimationTime: 800,
    scrollOffset: -300,
    formClass: 'my-form',
    modelOutput: {
    	onSubmit: {
    		PrerequisitesMatch: false
    	},
    	onNavigate: {
    		PrerequisitesMatch: false
    	}
    }
});
```
