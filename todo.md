# TODO

## General

* Refactor: unit tests, they should test on distributed code, not the /src/** stuff
* Refactor: the parser is overwriding the default values (exp: label)
* Add: json validation to the schema
* Refactor: strings, for multilanguage... buttons send, previous, next or links to toggle sections, or links to show extra info .... We need to find a way these texts are configurable. (locale?)

## UI

* Fix: tab order - 'next' should come before 'previous' when tabbing with the keyb.
* click next to date picker should close it
* rendering single checkbox fields should we do this different (label + checkbox + label >>>>  just render as single checkbox with a label in front... (check with Eline)

## Documentation

* Add: make changelist
* Add: angularDoc
* Add: schema documentation in markdown
* Add: github pages
* Refactor: examples to github pages possibly even outside this repo?
* Add examples html page for prerequisites:
    * ~~field value examples~~
        * ~~field value toggles a step~~
        * ~~field value toggles a section~~
        * ~~field value toggles another field~~
    * field complete examples
        * FieldsComplete toggle a step
        * FieldsComplete toggle a section
        * FieldsComplete toggle another field
    * steps complete examples
        * StepsComplete toggle a step
        * StepsComplete toggle a section
        * StepsComplete toggle another field
    * sections complete examples
        * SectionsComplete toggle a step
        * SectionsComplete toggle a section
        * SectionsComplete toggle another field
    * combo examples
        * any random of the above together with another, in OR clause
        * any random of the above together with another, in AND clause










