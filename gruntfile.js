module.exports = function (grunt) {
    'use strict';

    var paths = {
        basePath: 'src',
        sassPath: 'src/css/scss',
        cssPath: 'src/css',
        modules: {
            form: {
                templatePath: 'src/fr/modules/form/views',
                jsPath: 'src/fr/modules/form/js'
            },
            fieldTypes: {
                templatePath: 'src/fr/modules/field-types/views',
                jsPath: 'src/fr/modules/field-types/js'
            },
            validation: {
                templatePath: 'src/fr/modules/validation/views',
                jsPath: 'src/fr/modules/validation/js'
            },
            views: {
                templatePath: 'src/fr/modules/views'
            }
        },
        testPath: 'src/tests',
        dest: 'dist',
        temp: '.temp'
    };

    /**
     * LOAD GRUNT TASK & INIT CONFIG ----------------
     * https://github.com/sindresorhus/load-grunt-tasks
     * http://gruntjs.com/api/grunt.config
     *
     * LOAD GRUNT TASK
     * - Usually you would have to load each task one by one, which is unnecessarily cumbersome.
     * - Before: grunt.loadNpmTasks('grunt-shell');
     * - After: require('load-grunt-tasks')(grunt);
     *
     */
    require('load-grunt-tasks')(grunt);
    grunt.initConfig({
        paths: paths,
        pkg: grunt.file.readJSON('package.json'),

        /**
         * GRUNT CONTRIB CONNECT --------------------
         * https://github.com/gruntjs/grunt-contrib-connect
         */

        connect: {
            server: {
                options: {
                    port: 35732,
                    hostname: '*',
                    base: '.'
                }
            }
        },

        bump: {
            options: {
                files: ['package.json', 'bower.json'],
                commit: false,
                createTag: false,
                push: false
            }
        },

        /**
         * LIBSASS -----------------------------------
         * https://github.com/sindresorhus/grunt-sass
         */
        sass: {
            options: {
                sourceMap: true,
                includePaths: [
                    'bower_components'
                ]
            },
            build: {
                files: {
                    '<%= paths.dest %>/<%= pkg.name %>.css': '<%= paths.sassPath %>/main.scss'
                }
            }
        },
        /**
         * CSSMIN -------------------------------------------
         * https://github.com/gruntjs/grunt-contrib-cssmin
         */
        cssmin: {
            options: {
                sourceMap: true
            },
            formRenderer: {
                files: {
                    '<%= paths.dest %>/<%= pkg.name %>-<%= pkg.version %>.min.css': [
                        '<%= paths.dest %>/<%= pkg.name %>.css'
                    ]
                }
            }
        },

        /**
         * Html2js ------------------------------------------
         * This plugin converts a group of templates to JavaScript
         * and assembles them into an Angular module
         * https://github.com/karlgoldstein/grunt-html2js
         */
        html2js: {
            options: {
                base: '<%= paths.basePath %>',
                module: 'formRenderer-templates',
                useStrict: true,
                watch: true,
                fileHeaderString:   '// ATTENTION!' + String.fromCharCode(13) +
                            '// DO NOT MODIFY THIS FILE BECAUSE IT WAS GENERATED AUTOMATICALLY' + String.fromCharCode(13) +
                            '// SO ALL YOUR CHANGES WILL BE LOST THE NEXT TIME THE FILE IS GENERATED'
            },
            main: {
                    src: ['<%= paths.basePath %>/**/*.html'],
                    dest: '<%= paths.temp %>/<%= pkg.name %>-templates.js'
            }
        },

        /**
         * KARMA TESTS --------------------------------------
         * see karma.conf for configuration options
         * https://github.com/karma-runner/grunt-karma
         */
        karma: {
            options: {
                configFile: 'karma.conf.js',
            },
            unit: {
                singleRun: true
            },
            continuous: {
                background: true,
                singleRun: false
            },
        },

        /**
         * JSHINT -----------------------------------
         * See .jshintrc for configuration options
         * https://github.com/gruntjs/grunt-contrib-jshint
         * http://www.jshint.com/docs/options/
         */
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            files: [
                'gruntfile.js',
                '<%= paths.basePath %>/**/*.js',
                '!grunt-tmp/**/*.js',
                '!<%= paths.basePath %>/vendor/**/*.js'
            ]
        },

        /**
         * Watch files -------------------------------
         * https://github.com/gruntjs/grunt-contrib-watch
         */
        watch: {
            jshint: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint']
            },
            buildjs: {
                files: ['<%= jshint.files %>'],
                tasks: ['html2js', 'buildjs']
            },
            sass: {
                files: ['<%= paths.sassPath %>/**/*.scss'],
                tasks: ['buildcss']
            },
            karma: {
                files: [
                    '<%= paths.basePath %>/**/*.js',
                    '<%= paths.testPath %>/**/*.js',
                    '<%= paths.testPath %>/*.js'
                ],
                tasks: ['karma:continuous:run'] //NOTE the :run flag
            },
            html2js: {
                files: [
                    '<%= paths.basePath %>/**/*.html'
                ],
                tasks: ['html2js', 'buildjs']
            }
        },

        /**
         * CLEAN PREVIOUS BUILDS ---------------------
         * We will clean up all minified files before a new build
         * We remove alle temp folders after a successful build
         *
         * https://github.com/gruntjs/grunt-contrib-clean
         */
        clean: {
            prebuildjs: {
                src: [
                    '<%= paths.dest %>/<%= pkg.name %>.js',
                    '<%= paths.dest %>/<%= pkg.name %>-*.js',
                    '<%= paths.dest %>/<%= pkg.name %>-*.js.map',
                ]
            },
            prebuildcss: {
                src: [
                    // '<%= paths.dest %>/<%= pkg.name %>-*.css',
                    // '<%= paths.dest %>/<%= pkg.name %>-*.css.map'
                ]
            },
            postbuild: {
                src: ["<%= paths.temp %>"]
            },
        },

        /**
         * CONCAT ALL JS FILES -------------------------------
         * Concat all javascript files and store them in a .tmp folder
         * https://github.com/gruntjs/grunt-contrib-concat
         */
        concat: {
            options: {
                separator: ';',
                stripBanners: true,
                banner: '/*\n\tAngular Form Renderer v<%= pkg.version %>\n*/\n'
            },
            formRenderer: {
                src: [
                    '<%= paths.temp %>/formrenderer-templates.js',
                    '<%= paths.basePath %>/fr/**/*.js',
                    '!<%= paths.basePath %>/fr/modules.js',
                    '!<%= paths.basePath %>/fr/main.js'
                ],
                dest: '<%= paths.temp %>/formRenderer_without_main.js'
            },
            formRendererWithMain: {
                src: [
                    '<%= paths.basePath %>/fr/modules.js',
                    '<%= paths.basePath %>/fr/main.js',
                    '<%= paths.temp %>/formRenderer_without_main.js'
                ],
                dest: '<%= paths.dest %>/formrenderer.js'
            }
        },
        /**
         * STRIP ALL JS FILES ----------------------------------
         * Remove all console and debug commands
         * https://github.com/jsoverson/grunt-strip
         */
        strip: {
            formsRenderer: {
                src: '<%= paths.dest %>/formrenderer.js',
                dest: '<%= paths.temp %>/formrenderer_stripped.js',
                options: {
                    nodes: [
                      /*  'console.log',
                        'console.warn',
                        'console.error',
                        'console.group',
                        'console.groupEnd',
                        'console.info',
                        'console.groupCollapsed',
                        'debug'*/
                    ]
                }
            }
        },
        /**
         * UGLIFY ALL JS FILES ------------------------------
         * https://github.com/gruntjs/grunt-contrib-uglify
         */
        uglify: {
            options: {
                mangle: false
            },
            formRenderer: {
                files: {
                    '<%= paths.dest %>/<%= pkg.name %>-<%= pkg.version %>.min.js': ['<%= paths.temp %>/formrenderer_stripped.js']
                }
            }
        },
        /**
         * GENERATE JS DOC FILES ----------------------------
         * https://github.com/krampstudio/grunt-jsdoc
         */
        jsdoc : {
            dist : {
                src: [
                    '<%= paths.basePath %>/fr/modules/**/*.js',
                    'README.md'
                ],
                options: {
                    destination: 'doc',
                    template : "node_modules/grunt-jsdoc/node_modules/ink-docstrap/template",
                    configure : "node_modules/grunt-jsdoc/node_modules/ink-docstrap/template/jsdoc.conf.json"
                }
            }
        }
    });

    var bumper = grunt.option('bump') || 'patch';
    // development task, use: `grunt development` code hinting everytime you save your javascript files
    grunt.registerTask('development', [
        'connect',
        'jshint',
        'sass',
        'html2js',
        // 'karma:continuous',
        'watch'
    ]);

    // unit test task
    grunt.registerTask('unit-test', [
        'karma:continuous',
        'watch:karma'
    ]);

    // default task, when using `grunt` without arguments
    grunt.registerTask('default', [
        'development'
    ]);

    // production task
    grunt.registerTask('production', [
        'html2js',
        'buildjs',
        'buildcss'//,
        // 'jsdoc'
    ]);

    // release task
    grunt.registerTask('release', [
        'bump:' + bumper,
        'production'
    ]);

    grunt.registerTask('buildjs', [
        'clean:prebuildjs',
        'concat',
        'strip',
        'uglify',
        'clean:postbuild'
    ]);

    grunt.registerTask('buildcss', [
        'clean:prebuildcss',
        'sass',
        'cssmin',
        'clean:postbuild'
    ]);

    // Move all files to the dest folder
    // and open up a server
    grunt.registerTask('server', [
        'html2js',
        'buildjs',
        'buildcss',
        'connect:server:keepalive'
    ]);

};
