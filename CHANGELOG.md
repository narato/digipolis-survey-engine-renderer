# Changelog formrenderer angular module
===========================================

Please document all notable change to the project inside this file.


## 2.0.14

Bugfixes:

- Make sure that the model received in the onNavigate hook is the whole model by default.

## 2.0.13

Feature:

- add a new function (getDataStore) on the form controller.

## 2.0.12 (BROKEN)

## 2.0.11

- Add a new class (fr-field--first) to each **first** fr-field inside a step or section.
- Fix model creation for fields where the modelType is set to "Array"


## 2.0.10 (BROKEN)

## 2.0.9
## 2.0.8
## 2.0.7
## 2.0.6
## 2.0.5
## 2.0.4
## 2.0.3

## 2.0.2

- fix examples
- restyling sections
- change the currentIndex when the activeStep is changed.  

## 2.0.0

- All basic field types are removed from this repository. Here you can find all basic fields: ssh://git@git.antwerpen.be/astad/formrenderer-types-astad_tools.angularjs.git


## 1.1.2

- rename the vendor map to bower_components
- fix checkbox validation bug, use ctrl.modelValue instead of ctrl.viewValue.
- include sasskit
- cleanup styling


## 0.0.1

Initial release
