(function () {
    'use strict';

    var API = {};
    var frConfig;
    var $injector;

    // load modules
    beforeEach(function moduleLoading() {
        reset();
        angular.module('formRenderer-templates', []);
        module('formRenderer');
        module('formTemplates');
    });

    function reset() {
        $injector = undefined;
        frConfig = undefined;
    }

    function $get(name) {
        if (!$injector) {
            inject(function (_$injector_) {
                $injector = _$injector_;
            });
        }
        return $injector.get(name);
    }

    function $setTypeText() {

        if (!frConfig) {
            frConfig = $get('frFieldConfig');
        }

        if  (!frConfig.getType('text')) {
            frConfig.setType({
                name: 'text',
                template: '<input name="defaultText" type="text" value="text">'
            });
        }
    }

    API.$get = $get;
    API.$setTypeText = $setTypeText;
    window.helper = API;

})();
