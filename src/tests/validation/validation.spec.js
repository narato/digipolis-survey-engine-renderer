(function (expect, describe, it, angular, helper) {
    'use strict';
    describe("Validation.Directive: validation", function () {


        var $injector,
            $controller,
            $compile,
            $scope,
            $rootScope,
            $templateCache,
            $q,
            frValidationProviderObject,
            validHTML;

        function getCompiledElement(template, scope) {
            var element = angular.element(template);
            var compiledDirective = $compile(element)(scope);
            $scope.$digest();
            return {
                compiledHTML: compiledDirective,
                isolateScope: element.isolateScope()
            };
        }

        validHTML = '<form name="Form" >' +
                    '<input ' +
                    'fr-validation ' +
                    'validation="inputField.inputFilter" ' +
                    'id="{{ inputField.id }}" ' +
                    'name="required" ' +
                    'ng-model="inputField.value" ' +
                    'type="text" ' +
                    'placeholder="inputField.placeholder" ' +
                    'data-valid-callback="inputField.validCallback()" ' +
                    'data-invalid-callback="inputField.invalidCallback()" >' +
                    '</form>';

        /**
         * Load form renderer and form template modules
         */
        beforeEach(function() {
            module(function(frValidationConfigProvider) {
                frValidationProviderObject = frValidationConfigProvider;
            });
        });

        beforeEach(function suiteArrange() {
            inject(function (_$injector_, _$controller_, _$compile_, _$templateCache_, _$q_) {
                $injector = _$injector_;
                $controller = _$controller_;
                $compile = _$compile_;
                $q = _$q_;
                $templateCache = _$templateCache_;
                $rootScope = helper.$get('$rootScope');
            });
        });

        describe('inputFilter.required property', function() {
            beforeEach(function() {
                $scope = $rootScope.$new();
                frValidationProviderObject.setValidator({
                    type: 'required',
                    expression: function(value, scope, element, attrs, param) {
                        return !!value;
                    },
                    defaultMessage: "the field is required",
                });
            });

            it('should add required validator when `inputFilter.required` is true and there isn\'t already a validator with the name `required`' , function() {
                $scope.inputField = {
                    value: '',
                    id: 'required',
                    name: 'required',
                    inputFilter: {
                        name: 'required',
                        required: true
                    }
                };
                var directive = getCompiledElement(validHTML, $scope);
                $scope.inputField.value = 'dummyValue';
                $scope.$digest();
                $scope.inputField.inputFilter.validators.should.include.something.that.deep.equals(
                    {
                        "name": "required",
                        "type": "required",
                        "errorMessage": "this is a required field"
                    }
                );
                $scope.inputField.value = 'dummyValue1';
                $scope.$digest();
                expect($scope.inputField.inputFilter.validators.length).to.equals(1);
            });

            it('should ignore an unknown validator type', function() {
                $scope.inputField = {
                    value: '',
                    id: 'required',
                    name: 'required',
                    inputFilter: {
                        name: 'required',
                        required: false,
                        validators: [
                            {
                                name: 'dummy validator',
                                type: 'unknownValidator',
                                errorMessage: 'dummyValidator'
                            }
                        ]
                    }
                };
                var directive = getCompiledElement(validHTML, $scope);
                $scope.inputField.value = 'dummyValue';
                $scope.$digest();
                expect($scope.inputField.inputFilter.error).to.be.empty;

            });

            it('should remove the `required` validator when `inputFilter.required` is false', function() {
                $scope.inputField = {
                    value: '',
                    id: 'required',
                    name: 'required',
                    inputFilter: {
                        name: 'required',
                        required: true
                    }
                };
                var directive = getCompiledElement(validHTML, $scope);
                $scope.inputField.value = 'dummyValue';
                $scope.$digest();
                $scope.inputField.inputFilter.validators.should.include.something.that.deep.equals(
                    {
                        "name": "required",
                        "type": "required",
                        "errorMessage": "this is a required field"
                    }
                );
                $scope.inputField.inputFilter.required = false;
                $scope.$digest();
                $scope.inputField.value = 'dummyValue1';
                $scope.$digest();
                $scope.inputField.inputFilter.validators.should.not.include(
                    {
                        "name": "required",
                        "type": "required",
                        "errorMessage": "this is a required field"
                    }
                );
            });

            it('should overwrite the default errorMessage', function() {
                $scope.inputField = {
                    value: '',
                    id: 'required',
                    name: 'required',
                    inputFilter: {
                        name: 'required',
                        errorMessage: 'overwrite errorMessage',
                        required: true
                    }
                };
                var directive = getCompiledElement(validHTML, $scope);
                $scope.inputField.value = 'dummyValue';
                $scope.$digest();
                $scope.inputField.inputFilter.validators.should.include.something.that.deep.equals(
                    {
                        "name": "required",
                        "type": "required",
                        "errorMessage": "overwrite errorMessage"
                    }
                );
            });
        });

        describe('Example of required', function () {

            var directive;

            beforeEach(function prepareDirective() {
                $scope = $rootScope.$new();
                $scope.inputField = {
                    value: '',
                    id: 'required',
                    name: 'required',
                    invalidCallback: sinon.spy(),
                    validCallback: sinon.spy(),
                    inputFilter: {
                        name: 'required',
                        validators: [{
                            type: 'required',
                            name: 'required',
                            errorMessage: 'this is a required field'
                        }]
                    }
                };
                directive = getCompiledElement(validHTML, $scope);
            });

            it('should be initial pristine and valid', function() {
                // prestine means that the form is clean (not used)
                expect($scope.Form.$pristine).to.be.true;
                expect(directive.compiledHTML.hasClass('ng-pristine')).to.be.true;
                expect($scope.Form.$valid).to.be.true;
                expect($scope.Form.$invalid).to.be.false;
            });

            it('should be dirty and valid after input', function() {
                $scope.Form.required.$setViewValue('Required');

                expect($scope.Form.$dirty).to.be.true;
                expect(directive.compiledHTML.hasClass('ng-dirty')).to.be.true;
                expect($scope.Form.$valid).to.be.true;
                expect(directive.compiledHTML.hasClass('ng-valid')).to.be.true;
            });

            it('input should be valid', function() {
                 frValidationProviderObject.setValidator({
                    type: 'required',
                    expression: function(value, scope, element, attrs, param) {
                        return !!value;
                    },
                    defaultMessage: "the field is required",
                });
                $scope.inputField.value = 'required';
                $scope.$digest();

                expect($scope.Form.$dirty).to.be.true;
                expect(directive.compiledHTML.hasClass('ng-dirty')).to.be.true;
                expect($scope.Form.$valid).to.be.true;
                expect(directive.compiledHTML.hasClass('ng-valid')).to.be.true;
            });

            it('input dirty set to null should be invalid', function() {

                frValidationProviderObject.setValidator({
                    type: 'required',
                    expression: function(value, config) {
                        var deferred = $q.defer();
                        var result = !!value;
                        if (result) {
                            deferred.resolve(config);
                        } else {
                            deferred.reject(config);
                        }
                        return deferred.promise;
                    },
                    defaultMessage: "the field is required",
                });
                $scope.inputField.value = 'required';
                $scope.$digest();
                $scope.inputField.value = '';
                $scope.$digest();

                // there are now validators yet!!!!!
                expect($scope.Form.$dirty).to.be.true;
                expect(directive.compiledHTML.hasClass('ng-dirty')).to.be.true;
                expect($scope.Form.$invalid).to.be.true;
                expect(directive.compiledHTML.hasClass('ng-invalid')).to.be.true;
            });
        });
    });

})(chai.expect, describe, it, angular, window.helper);
