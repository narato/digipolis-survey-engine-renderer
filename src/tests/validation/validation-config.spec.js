(function (expect, describe, it, angular, helper) {
    describe("Validation.Provider: validation-config", function () {

        var frValidationConfig,
            frValidationProviderObject;

        beforeEach(function() {
            module(function(frValidationConfigProvider) {
                frValidationProviderObject = frValidationConfigProvider;
            });
        });

        beforeEach(inject(function ($injector) {
            frValidationConfig = helper.$get('frValidationConfig');
        }));

        it('add a new validator to the list of validators', function() {
            var validator = frValidationProviderObject.setValidator({
                type: 'required',
                expression: sinon.spy(),
                defaultMessage: 'test'
            });
            expect(validator).to.exist;
        });

        it('add an array of new validators to the list of validators', function() {
            var validators = [
                {
                    type: 'required',
                    expression: function() {},
                    defaultMessage: 'test'
                },
                {
                    type: 'digits',
                    expression: function() {},
                    defaultMessage: 'test'
                }
            ];
            var allValidators = frValidationProviderObject.setValidator(validators);
            expect(allValidators[0].type).to.equal('required');
            expect(allValidators[1].defaultMessage).to.equal('test');
        });

        it('type, expression and defaultMessage are required fields, throw error when not', function() {
            expect(function() {
                frValidationProviderObject.setValidator({
                    type: 'required',
                    expression: function() {}
                });
            }).to.throw;
        });

        it('should return (true / false) if the form is valid', function() {
            var formTrue = {
                $valid: true
            };
            expect(frValidationProviderObject.checkFormValid(formTrue)).to.be.true;

            var formFalse = {
                $valid: false
            };
            expect(frValidationProviderObject.checkFormValid(formFalse)).to.be.false;

            var formUnd = {
                $valid: undefined
            };
            expect(frValidationProviderObject.checkFormValid(formUnd)).to.be.false;
        });

        it('get a validator by type', function() {
            frValidationProviderObject.setValidator({
                type: 'required',
                expression: sinon.spy(),
                defaultMessage: 'test'
            });
            expect(frValidationConfig.getValidator('required').type).to.equal('required');
            expect(frValidationConfig.getValidator('required').defaultMessage).to.equal('test');
        });

        it('should get a default message when there is no errorMessage set', function() {
            var validator = frValidationProviderObject.setValidator({
                type: 'required',
                expression: sinon.spy(),
                defaultMessage: 'test'
            });
            expect(frValidationConfig.getErrorMessage('required')).to.equal('test');
        });

         it('should get a errorMessage when there is one', function() {
            var validator = frValidationProviderObject.setValidator({
                type: 'required',
                expression: sinon.spy(),
                defaultMessage: 'test',
                errorMessage: 'dummy errorMessage'
            });
            expect(frValidationConfig.getErrorMessage('required')).to.equal('dummy errorMessage');
        });
    });
})(chai.expect, describe, it, angular, window.helper);
