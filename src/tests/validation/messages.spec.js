(function (expect, describe, it, angular, helper) {
    describe("Validation.Directive: messages", function () {

        var $scope,
            $timeout,
            $compile,
            compiledDirective,
            element,
            isolateScope,
            frValidationProviderObject;

        var validHTML = '<div fr-messages="inputField.error"></div>';
        /**
         * Create new scope
         * create new angular element
         * compile element with his scope
         * call the digest loop
         */
        beforeEach(inject(function($rootScope, _$compile_, $templateCache, $injector){
            $timeout = helper.$get('$timeout');
            $compile = _$compile_;
            $scope = $rootScope.$new();
            $scope.inputField = {
                error: [
                    {
                        type: 'required',
                        text: 'errorMessage'
                    }
                ],
                inputFilter: {
                    name: 'required',
                    validators: [{
                        type: 'required'
                    }]
                }
            };
            element = angular.element(validHTML);
            compiledDirective = $compile(element)($scope);
            $scope.$digest();
            isolateScope = element.isolateScope();
        }));


        /**
         * Testing scope -------------------------------------------------------------------
         */

        it('`messages` on isolated scope should be two-way bound', function() {
            isolateScope.messages = [
                {
                    type: 'between',
                    text: 'errorMessage'
                }
            ];
            $scope.$digest();
            expect($scope.inputField.error).to.equal(isolateScope.messages);
        });


        ////////////////////////////// End testing scope ////////////////////////////////////

        /**
         * Testing Directive template ------------------------------------------------------
         * We can verify if the template is applied on a directive
         * and also if the template has certain elements or directives in it.
         */

        it('compile the html correctly', function () {
            expect(compiledDirective.html()).to.not.equal('');
        });

        it('the template should have a class `fr-field-problem`', function() {
            expect(angular.element(compiledDirective.find('div')[0]).hasClass('fr-field-problem')).to.be.true;
        });

        ////////////////////////////// End testing template ////////////////////////////////////


    });

})(chai.expect, describe, it, angular, window.helper);
