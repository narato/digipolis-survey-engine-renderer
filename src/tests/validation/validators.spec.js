(function (expect, describe, it, angular, helper) {
    describe("Validation.Services: Default validators", function () {

        var frValidators,
            frDate,
            defaultValidators;

        beforeEach(inject(function ($injector) {
            frValidators = helper.$get('frValidators');
            frDate = helper.$get('akit.DateService');
            defaultValidators = frValidators.getDefaultValidators();
        }));


        it('should get all default validators', function() {
            expect(defaultValidators).to.exist;
        });

        /**
         * Required Validator
         */
        it('validator `required` should be rejected when value is empty and fulfilled when it is not empty', function() {
            angular.forEach(defaultValidators, function(validator) {
                if (validator.type === 'required') {
                    validator.expression('', {}).should.be.rejected;
                    validator.expression('test', {}).should.be.fulfilled;
                    validator.expression(0, {}).should.be.fulfilled;
                    validator.expression(true, {}).should.be.fulfilled;
                    validator.expression(false, {}).should.be.rejected;
                }
            });
        });

        /**
         * Number Validator
         */
        it('validator `number` should be fulfilled when the value is a number and rejected when it is not a number', function() {
             angular.forEach(defaultValidators, function(validator) {
                if (validator.type === 'number') {
                    // Return false
                    validator.expression('dcdcdc', validator).should.be.rejected;

                    // Return true when there is no value to check
                    validator.expression('', validator).should.be.fulfilled;
                    validator.expression(undefined, validator).should.be.fulfilled;
                    validator.expression(null, validator).should.be.fulfilled;

                    // Return true
                    validator.expression('8', validator).should.be.fulfilled;
                }
            });
        });

        /**
         * RegExp Validator
         */
        it('validator `regexp` should be fulfilled when the value is matching a given pattern and rejected when it is not matching', function() {
             angular.forEach(defaultValidators, function(validator) {
                if (validator.type === 'regexp') {

                    var regExp = new RegExp('^[0-9]+$');
                    // Return false and use a string as RegExp
                    validator.expression('test', { options: { pattern: '^[0-9]+$' } }).should.be.rejected;
                    validator.expression('test', { options: { pattern: '/^[0-9]+$/' } }).should.be.rejected;

                    // Return true when there is no value to check
                    validator.expression('', { options: { pattern: '/^[0-9]+$/' } }).should.be.fulfilled;
                    validator.expression(undefined, { options: { pattern: '/^[0-9]+$/' } }).should.be.fulfilled;
                    validator.expression(null, { options: { pattern: '/^[0-9]+$/' } }).should.be.fulfilled;

                    // return false and use a regExp object
                    validator.expression('test', { options: { pattern: regExp } }).should.be.rejected;

                    // Return true
                    validator.expression('8', { options: { pattern: '^[0-9]+$' } }).should.be.fulfilled;
                    validator.expression('8', { options: { pattern: '/^[0-9]+$/' } }).should.be.fulfilled;
                    validator.expression('8', { options: { pattern: regExp } }).should.be.fulfilled;

                    // use integers
                     validator.expression(8, { options: { pattern: '^[0-9]+$' } }).should.be.fulfilled;
                }
            });
        });

        /**
         * Length Validator
         * { min: '', max: ''}
         */
        it('validator `length` should be fulfilled when the value is matching a given pattern and rejected when it is not matching', function() {
             angular.forEach(defaultValidators, function(validator) {
                if (validator.type === 'length') {
                    validator.expression('ye', { options: { min: 2, max: 5 } }).should.be.fulfilled;
                    validator.expression('', { options: { min: 1, max: 5 } }).should.be.rejected;
                }
            });
        });

        /**
         * url Validator
         * Allow : http, https, ftp
         */
        it('validator `url` should be fulfilled when the value is a url otherwise the promis will be rejected', function() {
             angular.forEach(defaultValidators, function(validator) {
                if (validator.type === 'url') {
                    // Return false
                    validator.expression({ test: 'test'}, validator).should.be.rejected;
                    validator.expression('www.antwerpen.be', validator).should.be.rejected;
                    validator.expression('antwerpen.be', validator).should.be.rejected;
                    validator.expression('antwerpen', validator).should.be.rejected;

                    // Return true when there is no value to check
                    validator.expression('', validator).should.be.fulfilled;
                    validator.expression(undefined, validator).should.be.fulfilled;
                    validator.expression(null, validator).should.be.fulfilled;

                    // Return true when we have a valid URL
                    validator.expression('http://www.antwerpen.be', validator).should.be.fulfilled;
                    validator.expression('https://www.antwerpen.be', validator).should.be.fulfilled;
                    validator.expression('ftp://www.antwerpen.be', validator).should.be.fulfilled;
                }
            });
        });

        /**
         * url Validator
         * Allow : http, https, ftp
         */
        it('validator `email` should be fulfilled when the value is a email otherwise the promise will be rejected', function() {
             angular.forEach(defaultValidators, function(validator) {
                if (validator.type === 'email') {
                    // Return false
                    validator.expression('test.be', validator).should.be.rejected;
                    validator.expression('test@test', validator).should.be.rejected;
                     validator.expression({ test: 'test'}, validator).should.be.rejected;

                    // Return true when there us no value to check
                    validator.expression('', validator).should.be.fulfilled;
                    validator.expression(undefined, validator).should.be.fulfilled;
                    validator.expression(null, validator).should.be.fulfilled;

                   // Return true when we have a valid URL
                    validator.expression('test@test.be', validator).should.be.fulfilled;
                    validator.expression('verschoorenglenn@gmail.com', validator).should.be.fulfilled;
                }
            });
        });

        /**
         * url Validator
         * Allow : http, https, ftp
         */
        it('validator `between` should be fulfilled when the value is between min and max', function() {
             angular.forEach(defaultValidators, function(validator) {
                if (validator.type === 'between') {

                    // Don't validate when there is no data to check
                    validator.expression('', {}).should.be.fulfilled;
                    validator.expression(undefined, { options: { min: 1, max: 4} }).should.be.fulfilled;
                    validator.expression(null, { options: { min: 1, max: 4} }).should.be.fulfilled;

                    // check value between min and max
                    validator.expression('test', { options: { min: 1, max: 4} }).should.be.rejected;
                    validator.expression('3', { options: { min: 1, max: 4} }).should.be.fulfilled;
                    validator.expression(3, { options: { min: 1, max: 4} }).should.be.fulfilled;
                    validator.expression(3, { options: { min: 1, max: 3} }).should.be.fulfilled;
                    validator.expression(3, { options: { min: 1, max: 2} }).should.be.rejected;

                    // check value between min and infin
                    validator.expression(10000000000000000000000000000000000, { options: { min: 1} }).should.be.fulfilled;
                    validator.expression(2, { options: { min: 8} }).should.be.rejected;

                    // check value between 0 and max
                    validator.expression(0, { options: { max: 8} }).should.be.fulfilled;
                    validator.expression(8, { options: { max: 8} }).should.be.fulfilled;
                    validator.expression(9, { options: { max: 8} }).should.be.rejected;
                }
            });
        });


        describe('Date validator', function() {
            /**
             * Date Validator
             */
            it('validator `date` should compare dates', function() {
                angular.forEach(defaultValidators, function(validator) {
                    if (validator.type === 'date') {
                        var date = new Date('Fri Jun 1 2015 00:00:00 GMT+0200 (CEST)');
                        var dateWithoutTime = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                        var dateNowString = frDate.formatDate(dateWithoutTime, 'DD/MM/YYYY');
                        var datePlus = frDate.formatDate(frDate.parseDateYYMMDD(new Date(dateWithoutTime.getTime() + 20 * 24 * 60 * 60 * 1000)), 'DD/MM/YYYY');

                        validator.expression('thisIsNoDate', {
                            options: {
                                compare: {
                                    operator: '=',
                                    addDays: 20
                                }
                            }
                        }).should.be.rejected;

                        validator.expression('thisIsNoDate', {
                            options: {
                                compare: {
                                    date: 'thisIsNoDate',
                                    operator: '=',
                                    addDays: 20
                                }
                            }
                        }).should.be.rejected;

                        validator.expression(datePlus, {
                            options: {
                                compare: {
                                    operator: '=',
                                    addDays: 20
                                }
                            }
                        }).should.be.rejected;

                        validator.expression('21/06/1988', {
                            options: {
                                compare: {
                                    date: '1/06/1988',
                                    operator: '=',
                                    addDays: 20
                                }
                            }
                        }).should.be.fulfilled;

                        validator.expression('21/06/1988', {
                            options: {
                                compare: {
                                    date: '1/06/1988',
                                    operator: '>',
                                    addDays: 10
                                }
                            }
                        }).should.be.fulfilled;

                        validator.expression('19/06/1988', {
                            options: {
                                compare: {
                                    date: '20/06/1988',
                                    operator: '<'
                                }
                            }
                        }).should.be.fulfilled;

                        validator.expression('20/06/1988', {
                            options: {
                                compare: {
                                    date: '20/06/1988',
                                    operator: '<='
                                }
                            }
                        }).should.be.fulfilled;

                        validator.expression('20/06/1988', {
                            options: {
                                compare: {
                                    date: '20/06/1988',
                                    operator: '>='
                                }
                            }
                        }).should.be.fulfilled;
                    }
                });
            });
            it('Validator `date` should check if a date string comes before today', function() {
                angular.forEach(defaultValidators, function(validator) {
                    if (validator.type === 'date') {

                        validator.expression('10/06/1988', {
                            options: {
                                before: '15/06/1988'
                            }
                        }).should.be.fulfilled;

                        validator.expression('15/06/1988', {
                            options: {
                                before: '10/06/1988'
                            }
                        }).should.be.rejected;

                    }
                });
            });

            it('Validator `date` should check if a date string comes after today', function() {
                angular.forEach(defaultValidators, function(validator) {
                    if (validator.type === 'date') {

                        validator.expression('22/06/1988', {
                            options: {
                                after: '15/06/1988'
                            }
                        }).should.be.fulfilled;

                        validator.expression('01/06/1988', {
                            options: {
                                after: '10/06/1988'
                            }
                        }).should.be.rejected;
                    }
                });
            });

            it('Validator `date` should check if a date string has passed', function() {
                angular.forEach(defaultValidators, function(validator) {
                    if (validator.type === 'date') {

                        validator.expression('15/06/1988', {
                            options: {
                                hasPassed : true
                            }
                        }).should.be.fulfilled;

                        validator.expression('15/06/1988', {
                            options: {
                               hasPassed: false
                            }
                        }).should.be.rejected;
                    }
                });
            });
        });
    });
})(chai.expect, describe, it, angular, window.helper);
