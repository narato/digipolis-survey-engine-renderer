(function (expect, describe, it, angular, moment, helper) {
    describe("Utils.Service: fr utilities", function () {

        var frUtils;

        beforeEach(inject(function ($injector) {
            frUtils = helper.$get('frUtils');
        }));


        describe('extenDeep()', function() {

            it('should merge all properties from a src object to a destination (overwrite all properties)', function() {
                var obj1 = {
                    firstName: 'Glenn',
                    name: 'Verschooren',
                    parents: {
                        mother: 'Marianne',
                        father: 'Marc'
                    }
                };

                var obj2 = {
                    firstName: 'Glenn',
                    name: 'Scheers',
                    parents: {
                        mother: 'Brigitte',
                        father: 'Leon'
                    },
                    sister: 'Sien'
                };

                expect(frUtils.extendDeep(obj1, obj2)).to.eql({
                    firstName: 'Glenn',
                    name: 'Scheers',
                    parents: {
                        mother: 'Brigitte',
                        father: 'Leon'
                    },
                    sister: 'Sien',
                });
            });
        });

       describe('reverseDeepMerge()', function() {
            it('should merge all properties that ar not defined in the destination object from a source to a destination', function() {
                var obj1 = {
                    firstName: 'Glenn',
                    name: 'Verschooren',
                    parents: {
                        mother: 'Marianne',
                        father: 'Marc'
                    }
                };
                var obj2 = {
                    firstName: 'Glenn',
                    name: 'Scheers',
                    parents: {
                        mother: 'Brigitte',
                        father: 'Leon'
                    },
                    sister: 'Sien',
                    favoriteAnimals: ['monkey', 'tiger']
                };
                expect(frUtils.reverseDeepMerge(obj1, obj2)).to.eql({
                    firstName: 'Glenn',
                    name: 'Verschooren',
                    parents: {
                        mother: 'Marianne',
                        father: 'Marc'
                    },
                    sister: 'Sien',
                    favoriteAnimals: ['monkey', 'tiger']
                });
            });
       });

        describe('objAndSameType()', function() {
            it('should check if two object are from the same type', function() {
                var obj1 = {};
                var obj2 = {};
                var arr1 = [];
                expect(frUtils.objAndSameType(obj1, obj2)).to.be.true;
                expect(frUtils.objAndSameType(obj1, arr1)).to.be.false;
            });
        });

        describe('arrayify()', function() {
            it('should convert a string or object to an array', function() {
                var string = 'test';
                var object = {
                    key: 'test'
                };
                var integer = 8;
                expect(frUtils.arrayify(string)).to.eql(['test']);
                expect(frUtils.arrayify(object)).to.eql([{
                    key: 'test'
                }]);
                expect(frUtils.arrayify(integer)).to.eql([8]);
            });
        });

        describe('getProperties(), via deepGet(object, stringPath)', function(){
            it('ignores periods that are escaped', function(){
                var obj = {a: {b: {'name.prop': 3}}};
                expect(frUtils.deepGet(obj, 'a.b.name\\.prop')).to.equal(3);
            });

            it('ignores back-slashes that are escaped', function(){
                var obj = {a: {b: {'name\\\\\\prop': 3}}};
                expect(frUtils.deepGet(obj, 'a.b.name\\\\\\\\\\\\prop')).to.equal(3);
            });

            it('ignores brackets that are escaped', function(){
                var obj = {a: {b: {'name[p]rop': 3}}};
                expect(frUtils.deepGet(obj, 'a.b.name\\[p\\]rop')).to.equal(3);
            });

            it('uses brackets similar to array/variable property notation', function(){
                var obj = {a: {b: {'name.prop': [1, 2, 3]}}};
                expect(frUtils.deepGet(obj, 'a.b.name\\.prop[2]')).to.equal(3);
            });

            it('allows multiple braces on after the other', function(){
                var obj = {a: {b: {'name.prop': [[1, 2, 3]]}}};
                expect(frUtils.deepGet(obj, 'a.b.name\\.prop[0][2]')).to.equal(3);
            });

            describe('syntax errors', function(){
                it('should throw when there are periods inside the brackets', function(){
                    try{
                        frUtils.deepGet({}, 'a[2.0]');
                    }
                    catch(e){
                        console.log(e);
                        expect(e instanceof SyntaxError).to.equal(true);
                    }
                });

                it('should throw when there are left brackets inside the brackets', function(){
                    try{
                        frUtils.deepGet({}, 'a.b.c[00[2]');
                    }
                    catch(e){
                        expect(e instanceof SyntaxError).to.equal(true);
                    }
                });
            });
        });
    });

})(chai.expect, describe, it, angular, window.moment, window.helper);
