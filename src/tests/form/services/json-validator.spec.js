(function (expect, describe, it, angular, helper) {
    describe("Module: formRenderer.form -> Service: json validator", function () {

        var frSchemaValidator,
            data;

        beforeEach(module('data/json-validator-schema.json'));

        beforeEach(inject(function ($injector) {
            frSchemaValidator = helper.$get('frSchemaValidator');
            // angular is a bitch, we use copy because it's constants are aparently not constant
            data = angular.copy(helper.$get('dataJsonValidatorSchema'));
        }));

        it('should not error when validating a form schema with valid formId', function() {
            var validate = frSchemaValidator.validate;
            expect(validate.bind(this, data)).to.not.throw('INVALID SCHEMA: missing [form.formId]');
        });

        it('should error when validating a form schema without formId', function() {
            data.formId = undefined;
            var validate = frSchemaValidator.validate;
            expect(validate.bind(this, data)).to.throw('INVALID SCHEMA: missing [form.formId]');
        });


    });

})(chai.expect, describe, it, angular, window.helper);
