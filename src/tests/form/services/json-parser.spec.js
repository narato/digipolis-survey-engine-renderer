(function (expect, describe, it, angular, _, helper) {
    describe("Module: formRenderer.form -> Service: json parser", function () {

        var frSchemaParser,
            frConfig,
            data;

        beforeEach(module('data/json-parser-schema.json'));

        beforeEach(inject(function ($injector) {
            frSchemaParser = helper.$get('frSchemaParser');
            // angular is a bitch, we use copy because it's constants are aparently not constant
            data = angular.copy(helper.$get('dataJsonParserSchema'));
            frConfig = helper.$get('frFieldConfig');
        }));

        it('should add the steps property if not supplied', function() {
            var schema = frSchemaParser.parse(data);
            expect(schema).to.include.keys('steps');
        });

        it('should add the sections property if not supplied', function() {
            var schema = frSchemaParser.parse(data);
            expect(schema).to.include.keys('sections');
        });

        it('should add the fields property if not supplied', function() {
            var schema = frSchemaParser.parse(data);
            expect(schema).to.include.keys('fields');
        });

        describe('editMode and editable', function() {

            // should set the editable state and editmode to false for each secttion or field that lives inside a step with an editable state set to false

            it('should set the editable state and editMode to false for each secttion or field that lives inside a step with an editable state set to false', function() {
                var schema = frSchemaParser.parse(data);
                _.forEach(schema.steps, function (step) {
                    if (step.state.editable === false) {
                        _.forEach(step.sections, function (section) {
                            expect(section.state.editable).to.equal(false);
                            expect(section.state.editMode).to.equal(false);
                            _.forEach(section.fields, function (field) {
                                expect(field.state.editable).to.equal(false);
                                expect(section.state.editMode).to.equal(false);
                            });
                        });
                    }
                });
            });

            it('should set the editable state and editMode to false for each field that lives inside a section with an editable state set to false', function() {
                var schema = frSchemaParser.parse(data);
                _.forEach(schema.sections, function (section) {
                    if (!section.state.editable) {
                        _.forEach(section.fields, function (field) {
                            expect(field.state.editable).to.equal(false);
                            expect(field.state.editMode).to.equal(false);
                        });
                    }
                });
            });

            it('should show an edit button for each section that is editable and has an editMode set to false', function() {
                var schema = frSchemaParser.parse(data);
                _.forEach(schema.sections, function(section) {
                    if (section.state.editable && !section.state.editMode) {
                        expect(section.state.showEditButton).to.equal(true);
                    }
                });
            });

            it('should hide the edit button for each section that isn\'t editable or has the editMode state set to false', function() {
                var schema = frSchemaParser.parse(data);
                _.forEach(schema.sections, function(section) {
                    if (!section.state.editable || section.state.editMode) {
                        expect(section.state.showEditButton).to.equal(false);
                    }
                });
            });

            it('should hide the edit button for each field that is editable and has an edit mode set to true and has a section with a showEditButton state set to true', function() {
                var schema = frSchemaParser.parse(data);
                _.forEach(schema.sections, function (section) {
                    // showEditButton state is true when the sections is editable and the editMode was set to false
                    if (section.state.editable && !section.state.editMode) {
                        _.forEach(section.fields, function (field) {
                            expect(field.state.showEditButton).to.equal(false);
                        });
                    }
                });
            });

            it('should show an edit button for each each field that has no parent section or step with an editable state set to true and an editMode state set to false', function() {
                var schema = frSchemaParser.parse(data);
                _.forEach(schema.fields, function(field) {
                    if (field.state.editable === true && field.state.editMode === false) {
                        expect(field.state.showEditButton).to.equal(true);
                    }
                });
            });

        });

        describe('set field layout', function() {

            it('should set a fieldclass when a fieldLayout was given', function() {
                frConfig.setFieldLayout({
                    name: 'medium',
                    fieldClass: 'medium-test-class'
                });
                var schema = {
                    fields: [
                        {
                            name: "testField",
                            spec: {
                                attributes: {
                                    type: 'text'
                                },
                                options: {
                                    layout: {
                                        fieldLayout: 'medium'
                                    }
                                }
                            }
                        }
                    ]
                };
                schema = frSchemaParser.parse(schema);
                _.forEach(schema.fields, function(field) {
                    expect(field.spec.options.layout.fieldLayout).to.equal('medium');
                    expect(field.spec.options.layout.fieldClass).to.equal(' medium-test-class');
                });
            });

        });

    });

})(chai.expect, describe, it, angular, _, window.helper);
