(function (expect, describe, it, angular, helper) {
    describe("Module: formRenderer.form -> Service: model parser", function () {

        var frModelParser,
            data,
            $injector,
            frFieldConfigMock,
            frConfigMock;

        function $get(name) {
            return $injector.get(name);
        }

        beforeEach(function mocking() {
            frFieldConfigMock = {
                getType: function (name) {
                    var types = {
                        fieldgroup: {
                            modelType: 'object'
                        },
                        repeater: {
                            modelType: 'array'
                        }
                    };
                    return types[name];
                }
            };
            frConfigMock = {
                fieldConfig: {
                    modelType: {
                        OBJECT: 'object',
                        ARRAY: 'array'
                    }
                }
            };
        });

        beforeEach(function moduleLoading() {
            module('data/model-parser-schema.json');
            module('formRenderer', function ($provide) {
                $provide.factory('frFieldConfig', function() {
                    return frFieldConfigMock;
                });
                $provide.factory('frConfig', function() {
                    return frConfigMock;
                });
            });

        });

        beforeEach(function suiteArrange() {
            inject(function (_$injector_) {
                $injector = _$injector_;
            });
        });

        beforeEach(inject(function () {
            frModelParser = helper.$get('frModelParser');
            // angular is a bitch, we use copy because it's constants are aparently not constant
            data = angular.copy(helper.$get('dataModelParserSchema'));
        }));

        it('should create a model for each field inside a step', function() {
            var model = frModelParser.createModel(data);
            expect(model).to.include.keys('stepField');
        });

        it('should create a model for each field wrapped in a step and section', function() {
            var model = frModelParser.createModel(data);
            expect(model).to.include.keys('stepSectionField');
        });

        it('should create a model for each field inside section', function() {
            var model = frModelParser.createModel(data);
            expect(model).to.include.keys('sectionField');
        });

        it('should create a model for each field', function() {
            var model = frModelParser.createModel(data);
            expect(model).to.include.keys('field');
        });

        it('should create a model for each field inside field options (modelType: `array`)', function() {
            var model = frModelParser.createModel(data);
            expect(model).to.include.keys('fieldRepeater');
            var repeaterField = model.fieldRepeater;
            repeaterField.should.include.something.that.deep.equals({ fieldRepeater1: undefined, fieldRepeater2: undefined });
        });

        it('should create a model for each field inside field options (modelType: `object`)', function() {
            var model = frModelParser.createModel(data);
            expect(model).to.include.keys('contact');
            var fieldgroup = model.contact;
            expect(fieldgroup).to.include.keys('street', 'zipcode');
        });

    });

})(chai.expect, describe, it, angular, window.helper);
