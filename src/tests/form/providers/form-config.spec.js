(function (expect, describe, it, angular, helper) {
    describe("Module: formRenderer.form -> Provider: form config", function () {

        var frFormConfig,
            frFormConfigP;

        beforeEach(function() {
            module(function(frFormConfigProvider) {
                frFormConfigP = frFormConfigProvider;
            });
        });
        beforeEach(inject(function ($injector) {
            frFormConfig = helper.$get('frFormConfig');
        }));

        describe('Provider functions', function() {

            it('should have an extendConfig function', function () {
                console.log(frFormConfigP);
                expect(frFormConfig.extendConfig).to.exist;
            });

            it('should extend the default config with the new config', function() {
                frFormConfig.extendConfig({
                    scrollToAndFocusFirstErrorOnSubmit: false,
                    scrollAnimationTime: 800,
                    scrollOffset: -300
                });

                expect(frFormConfig.config.scrollToAndFocusFirstErrorOnSubmit).to.equal(false);
                expect(frFormConfig.config.scrollAnimationTime).to.equal(800);
                expect(frFormConfig.config.scrollOffset).to.equal(-300);
            });
        });

        describe('factory functions', function() {
            it('should have a default config object', function () {
                expect(frFormConfig.config).to.exist;
            });
        });
    });
})(chai.expect, describe, it, angular, window.helper);
