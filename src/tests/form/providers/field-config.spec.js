(function (expect, describe, it, angular, helper) {
    describe("Module: formRenderer.form -> Provider: field config", function () {

        var frConfig,
            frConfigProviderObject;

        beforeEach(function() {
            module(function(frFieldConfigProvider) {
                frConfigProviderObject = frFieldConfigProvider;
            });
        });
        beforeEach(inject(function ($injector) {
            frConfig = helper.$get('frFieldConfig');
        }));

        describe('get and set a field type', function() {
            it('should get a field by type', function() {
                expect(frConfig.setType({
                    name: 'text',
                    templateUrl: 'fr/modules/form/views/input/inputfield-text.html',
                    wrapper: 'frField'
                }));
                expect(frConfig.getType('text')).to.eql({
                    name: 'text',
                    templateUrl: 'fr/modules/form/views/input/inputfield-text.html',
                    wrapper: 'frField'
                });
            });

            it('should throw an error when we try to overwriting a type field', function() {
                var type = {
                    name: 'radio',
                    templateUrl: 'views/test.html'
                };
                //expect(frConfigProviderObject.setType(type)).to.throw;
            });

            describe('create new field without extend', function() {

                it('should create a new field type without defaultOptions or extends', function() {
                    var type = {
                        name: 'testType',
                        templateUrl: 'views/test.html'
                    };
                    var typeConf = frConfig.setType(type);
                    expect(typeConf).to.eql({
                        name: 'testType',
                        templateUrl: 'views/test.html'
                    });
                });

                it('should create a new type of inputfield when we call setType (given an Array)', function() {
                    var type = [{
                        name: 'testType1',
                        templateUrl: 'views/test.html'
                    },
                    {
                        name: 'testType2',
                        templateUrl: 'views/test2.html'
                    }];
                    var typeConf = frConfig.setType(type);
                    expect(typeConf).to.eql([{
                        name: 'testType1',
                        templateUrl: 'views/test.html'
                    },{
                        name: 'testType2',
                        templateUrl: 'views/test2.html'
                    }]);
                });
            });

            describe('create new field with extend', function() {
                it('should create a new field type and extend from an existing one', function() {
                    frConfig.setType([{
                            name: 'baseType',
                            templateUrl: 'views/baseType.html'
                        },
                        {
                            name: 'customField',
                            extend: 'baseType'
                        }
                    ]);
                    var customField = frConfig.getType('customField');
                    expect(customField).to.eql({
                        name: 'customField',
                        extend: 'baseType',
                        templateUrl: 'views/baseType.html'
                    });
                });

                it('should create a new field and extend a link function from an existing one', function() {

                    var textLinkFn = sinon.spy();
                    var extendTextLinkFn = sinon.spy();
                    frConfig.setType([
                        {
                            name: 'text',
                            templateUrl: 'views/text.html',
                            link: textLinkFn
                        },
                        {
                            name: 'extendText',
                            extend: 'text',
                            link: extendTextLinkFn
                        }
                    ]);

                    var field = frConfig.getType('text');
                    var extendField = frConfig.getType('extendText');
                    extendField.link.apply(this, ['1', '2', '3']);
                    field.link.apply(this, ['1', '2', '3']);
                    // the two link function should be called
                    textLinkFn.should.have.been.calledWith('1', '2', '3');
                    textLinkFn.should.have.been.calledTwice;
                    extendTextLinkFn.should.have.been.calledWith('1', '2', '3');

                });

                it('should create a new field and extend defaultOptions from an existing one', function() {
                    frConfig.setType([{
                            name: 'baseTypeEO',
                            templateUrl: 'views/baseTypeEO.html',
                            defaultOptions: {
                                attrs: {
                                    label: 'my custom label'
                                }
                            }
                        },
                        {
                            name: 'customFieldEO',
                            extend: 'baseTypeEO',
                            defaultOptions: {
                                attrs: {
                                    description: 'my custom description'
                                }
                            }
                        }
                    ]);
                    var customField = frConfig.getType('customFieldEO');
                    expect(customField).to.eql({
                        name:'customFieldEO',
                        extend: 'baseTypeEO',
                        templateUrl: 'views/baseTypeEO.html',
                        defaultOptions: {
                            attrs: {
                                label: 'my custom label',
                                description: 'my custom description'
                            }
                        }
                    });
                });

                it('should create a new field and extend defaultOptions(function) from an existing one', function() {
                    frConfig.setType([{
                            name: 'baseTypeEO',
                            templateUrl: 'views/baseTypeEO.html',
                            defaultOptions: function(opts) {
                                return {
                                    attrs: {
                                        label: 'my custom label'
                                    }
                                };
                            }
                        },
                        {
                            name: 'customFieldEO',
                            extend: 'baseTypeEO',
                            defaultOptions: function(opts) {
                                return {
                                    attrs: {
                                        description: 'my custom description'
                                    }
                                };
                            }
                        }
                    ]);
                    var customField = frConfig.getType('customFieldEO');
                    var defaultOptions = customField.defaultOptions();
                    expect(defaultOptions).to.eql({
                        attrs: {
                            label: 'my custom label',
                            description: 'my custom description'
                        }
                    });
                });
            });
        });

        describe('set and get a template wrapper', function() {
            it('should set an aray of new template wrappers', function() {
                var wrappers = frConfig.setTempWrapper([
                    {
                        name: 'defaultWrapper',
                        templateUrl: 'views/wrappers/default.html',
                        fieldTypes: ['radio', 'input', 'street']
                    },
                    {
                        name: 'selectWrapper',
                        templateUrl: 'view/wrappers/select.html',
                        fieldTypes: ['select']
                    }
                ]);

                expect(wrappers[0]).to.be.eql(frConfig.getTempWrapper('defaultWrapper'));
                expect(wrappers[1]).to.be.eql(frConfig.getTempWrapper('selectWrapper'));
            });

            it('should set only one new template wrapper', function() {
                var wrapper = frConfig.setTempWrapper({
                    name: 'defaultWrapper',
                    templateUrl: 'views/wrappers/default.html',
                    fieldTypes: ['radio', 'input', 'street']
                });
                expect(wrapper).to.be.eql(frConfig.getTempWrapper('defaultWrapper'));
            });

            it('should throw an error when fieldTypes in not an arry or string', function() {
                /*expect(frConfig.setTempWrapper({
                    name: 'defaultWrapper',
                    templateUrl: 'views/wrappers/default.html',
                    fieldTypes: { type: 'select' }
                })).to.throw(Error, 'trying to create a template wrapper with fieldTypes that is not an array of strings');*/
            });

            it('when no wrapper name was given a default one was set', function () {
                var wrapper = frConfig.setTempWrapper({
                    templateUrl: 'views/wrappers/default.html',
                    fieldTypes: ['radio', 'input', 'street']
                });
                // if no types or names are defined
                // the templateUrl is used by default for each field type
                var defaultWrapper = frConfig.setTempWrapper({
                    templateUrl: 'views/wrappers/default2.html'
                });

                expect(frConfig.getTempWrapperByType('radio')[0].name).to.equal('radio input street');
                expect(frConfig.getTempWrapper().name).to.equal('defaultWrapper');
            });
        });

        describe('set and get a fieldLayout', function() {

            it('should be possible to create a default field layout', function() {
                var defaultFieldLayout = frConfig.setFieldLayout({
                    fieldClass: 'default field class'
                });
                expect(defaultFieldLayout).to.be.eql(frConfig.getFieldLayout());
            });

            it('should set an array of fieldLayouts', function() {
                var fieldLayouts = frConfig.setFieldLayout([
                    {
                        name: 'small',
                        fieldClass: 'small'
                    },
                    {
                        name: 'medium',
                        fieldClass: 'medium'
                    }
                ]);
                expect(fieldLayouts[0]).to.be.eql(frConfig.getFieldLayout('small'));
                expect(fieldLayouts[1]).to.be.eql(frConfig.getFieldLayout('medium'));
            });

            it('should create a new fieldLayout by a given object', function() {
                 var fieldLayouts = frConfig.setFieldLayout({
                    name: 'small',
                    fieldClass: 'small'
                });
                expect(fieldLayouts).to.be.eql(frConfig.getFieldLayout('small'));
            });

        });
    });

})(chai.expect, describe, it, angular, window.helper);
