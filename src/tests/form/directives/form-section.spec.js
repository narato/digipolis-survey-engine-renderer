(function (describe, it, expect, sinon, _, helper) {
    'use strict';
    /*jshint expr:true*/

    describe('Module: formRenderer.form -> Directive: form-section', function () {

        var $injector,
            $controller,
            $compile,
            $scope,
            $rootScope,
            $templateCache,
            directive;


        var directiveHTML = [
            '<div fr-form-section="section" model="model">',
            '</div>'
        ].join('');

        function getCompiledElement(template, scope) {
            var element = angular.element(template);
            var compiledDirective = $compile(element)(scope);
            $scope.$digest();
            return {
                compiledHTML: compiledDirective,
                isolateScope: element.isolateScope(),
                controller: element.controller
            };
        }

        beforeEach(function suiteArrange() {
            inject(function (_$injector_, _$controller_, _$compile_, _$templateCache_) {
                $injector = _$injector_;
                $controller = _$controller_;
                $compile = _$compile_;
                $templateCache = _$templateCache_;
                $rootScope = helper.$get('$rootScope');

                $scope = $rootScope.$new();
                $scope.section = {
                    id: 'dummmySectionID',
                    title: 'dummy title',
                    fields: [],
                    state: {
                        editable: true,
                        editMode: true,
                        collapsible: true,
                        collapsed: false
                    }
                };
                $scope.model = {};

                directive = getCompiledElement(directiveHTML, $scope);
            });
        });

        describe('form-section controller', function() {

            it('should toggle the collapsed state when toggleCollapse is called and the section is collapsible', function () {
                directive.isolateScope.toggleCollapse();
                expect($scope.section.state.collapsed).to.equal(true);
            });

            it('should not be possible to toggle a section when it is not collapsible', function() {
                $scope.section.state.collapsible = false;
                $scope.$digest();
                directive.isolateScope.toggleCollapse();
                expect($scope.section.state.collapsed).to.equal(false);
            });

            it('should toggle the editmode when toggleEditmode was called', function() {
                directive.isolateScope.section.toggleEditMode();
                expect(directive.isolateScope.section.state.editMode).to.equal(false);
            });

            it('should expand the section when the editmode was toggled', function() {
                directive.isolateScope.section.state.collapsed = true;
                $scope.$digest();
                directive.isolateScope.section.toggleEditMode();
                expect(directive.isolateScope.section.state.collapsed).to.equal(false);
            });
        });

    });

})(describe, it, chai.expect, sinon, _, window.helper);
