(function (describe, it, expect, sinon, angular, helper) {
    'use strict';

    describe('Module: formRenderer.form -> Directive: frForm', function () {

        var $controller;
        var $rootScope;
        var $compile;
        var $scope;
        var element;
        var schema;
        var frConfig;

        function $getController($scope) {
            return $controller('frFormController', {
                '$anchorScroll': helper.$get('$anchorScroll'),
                '$location':  helper.$get('$location'),
                '$q':  helper.$get('$q'),
                '$scope': $scope,
                'frConfig': helper.$get('frConfig'),
                'frSchemaValidator': helper.$get('frSchemaValidator'),
                'frSchemaParser': helper.$get('frSchemaParser'),
                'frPrerequisitesParser': helper.$get('frPrerequisitesParser'),
                'frModelParser': helper.$get('frModelParser'),
                'frUtils': helper.$get('frUtils')
            });
        }

        // Load json data
        beforeEach(function loadJsonData() {
            module('data/default-form-schema.json');
        });

        beforeEach(function() {
            $rootScope = helper.$get('$rootScope');
            $compile = helper.$get('$compile');
            schema = angular.copy(helper.$get('dataDefaultFormSchema'));
            element = angular.element(
                '<fr-form schema="schema">' +
                '</fr-form>'
            );

            $scope = $rootScope;
            // set text field inside the form renderer
            helper.$setTypeText();
            $scope.schema = schema;
            $compile(element)($scope);
            $scope.$digest();
        });

        beforeEach(function suitArrange() {
            $controller = helper.$get('$controller');
            $rootScope = helper.$get('$rootScope');
        });

        // Include directive tests
        describe('frForm controller', function () {
            var scope, ctrl;

            beforeEach(function() {
                scope = $rootScope;
                $controller = helper.$get('$controller');
                ctrl = $getController(scope);
            });

            describe('getDataStore()', function () {

                it('should get a data source by name', function () {
                    var dataStore = ctrl.getDataStore('someStore');
                    expect(dataStore).to.deep.include({key: 'someKey', value: 'someValue'});
                });

                it('should return undefined when the data store does not exist', function () {
                    var dataStore = ctrl.getDataStore('undefinedStore');
                    expect(dataStore).to.be.undefined;
                });

            });

            describe('getFormId', function () {
                it('should get the form id', function () {
                    var formId = ctrl.getFormId();
                    expect(formId).to.be.equal('uniqueFormId');
                });
            });

            describe('getFormModel()', function () {
                it('should get a copy of the form model', function () {
                    var model = ctrl.getFormModel();
                    expect(model).to.deep.equal({name: 'Jon', firstName: '', 'e-mail': '', telephoneNumber: '+32'});
                });
            });

            describe('getPrerequisitesMatchFormModel()', function () {
                it('should get the model that matches the prerequisites', function () {
                    var model = ctrl.getPrerequisitesMatchFormModel();
                    // firstName is not matching the prerequisites
                    // email is matching the prerequisites but the value is empty.
                    // Empty values are not send back
                    expect(model).to.deep.equal({name: 'Jon', telephoneNumber: '+32'});
                });
            });

            describe('getPrerequisitesMatchStepModel()', function () {
                it('get the model from a step that matched the prerequisites', function () {
                    var model = ctrl.getPrerequisitesMatchStepModel('step1');
                    expect(model).to.deep.equal({telephoneNumber: '+32'});
                });
            });

            describe('getStepById()', function () {
                it('should get a form step by step id', function () {
                    var step = ctrl.getStepById('step0');
                    expect(step).to.be.an('object');
                    expect(step).to.have.property('id', 'step0');
                });
            });

            describe('getStepByIndex()', function () {
                it('should get a form step by the index', function () {
                    var step = ctrl.getStepByIndex(0);
                    expect(step).to.be.an('object');
                    expect(step).to.have.property('id', 'step0');
                });
            });

            describe('validateForm()', function () {
                it('should validate the form and return a promise', function (done) {
                    var validate = ctrl.validateForm();
                    validate.should.be.fulfilled;
                    done();
                });
            });
        });
    });
})(describe, it, chai.expect, sinon, angular, window.helper);
