// NOTE! THIS TEST FILE IS BROKEN AND NEEDS TO BE FIXED!!
// (function (expect, describe, it, angular) {
//     describe("Module: formRenderer.form -> Directive field", function () {
//
//         /**
//          * Load form renderer and form template modules
//          */
//         beforeEach(function(){
//             // mock the real template module
//             angular.module('formRenderer-templates', []);
//             module('formRenderer.utils');
//             module('formRenderer.form');
//             module('formRenderer.fieldTypes');
//             module('formRenderer');
//
//         });
//         beforeEach(module('formTemplates'));
//
//         var validHTML,
//             $scope,
//             $timeout,
//             $compile,
//             $document,
//             compiledDoc,
//             compiledDirective,
//             $doc,
//             element,
//             isolateScope;
//
//         validHTML = '<div ' +
//                     'fr-field ' +
//                     'ng-model="inputField.value" ' +
//                     'form="theForm"' +
//                     'options="inputField">';
//
//         /**
//          * Create new scope
//          * create new angular element
//          * compile element with his scope
//          * call the digest loop
//          */
//         beforeEach(inject(function($rootScope, _$compile_, $templateCache, $injector){
//             $timeout = $injector.get('$timeout');
//             $compile = _$compile_;
//             $document = $injector.get('$document');
//             $scope = $rootScope.$new();
//             // THIS IS A TEMP HACK!!!!!!!
//             $scope.$parent = $rootScope.$new();
//             $scope.$parent.$parent = $rootScope.$new();
//             $scope.$parent.$parent.$parent = $rootScope.$new();
//             $scope.$parent.$parent.$parent.theForm = "";
//             /////////////////////////////////////////////////
//
//             $scope.theForm = {};
//             $scope.inputField = {
//                 name: 'field',
//                 spec: {
//                     attributes: {
//                         type: 'text',
//                         placeholder: 'placeholder',
//                         value: ""
//                     },
//                     options: {
//                         label: 'my field',
//                         extraInfo: 'my description',
//                         valueOptions: [{
//                             key: 'yes',
//                             value: 'Yes'
//                         }, {
//                             key: 'no',
//                             value: 'No'
//                         }],
//                         layout: {
//                             fieldClass: 'span4 desptop--span-full tablet--span-full'
//                         }
//                     }
//                 },
//                 state: {
//                     editMode: true,
//                     editable: true
//                 },
//                 inputFilter: {
//                     required: true
//                 }
//             };
//
//             element = angular.element(validHTML);
//             compiledDirective = $compile(element)($scope);
//             $scope.$digest();
//             isolateScope = element.isolateScope();
//         }));
//
//
//
//         /**
//          * Utilities ----------------------------------------------------------------------
//          */
//         function getCompiledElement(template) {
//             var directive = $compile(angular.element(template))($scope);
//             $scope.$digest();
//             return directive;
//         }
//         //////////////////////////////// End untilities ////////////////////////////////////
//
//         /**
//          * Testing scope -------------------------------------------------------------------
//          */
//
//         it('`model` on isolated scope should be two-way bound', function() {
//             isolateScope.ngModel = 'test2';
//             $scope.$digest();
//             expect($scope.inputField.value).to.equal('test2');
//         });
//
//         ////////////////////////////// End testing scope ////////////////////////////////////
//
//
//         /**
//          * Testing Directive template ------------------------------------------------------
//          * We can verify if the template is applied on a directive
//          * and also if the template has certain elements or directives in it.
//          */
//
//         it('compile the html correctly', function () {
//             expect(compiledDirective.html()).to.not.equal('');
//         });
//
//         it('should print the label text in the html', function() {
//             var labelText = compiledDirective.find('label').text();
//             expect(labelText).to.equal('my field');
//         });
//
//         it('has a class `required` when the field is required', function() {
//             var hasClassRequired = compiledDirective.find('label').hasClass('required');
//             expect(hasClassRequired).to.be.true;
//         });
//
//         ////////////////////////////// End testing template ////////////////////////////////////
//
//         /**
//          * Testing Controller ----------------------------------------------------------
//          *
//          */
//
//
//     });
//
// })(chai.expect, describe, it, angular);
