(function(ng) {
    'use strict';
    // child modules
    ng.module('formRenderer.config', []);
    ng.module('formRenderer.utils', ['formRenderer.config']);
    ng.module('formRenderer.form', ['formRenderer.config', 'formRenderer.utils']);
    ng.module('formRenderer.validation', ['formRenderer.config']);

    // main module
    ng.module('formRenderer', ['ngSanitize', 'akit.service.date', 'formRenderer.form', 'formRenderer.validation', 'formRenderer-templates']);

})(window.angular);
