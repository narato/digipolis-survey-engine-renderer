(function(ng) {
    'use strict';
    ng.module('formRenderer.config').constant('frConfig', {
        enums: {
            navigation: {
                NEXT: 'next',
                PREVIOUS: 'previous',
                DIRECT: 'direct'
            },
            entities: {
                FORM: 'form',
                STEP: 'step',
                SECTION: 'section',
                FIELD: 'field'
            },
            stepTypes: {
                INTRO: 'intro',
                OUTRO: 'outro',
                REGULAR: 'regular'
            },
            events: {
                NAVIGATE: 'fr.navigateTo',
                NAVIGATE_STARTED: 'fr.navigateTo.started',
                NAVIGATE_COMPLETED: 'fr.navigateTo.completed',
                NAVIGATION_REDRAW: 'fr.navigation.redraw',
                DEBUG: 'fr.debug',
                SUBMIT: 'fr.onSubmit',
                SUBMIT_STARTED: 'fr.submit.started',
                SUBMIT_COMPLETED: 'fr.submit.completed',
                VALIDATE_ON_SUBMIT: 'validate_field_on_submit',
                VALIDATE_FIELD: 'validate_field',
                DATE_PICKED: 'frDatepicker.picketDate',
                FIELD_CHANGED: 'fr.field.changed',
                SECTION_TOGGLE_EDITMODE: 'fr.section.toggle.editMode'
            },
            errors: {
                FIELD_NOT_FOUND: 'The ID given does not match with any field in our form.'
            },
            debug: {
                SHOWNEXT: 'showNext',
                SHOWPREVIOUS: 'showPrevious',
                SHOWSAVE: 'showSave',
                SHOWSAVEDRAFT: 'showSaveDraft',
                MODEL: 'model'
            }
        },
        fieldConfig: {
            modelTypes: {
                OBJECT: 'object',
                ARRAY: 'array'
            }
        },
        defaultRequiredValidator: {
            "name": "required",
            "type": "required",
            "errorMessage": "this is a required field"
        },
        defaultEntities: {
            form: {
                "formId": "MissingFormId",
                "info": {
                    "title": "Missing form title"
                },
                "name": "Missing Form Name",
                "rendererVersion": "1",
                "canSaveDraft": false,
                "steps": [],
                "sections": [],
                "fields": [],
                "validators": [],
                "navigationTexts": {
                    "next": "Ga verder",
                    "previous": "Vorige",
                    "concept": "Opslaan als concept",
                    "submit": "Verzenden"
                }
            },
            field: {
                "name": "MissingFieldName",
                "spec": {
                    "attributes": {
                        "type": "text",
                        "value": null
                    },
                    "options": {
                        "label": "Missing Label Text",
                        "displayLabel": true,
                        "layout": {
                            "fieldClass": ""
                        }
                    }
                },
                "state": {
                    "editable": true,
                    "editMode": true,
                    "disabled": false,
                    "extraInfoCollapsed": true
                },
                "inputFilter": {
                    "validators": []
                },
                "prerequisites": {}
            },
            section: {
                "id": "MissingSectionId",
                "title": "Missing Section Title",
                "state": {
                    "editable": true,
                    "editMode": true,
                    "collapsible": false,
                    "collapsed": false
                },
                "fields": [],
                "prerequisites": {}
            },
            step: {
                "id": "MissingStepId",
                "title": "Missing Step Title",
                "type": "regular",
                "state": {
                    "showTitle": true,
                    "editable": true,
                    "editMode": true
                },
                "prerequisites": {},
                "sections": []
            }
        },
        defaultArrayPropertiesMap: {
            form: [
                "steps",
                "sections",
                "fields"
            ],
            step: [
                "sections",
                "fields",
                "prerequisites.sectionsCompleted.sections",
                "prerequisites.stepsCompleted.steps",
                "prerequisites.fieldsCompleted.fields",
                "prerequisites.fieldValues.operands"
            ],
            section: [
                "fields",
                "prerequisites.sectionsCompleted.sections",
                "prerequisites.stepsCompleted.steps",
                "prerequisites.fieldsCompleted.fields",
                "prerequisites.fieldValues.operands"
            ],
            field: [
                "prerequisites.sectionsCompleted.sections",
                "prerequisites.stepsCompleted.steps",
                "prerequisites.fieldsCompleted.fields",
                "prerequisites.fieldValues.operands",
                "spec.options.fileinfo.allowedExtensions",
                "spec.options.valueOptions",
                "spec.options.fields",
                "inputFilter.validators"
            ]
        }
    });
})(window.angular);
