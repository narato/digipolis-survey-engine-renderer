(function(ng, _) {
    'use strict';
    ng.module('formRenderer.validation').provider('frValidationConfig', [
        function frValidationConfigWrapper() {

            /**
             * Define validation expressions and
             * default messages
             * @type {Object}
             */
            var validators = {};

            /**
             * @function
             * @author Glenn Verschooren
             * @name add validator ---------------------------------
             * @description Allow user to set a custom validator and
             * default error message
             * @param {object/array} data - validator object
             */
            this.setValidator = function setValidator(data) {
                if (_.isArray(data)) {
                    var allValidators = [];
                    _.forEach(data, function(item) {
                        allValidators.push(setValidator(item));
                    });
                    return allValidators;
                } else if (_.isPlainObject(data)) {
                    // check required fields
                    if (checkRequiredProperties(data)) {
                        validators[data.type] = data;
                        return validators[data.type];
                    }
                } else {
                    throw "You must provide an object or array to add a validator:" + JSON.stringify(arguments);
                }
            };

            /**
             * @function
             * @author Glenn Verschooren
             * @name  check required properties ---------------------
             * @description check if all properties are there
             * @param  {Object} data - validator object
             * @return {Boolan}
             */
            function checkRequiredProperties(data) {
                var isValidObject = !!(data.type && data.expression && data.defaultMessage);
                if (!isValidObject) {
                    throw 'You must provide all required properties when creating a new validator. ' +
                    'type: ' + data.type + ', expression: ' + data.expression + ', defaultMessage: ' + data.defaultMessage;
                }
                return isValidObject;
            }

            /**
             * Public api -----------------------------------------------
             * @function
             * This function will be executed in the run phase
             */
            this.$get = function($http, $templateCache, $compile, frValidators) {

                // include all default validators
                this.setValidator(frValidators.getDefaultValidators());

                /**
                 * @function
                 * @author Glenn Verschooren
                 * @name  check form valid ----------------------------
                 * @description Check if a form is valid
                 * @param  {Object} form - angular form object
                 * @return {Boolean}
                 */
                this.checkFormValid = function checkFormValid(form) {
                    if (form.$valid === undefined) {
                        return false;
                    }
                    return (form && form.$valid === true);
                };

                /**
                 * @function
                 * @author Glenn Verschooren
                 * @name  get error message ----------------------------
                 * @description If there isn't a errorMessage we return the default message
                 * @param  {String} name - validator name
                 * @return {String}  - Error message
                 */
                this.getErrorMessage = function getErrorMessage(type) {
                    var validator = validators[type];
                    if (validator && !validator.errorMessage) {
                        return validator.defaultMessage;
                    } else {
                        return validator.errorMessage;
                    }
                };

                /**
                 * @function
                 * @author Glenn Verschooren
                 * @name  get validator ----------------------------------
                 * @description get the validator by type
                 * @param  {String} type - type of the validator
                 * @return {Object} - validator object
                 */
                this.getValidator = function getValidator(type) {
                    return validators[type];
                };

                /*
                    Public api -------------------------------------------
                    @type {Object}
                 */
                return {
                    getValidator: this.getValidator,
                    setValidator: this.setValidator,
                    getErrorMessage: this.getErrorMessage
                };

            };

        }
    ]);

})(window.angular, window._);