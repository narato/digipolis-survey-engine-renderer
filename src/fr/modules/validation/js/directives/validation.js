(function(ng, _) {
    'use strict';
    ng.module('formRenderer.validation').directive('frValidation', [
        '$q',
        '$timeout',
        'frValidationConfig',
        '$compile',
        '$parse',
        'frConfig',
        function frValidationDirective($q, $timeout, frValidationConfig, $compile, $parse, config) {

            var defaultRequiredValidator = config.defaultRequiredValidator;

            /**
             * @name inValidFunc --------------------------------------------------
             * @function
             * @description call when the field is invalid
             * @param  {Object} element
             * @param  {Object} scope
             * @param  {Object} ctrl
             */
            var inValidFunc = function validFunc(element, scope, ctrl) {
                ctrl.$setValidity(ctrl.$name, false);
                if (scope.invalidCallback) {
                    scope.invalidCallback(scope, {
                        message: scope.message
                    });
                }
            };

            /**
             * @name  validFunc ---------------------------------------------------
             * @function
             * @description call when the field is valid
             * @param  {Object} element
             * @param  {Object} scope
             * @param  {Object} ctrl
             */
            var validFunc = function validFunc(element, scope, ctrl) {
                ctrl.$setValidity(ctrl.$name, true);
                if (scope.validCallback) {
                    scope.validCallback(scope);
                }
            };

            /**
             * Check validators ---------------------------------------------------
             * @function
             * @param  {Object} scope
             * @param  {Object} element
             * @param  {Object} attrs
             * @param  {Object} ctrl
             * @param  {Object} validators
             * @param  {String} value
             * @return {Promise}
             */
            var checkValidators = function checkValidators(scope, element, attrs, ctrl, validation, value) {
                var validatorsConfig = [],
                    promises = [],
                    isValid = true,
                    defaultRequiredValidatorCP = _.cloneDeep(defaultRequiredValidator),
                    deferred = $q.defer();
                validation.error = [];

                prepareRequiredValidator(validation, defaultRequiredValidatorCP);
                // extend the validation configs with the validators on the field
                validatorsConfig = extendConfWithValidator(validation.validators);
                // push all promis functions in an array
                _.forEach(validatorsConfig, function(validatorConfig, index) {
                    promises.push(validatorConfig.expression(value, validatorConfig));
                });
                $q.all(promises).then(
                    function success(validatorConfig) {
                        isValid = true;
                        validFunc(element, scope, ctrl);
                        deferred.resolve(isValid);
                    },
                    function onError(validatorConfig) {
                        scope.message = validatorConfig.errorMessage ? validatorConfig.errorMessage : frValidationConfig.getErrorMessage(validatorConfig.type);
                        validation.error.push({
                            type: validatorConfig.type,
                            text: scope.message
                        });
                        // set error message
                        isValid = false;
                        inValidFunc(element, scope, ctrl);
                        deferred.reject(isValid);
                    }
                );

                return deferred.promise;
            };

            /**
             * @function
             * @name  prepareRequiredValidator --------------------------------------
             * @description - add a required validator to the validators lis when
             * the field is required and there isn't already defined one.
             * Remove the required validator when the field isn't required
             * @param  {Object} validation - validation object
             */
            function prepareRequiredValidator(validation, defaultRequiredValidator) {
                 if (validation.required ===  true) {
                    validation.validators = validation.validators || [];
                    var requiredValidatorAlreadyDefined = _.find(validation.validators, function(validator) { return validator.type === defaultRequiredValidator.type;});
                    if (validation.errorMessage && !requiredValidatorAlreadyDefined) {
                        defaultRequiredValidator.errorMessage = validation.errorMessage;
                    }
                    // chekc if there is a required validators
                    !requiredValidatorAlreadyDefined && validation.validators.push(defaultRequiredValidator);
                } else if (validation.required === false) {
                    var requiredValidatorIndex = _.findIndex(validation.validators, function(validator) {
                        return validator.type === defaultRequiredValidator.type;
                    });
                    if (requiredValidatorIndex > -1) {
                        validation.validators.splice(requiredValidatorIndex, 1);
                    }
                }
            }

            /**
             * @function
             * @name  extendConfWithValidators ------------------------------
             * @description extend validators config with the validators that
             * where set to the field
             * @param  {Array} validators - array of validator object
             * @return {Array} - extended array
             */
            function extendConfWithValidator(validators) {
                var validatorsConfig = [];
                _.forEach(validators, function(validator) {
                    if (frValidationConfig.getValidator(validator.type) === undefined) {
                        //throw 'there was no validator found with the type ' + validator.type;
                        // we return false because we only want to use the validators that we know about
                        return false;
                    }
                    var location = validatorsConfig.push(_.cloneDeep(frValidationConfig.getValidator(validator.type))); // make copy -> don't modify the config
                    _.assign(validatorsConfig[location -1], validator);
                });
                return validatorsConfig;
            }

            return {
                restrict: 'A',
                require: 'ngModel',
                scope: false,
                link: function frValidationLink(scope, element, attrs, ctrl) {

                    var model, validation;


                    /**
                     * @name initialize directive ------------------------
                     * @function
                     * @description check if all required attributes are available
                     * on the element
                     */
                    function initialize() {
                        if (!attrs.ngModel) {
                            console.error('The validation directive requires a ngModel attribute');
                            return false;
                        }
                        if (!attrs.validation) {
                            console.error('The validation directive requires a validation attribute');
                            return false;
                        }
                        model = $parse(attrs.ngModel);
                        validation = $parse(attrs.validation)(scope);
                        checkCallbacks();
                        watchModel();
                    }

                    /**
                     * @function
                     * @name  EVENT: validate_field_on_submit
                     */
                    scope.$on(config.enums.events.VALIDATE_ON_SUBMIT, function(event, fieldValidatorPromises) {
                        var value = ctrl.$modelValue;
                        var validatorPromise = checkValidators(scope, element, attrs, ctrl, validation, value);
                        validatorPromise.finally(function () {
                            scope.$emit(config.enums.events.FIELD_CHANGED, ctrl.$name);
                        });
                        fieldValidatorPromises.push(validatorPromise);
                    });

                    /**
                     * @function
                     * @name  EVENT: validate_field
                     */
                    scope.$on(config.enums.events.VALIDATE_FIELD, function validateField(event, fieldName, succes, error) {
                        if (ctrl.$name === fieldName) {
                            var value = ctrl.$viewValue;
                            var validatorPromise = checkValidators(scope, element, attrs, ctrl, validation, value);
                            validatorPromise.finally(function validationIsDone() {
                                scope.$emit(config.enums.events.FIELD_CHANGED, ctrl.$name);
                            });
                            validatorPromise.then(function validateSuccess(data) {
                                succes(data);
                            }, function validateError(data) {
                                error(data);
                            });
                        }
                    });

                    /**
                     * @name  checkCallbacks -----------------------------
                     * @function
                     * @description check if there are any callback defined
                     * on the attributes and make them available on the current
                     * scope
                     */
                    function checkCallbacks() {
                        if (attrs.validCallback) {
                            scope.validCallback =  $parse(attrs.validCallback);
                        }
                        if (attrs.invalidCallback) {
                            scope.invalidCallback = $parse(attrs.invalidCallback);
                        }
                    }

                    /**
                     * @name  watchModel --------------------------------
                     * @function
                     * @description watch the model and check if the model
                     * and check if the model is valid
                     */
                    function watchModel () {
                        scope.$watchCollection(model, function(value) {
                             // dirty, pristine, viewValue control here
                            // $pristine -> true when input is clean
                            if (ctrl.$pristine && !_.isEmpty(ctrl.$viewValue)) {
                                // model has value when initialized
                                ctrl.$setViewValue(ctrl.$viewValue);
                            } else if (ctrl.$pristine) {
                                // don't validate the form when the inut is clean
                                return;
                            }
                            // calling the validate function inside the controller
                            checkValidators(scope, element, attrs, ctrl, validation, value)
                                .finally(function () {
                                    scope.$emit(config.enums.events.FIELD_CHANGED, ctrl.$name);
                                });
                        });
                    }

                    initialize();

                }
            };
        }
    ]);
})(window.angular, window._);
