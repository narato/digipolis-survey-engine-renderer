(function(ng) {
   'use strict';
   ng.module('formRenderer.validation').directive('frMessages', [
        function frMessagesDirective() {
            return {
                restrict: 'AE',
                scope: {
                    messages: '=frMessages'
                },
                templateUrl: 'fr/modules/validation/views/messages.html'
            };
        }
    ]);
})(window.angular);