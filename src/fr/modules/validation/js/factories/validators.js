(function(ng) {
    'use strict';
    ng.module('formRenderer.validation').factory('frValidators', [
        '$q',
        'akit.DateService',
        function frValidatorsFactory($q, frDate) {

            /**
             * @name  Check required ------------------------------------
             * @function
             * @public
             * @param  {String} value - value inputfield
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkRequired(value, config) {
                var deferred = $q.defer();
                var result = true;

                if (value instanceof Array) {
                    if (value.length === 0) {
                        result = false;
                    }
                } else {
                    // 0 should be a valid value
                    result = value === 0 ? true : !!value;
                }

                result && deferred.resolve(config);
                !result && deferred.reject(config);

                return deferred.promise;
            }

            /**
             * @name  check match -------------------------------------
             * @description check if a value is matching a given pattern
             * The pattern can be a string or a RegExp javascript object
             * @function
             * @param  {String} value - value inputfield
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkMatch(value, config) {
                var regExp,
                    match,
                    deferred = $q.defer();

                if (!config.options) {
                    deferred.resolve(config);
                    return deferred.promise;
                }
                var pattern = config.options.pattern;
                if (pattern) {
                    // We can't check a regexp on a value when is is not set
                    // we skip the validator by reurning true
                    if (value === undefined || value === null || value === '') {
                        deferred.resolve(config);
                        return deferred.promise;
                    }
                    if (typeof value === 'object') {
                        console.warn('It is not possible to check a regExp on an object ' + JSON.stringify(value));
                        deferred.reject(config);
                        return deferred.promise;
                    }
                    if (typeof pattern === 'string') {
                        // if the pattern has a / on index 0
                        // remove it from the pattern
                        if (pattern.indexOf('\/') === 0) {
                            pattern = pattern.substring(1, pattern.length);
                        }
                        // if the pattern has a / on the last index
                        // remove it from the pattern
                        if (pattern.lastIndexOf('\/') === pattern.length - 1) {
                            pattern = pattern.substring(0, pattern.length - 1);
                        }
                        regExp = new RegExp(pattern);
                    }
                    else if (pattern instanceof RegExp) {
                        regExp = pattern;
                    }

                    match = String(value).match(regExp);
                    if ((match !== null)) {
                        deferred.resolve(config);
                    } else {
                        deferred.reject(config);
                    }
                    return deferred.promise;
                }
            }

            /**
             * Check Date --------------------------------------------
             * @description check if the date is valid
             * then the min/max properties
             *    compare: {
             *         addDays: '',
             *         Date: 'now/15/06/1988',
             *         operator: ''
             *      },
             *      before: '15/06/1988',
             *      after: '15/06/1988',
             *      hasPassed: true/false
             * @function
             * @param  {String} value - value inputfield
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkDate(value, config) {
                var result = false,
                    checked = false,
                    resultPassed,
                    deferred = $q.defer();

                if (!config.options) {
                    deferred.resolve(config);
                    return deferred.promise;
                }

                if (config.options.compare && !checked) {
                    checked = true;
                    result = checkDateCompare(value, config.options.compare);
                }

                if (config.options.before && !checked) {
                    checked = true;
                    result = frDate.isBefore(value, config.options.before);
                }

                if (config.options.after && !checked) {
                    checked = true;
                    result = frDate.isAfter(value, config.options.after);
                }

                if (config.options.hasPassed !== undefined && !checked) {
                    checked= true;
                    resultPassed = frDate.isPassed(value);
                    result = config.options.hasPassed ? resultPassed : !resultPassed;
                }

                if (result) {
                    deferred.resolve(config);
                } else {
                    deferred.reject(config);
                }
                return deferred.promise;
            }

            /**
             * @name  checkDateCompare ---------------------------------
             * @description Compare two dates with each other
             * @function
             * @param  {String} value - Date string
             * @param  {Objetc} compareObj - validation compare object
             * @return {Booelan}
             */
            function checkDateCompare(inputDate, compareObj) {
                // only check if there is a value
                if (inputDate === undefined || inputDate === null || inputDate === '') {
                    return true;
                }

                var compareDate, result = false;
                var options = {
                    addDays: parseInt(compareObj.addDays),
                    date: compareObj.date || 'now',
                    operator: compareObj.operator || '='
                };
                // parse a datestring to a JS object without time information -> if it fails return false
                compareDate = frDate.parseToDate(inputDate) ? frDate.parseDateYYMMDD(frDate.parseToDate(inputDate)) : false;
                if (options.date === 'now') {
                    options.date = frDate.parseDateYYMMDD(new Date(Date.now()));
                } else {
                    options.date = frDate.parseToDate(options.date) ? frDate.parseDateYYMMDD(frDate.parseToDate(options.date)) : false;
                }

                if (!compareDate || !options.date) {
                    return false;
                }

                if (options.addDays && frDate.validateDate(options.date)) {
                    options.date = new Date(options.date.getTime() + options.addDays * 24 * 60 * 60 * 1000);
                    options.date = frDate.parseDateYYMMDD(options.date);
                }

                switch (options.operator) {
                    case '>' :
                        result = (compareDate > options.date);
                        break;
                    case '>=' :
                        result = (compareDate >= options.date);
                        break;
                    case '<' :
                        result = (compareDate < options.date);
                        break;
                    case '<=' :
                        result = (compareDate <= options.date);
                        break;
                    case '=' :
                        // http://stackoverflow.com/questions/7606798/javascript-date-object-comparison
                        // comparing two objets in never the same, therefor we wrap them in a number function
                        result = (Number(compareDate) === Number(options.date));
                        break;
                }
                return result;
            }

            /**
             * Check Length --------------------------------------------
             * @description check if a value length is not smaler or higher
             * then the min/max properties
             * @function
             * @param  {String} value - value inputfield
             * @param  {Object} scope - the current scope
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkLength(value, config) {
                var min,
                    max,
                    length,
                    deferred = $q.defer();

                if (!config.options) {
                    deferred.resolve(config);
                    return deferred.promise;
                }

                min = config.options.min !== undefined ? config.options.min : 0;
                max = config.options.max !== undefined ? config.options.max : Number.POSITIVE_INFINITY;
                length = value ===  null ? 0 :value.length;

                if (length >= min && length <= max) {
                    deferred.resolve(config);
                } else {
                    deferred.reject(config);
                }
                return deferred.promise;
            }

            /**
             * @name  checkBetween -----------------------------------
             * @description Check if a given value is between a min
             * and max value. if min is undefined it will be set to zero
             * @function
             * @param  {String} value - value inputfield
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkBetween(value, config) {
                var deferred = $q.defer();
                if (value === undefined || value === null || value === '') {
                    deferred.resolve(config);
                }
                if (!config.options) {
                    deferred.resolve(config);
                    return deferred.promise;
                }

                var min = config.options.min !== undefined ? config.options.min : 0;
                var max = config.options.max !== undefined ? config.options.max : Number.POSITIVE_INFINITY;
                if (max === Number.POSITIVE_INFINITY) {
                    if ((parseInt(value, 10) >= parseInt(min, 10) && parseInt(value, 10) <= max)) {
                        deferred.resolve(config);
                    } else {
                        deferred.reject(config);
                    }
                } else {
                    if ((parseInt(value, 10) >= parseInt(min, 10) && parseInt(value, 10) <= parseInt(max, 10))) {
                        deferred.resolve(config);
                    } else {
                        deferred.reject(config);
                    }
                }
                return deferred.promise;
            }

            /**
             * @name  Get default validators ---------------------------
             * @function
             * @return {Array} Return all validator defenitions
             */
            this.getDefaultValidators = function getDefaultValidators() {
                return DefaultValidators;
            };

             /**
             * Default validators
             * @type {Array}
             */
            var DefaultValidators = [
                {
                    type: 'required',
                    expression: checkRequired,
                    defaultMessage: 'Dit veld is verplicht'
                },
                {
                    type: 'regexp',
                    expression: checkMatch,
                    defaultMessage: 'Dit veld is niet correct'
                },
                {
                    type: 'length',
                    expression: checkLength,
                    defaultMessage: 'dit veld heeft te weinig karakters'
                },
                {
                    type: 'number',
                    expression: checkMatch,
                    options: {
                        pattern: /^[0-9]+$/
                    },
                    defaultMessage: 'dit veld is geen nummer'
                },
                {
                    type: 'url',
                    expression: checkMatch,
                    options: {
                        pattern: /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
                    },
                    defaultMessage: 'vul een geldig url formaat in'
                },
                {
                    type: 'email',
                    expression: checkMatch,
                    options: {
                        pattern : /\S+@\S+\.\S+/
                    },
                    defaultMessage: 'vul een geldig email adres in'
                },
                {
                    type: 'between',
                    expression: checkBetween,
                    defaultMessage: 'De input moet zich tussen twee waarden bevinden'
                },
                {
                    type: 'date',
                    expression: checkDate,
                    defaultMessage: 'De datum is niet geldig'
                }
            ];

            // @type {Object}
            return {
                getDefaultValidators: this.getDefaultValidators
            };
        }
    ]);
})(window.angular);
