(function(ng, _) {
    'use strict';
    ng.module('formRenderer.utils').service('frUtils', [
        function frParseSchema() {

            /**
             * @function
             * @name  extenDeep --------------------------------------------
             * @description - the destination object will extend all properties
             * from the objects given in the arguments
             * @param  {Object} dst - destination object
             * @arguments
             * @return {Object}
             */
            function extendDeep(dest) {
                _.forEach(arguments, function(obj, index) {
                    // the first time we go trough this loop
                    // obj and dest will be the same so we don't need to go further
                    if (!index) {
                        return;
                    }
                    // console.log('extending with:', obj);
                    _.forEach(obj, function(value, key) {
                        // if we deal with an object -> call deep extend
                        if (objAndSameType(dest[key], value)) {
                            extendDeep(dest[key], value);
                        } else {
                            dest[key] = value;
                        }
                    });
                });
                return dest;
            }

            /**
             * @function
             * @name reverseDeepMerge ------------------------------------------
             * @description - only copy properties formm the src object
             * when the properties are not already defined in the destination object
             * @param  {Objet} dest - destination object
             * @return {Object}
             */
            function reverseDeepMerge(dest) {
                _.forEach(arguments, function(src, index) {
                    // the first time we go trough this loop
                    // obj and dest will be the same so we don't need to go further
                    if (!index) {
                        return;
                    }
                    _.forEach(src, function(val, prop) {
                        //Determines if a reference is defined.
                        // if not copy the src to the dest
                        // else check if we have an object from the same type
                        // call the reverse deep merge again!
                        if (!ng.isDefined(dest[prop])) {
                            dest[prop] = _.cloneDeep(val);
                        } else if (objAndSameType(dest[prop], val)) {
                            reverseDeepMerge(dest[prop], val);
                        }
                    });
                });
              return dest;
            }

            /**
             * @function
             * @name  objAndSameType ------------------------------
             * @description - check if two object are plain object and share
             * the same type
             * @param  {Object} obj1 - js Object
             * @param  {Object} obj2 - js Object
             * @return {Boolean}
             */
            function objAndSameType(obj1, obj2) {
                return _.isPlainObject(obj1) && _.isPlainObject(obj2) &&
                Object.getPrototypeOf(obj1) === Object.getPrototypeOf(obj2);
            }

            /**
             * @function
             * @name  arrayify -----------------------------------
             * @description create array from object
             * if no object is given return empty array
             * @param  {Object|String|Int} obj
             * @return {Array}
             */
            function arrayify(obj) {
                if (obj && !_.isArray(obj)) {
                    // if not array create one
                    obj = [obj];
                } else if (!obj) {
                    // if no object is given
                    // set empty array
                    obj = [];
                }
                return obj;
            }

            /**
             * @function
             * @name  deepIn -----------------------------------
             * @description - check if we can find the propertyPath in a collection
             * @param  {Object|Array} collection - object where we must find the given propertyPath
             * @param  {String} propertyPath - a string that describes a property path
             * @return {Boolean} - true if the path was found
             */
            function deepIn(collection, propertyPath) {
                var properties = getProperties(propertyPath);
                for(var i = 0; i < properties.length; i = i + 1){
                    var property = properties[i];
                    if(_.has(collection, property) ||
                        _.isObject(collection) && property in collection){
                        collection = collection[property];
                    }
                    else{
                        return false;
                    }
                }
                return true;
            }

            /**
             * @function
             * @name  deepGet ----------------------------------
             * @description - get The value from a property in a collection
             * @param  {Object|Array} collection
             * @param  {String} propertyPath - a string that describes a property path
             * @return {String|Int|object|Array} - return the value
             */
            function deepGet(collection, propertyPath) {
                var properties = getProperties(propertyPath);
                if (deepIn(collection, propertyPath)) {
                    return _.reduce(properties, function(object, property){
                        return object[property];
                    }, collection);
                }
            }

            /**
             * @function
             * @name  deepSet -----------------------------------
             * @description - Sets a value of a property in an object tree. Any missing objects/arrays will be created
             * @param {Object|Array} collection - The root object/array of the tree.
             * @param {string|Array} propertyPath - The propertyPath.
             * @param {*} value - The value to set.
             * @returns {Object} The object.
             */
            function deepSet(collection, propertyPath, value){
                var properties = getProperties(propertyPath);
                var currentObject = collection;
                _.forEach(properties, function(property, index){
                    if(index + 1 === properties.length){
                        currentObject[property] = value;
                    }
                    else if(!_.isObject(currentObject[property])){
                        currentObject[property] = isValidArrayKey(properties[index + 1]) ? [] : {};
                    }
                    currentObject = currentObject[property];
                });

                return collection;
            }

            /**
             * @function
             * @name getProperties ------------------------------
             * @description - get all properties from a property path
             * Look @ the parseStringPropertyPath for more information
             * @param  {String} propertyPath - a string that describes a property path
             * @return {Array} - It returns an array of strings
             */
            function getProperties(propertyPath) {
                if (_.isArray(propertyPath)) {
                    return propertyPath;
                }

                if (!_.isString(propertyPath)) {
                    return [];
                }

                return parseStringPropertyPath(propertyPath);
            }

            /**
             * @function
             * @name  parseStringPropertyPath --------------------
             * @description - parse a string that describes a property path.
             * @param  {String} propertyPath - a string that describes a property path
             * @return {array} - It returns an array of strings
             *
             * @example
             * object.property -> [ 'object', 'property'],
             * array[0].property -> [ 'array', '0', 'property']
             */
            function parseStringPropertyPath(propertyPath) {
                var parsedPropertyPath = [],
                    parsedPropertyPathPart = '',
                    escapeNextCharacter = false,
                    isSpecialCharacter = false,
                    insideBrackets = false;

                _.forEach(propertyPath, function forEachCharacter(character, index) {
                    isSpecialCharacter = ( (character === '\\') || (character === '[') || (character === ']') || (character === '.') );

                    if (isSpecialCharacter && !escapeNextCharacter) {
                        // don't allow a special characters inside brackets!
                        if (insideBrackets && character !== ']') {
                            throw new SyntaxError('unexpected ' + character + ' within brackets at character' + index + ' in property path' + propertyPath);
                        }

                        switch(character) {
                        case '\\':
                            escapeNextCharacter = true;
                        break;
                        case ']':
                            insideBrackets = false;
                            break;
                        case '[':
                            insideBrackets = true;
                            // push parsedPropertyPathPart when we find [ ore .
                            /* falls through */
                        case '.':
                            parsedPropertyPath.push(parsedPropertyPathPart);
                            // clear parsedPropertyPart after push
                            parsedPropertyPathPart = '';
                        break;
                        }
                    } else {
                        parsedPropertyPathPart = parsedPropertyPathPart + character;
                        escapeNextCharacter = false;
                    }
                });

                // The first index could be an empty string when the first character is [ or .
                // Remove the first index because we can't do anything with an empty string!
                // example: [0] or .0
                if (parsedPropertyPath[0] === '') {
                    parsedPropertyPath.splice(0, 1);
                }

                // push the final parsedPropertyPathPart
                parsedPropertyPath.push(parsedPropertyPathPart);
                return parsedPropertyPath;
            }

            /**
             * @function
             * @name  isValidArryKey ---------------------------------
             * @description - Checks whether key is a valid array key
             * @param key
             * @returns {boolean}
             */
            function isValidArrayKey(key){
                var array = [];
                array[key] = null;
                return array.length > 0;
            }

            /**
             * @return {Object} - public api
             */
            return {
                extendDeep: extendDeep,
                reverseDeepMerge: reverseDeepMerge,
                objAndSameType: objAndSameType,
                getProperties: getProperties,
                deepGet: deepGet,
                deepSet: deepSet,
                deepIn: deepIn,
                arrayify: arrayify,
                isValidArrayKey: isValidArrayKey
            };
        }
    ]);
})(window.angular, window._);
