(function(ng) {
    'use strict';
    ng.module('formRenderer.form').service('frSchemaValidator', [
        'frUtils',
        'frConfig',
        function frParseSchema(utils, config) {

            /**
             * @name  validateSchema -------------------------
             * @description validate the given json object
             * @param  {String} formSchema : formSchema to validate
             * @return {Array}
             */
            function validateSchema(formSchema) {
                // TODO: not implemented, later this could validate the given object against a schema, and return errors to the user.
                var errorsAndWarnings = [];

                // if there are errors, push em into the errorsAndWarnings obj
                // dummy example
                if (formSchema.formId === undefined) {
                    throw new Error('INVALID SCHEMA: missing [form.formId]');
                }

                // and throw em to the top
                if (errorsAndWarnings.length > 0) {
                    var errors = errorsAndWarnings.join('\n');
                    throw new Error('INVALID SCHEMA: ' + errors);
                }

                return formSchema;
            }

            return {
                validate: validateSchema
            };
        }
    ]);
})(window.angular);
