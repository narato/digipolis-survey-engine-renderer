(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').service('frModelParser', [
        'frUtils',
        'frFieldConfig',
        'frConfig',
        function frParseModel(utils, frFieldConfig, frConfig) {

            /**
             * @function
             * @name createModel -----------------------------------
             * @description generate a model object that we pass to all
             * fields in the form
             * @param  {Object} formSchema
             * @return {Object} Model
             */
            function createModel(formSchema) {
                formSchema._internalCache = formSchema._internalCache || {};
                var model = formSchema._internalCache.formModel = {};

                if (ng.isDefined(formSchema.steps)) {
                    // go trough all steps and find fields or sections
                    _.forEach(formSchema.steps, function(step) {
                        var stepsModel = createModel(step);
                        for (var index in stepsModel) {
                            model[index] = stepsModel[index];
                        }
                    });
                }

                if (ng.isDefined(formSchema.sections)) {
                    _.forEach(formSchema.sections, function(section) {
                       var sectionModel = createModel(section);
                        for (var index in sectionModel) {
                            model[index] = sectionModel[index];
                        }
                    });
                }

                if (ng.isDefined(formSchema.fields)) {
                    _.forEach(formSchema.fields, function(field) {
                        var subFields = field.spec.options.fields,
                            fieldType = field.spec.attributes.type,
                            fieldOptions = field.spec.options;

                        // check if there are nested fields available
                        if (ng.isDefined(subFields)) {
                            var fieldConfig = frFieldConfig.getType(fieldType);
                            // modelType is required to render a correct model
                            if (!fieldConfig.modelType) {
                                throw 'You must provide a known modelType for field type: ' + fieldType;
                            }
                            switch(fieldConfig.modelType) {
                                case frConfig.fieldConfig.modelTypes.OBJECT:
                                    // create an object and assign it to the model
                                    model[field.name] = createModel(fieldOptions);
                                    break;
                                case frConfig.fieldConfig.modelTypes.ARRAY:
                                    // create an array and assign it to the model
                                    // so we can create repeatable field groups
                                    var value = field.spec.attributes.value;
                                    model[field.name] = [];
                                    if (_.isArray(value) && !_.isEmpty(value)) {
                                        model[field.name] = value;
                                    } else {
                                        model[field.name].push(createModel(fieldOptions));
                                    }
                                    break;
                                default:
                                    // throw an error when someone is trying to set a modelType we don't know about!
                                    var error = [
                                        'You must provide a known modelType for field type: ' + fieldType + '.\n',
                                        ' Known system types: ' + JSON.stringify(frConfig.fieldConfig.modelTypes) + '\n',
                                        ' You provide: ' + JSON.stringify(fieldConfig.modelType)
                                    ].join('');
                                    throw error;
                            }
                        } else {
                            model[field.name] = field.spec.attributes.value;
                        }
                    });
                }
                return model;
            }

            return {
                createModel: createModel
            };
        }
    ]);
})(window.angular, window._);
