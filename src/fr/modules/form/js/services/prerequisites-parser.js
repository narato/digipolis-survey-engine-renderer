(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').service('frPrerequisitesParser', [
        'frUtils',
        'frConfig',
        function frParseSchema(utils, config) {
            // Preprocessare Prerequisites -----------------------------------

            // this should not be using the config.js entity names,
            // yes these are constants, but if someone renames an entity in the config
            // it will look for other properties than the 4 below, in the json Schema
            // this will not work as the schema only uses these specs.
            var entityNames = {
                FIELD: 'field',
                STEP: 'step',
                SECTION: 'section',
                FORM: 'form'
            };

            var _cache;

            /**
             * @name  parseSchema -------------------------
             * @description parse the given json object and extend it to include all necessary properties
             * @param  {String} schema : schema to parse
             * @return {Object} the parsed schema;
             */
            function parseSchema(schema) {
                _processEntities(schema);

                return schema;
            }

            /**
             * @name  _processEntities -------------------------
             * @private
             * @description itterate over all entities, and build their dependency cache
             * @param  {Object} schema : the formSchema to be parsed
             * @return {Object} item : the extended item.
             */
            function _processEntities(schema) {
                _cache = schema._internalCache;

                _.forEach([
                    entityNames.STEP,
                    entityNames.SECTION,
                    entityNames.FIELD
                ], function (entityType) {
                    for (var i = 0; i < _cache[entityType].length; i += 1) {
                        _prepEntity(_cache[entityType][i], _cache);
                    }
                });

                _.forEach([
                    entityNames.STEP,
                    entityNames.SECTION,
                    entityNames.FIELD
                ], function (entityType) {
                    testMultipleEntities(_cache[entityType]);
                });

                return schema;
            }

            /**
             * @name  _prepEntity -------------------------
             * @private
             * @description process an entity's prerequisites and add them to this item's dependency cache.
             * @param  {Object} item : the item to be parsed
             * @return {Object} item : the extended item.
             */
            function _prepEntity(item) {
                if (item.prerequisites) {
                    var prerequisites = item.prerequisites;

                    _.forEach([
                        entityNames.STEP,
                        entityNames.SECTION,
                        entityNames.FIELD
                    ], function (entityType) {
                        if (prerequisites[entityType + 'sCompleted'] && prerequisites[entityType + 'sCompleted'][entityType + 's']) {
                            _.forEach(prerequisites[entityType + 'sCompleted'][entityType + 's'], function (subjectId) {
                                var subject = _cache[entityType + 'ById'][subjectId];
                                // TODO throw error when subject is undefined
                                subject._prerequisitesDependencies = subject._prerequisitesDependencies || [];
                                subject._prerequisitesDependencies.push(item);
                            });
                        }
                    });

                    // cached for all completed prerequisites, now do it for the field values too...
                    if (prerequisites.fieldValues && prerequisites.fieldValues.operands) {
                        _.forEach(prerequisites.fieldValues.operands, function (operand) {
                            var lastProperyInPath = _.last(utils.getProperties(operand.name));
                            var subject = _cache[entityNames.FIELD + 'ById'][lastProperyInPath];
                            // TODO throw error when subject is undefined
                            subject._prerequisitesDependencies = subject._prerequisitesDependencies || [];
                            subject._prerequisitesDependencies.push(item);
                        });
                    }
                }
                return item;
            }

            function testMultipleEntities(entities) {
                _.forEach(entities, testEntity);
                indexSteps();
            }

            function testEntity(entity, loopIndex) {
                // take the entity's prerequisites
                var prereq = entity.prerequisites;
                var valid = true;


                if (!_.isEmpty(prereq.fieldValues)) {
                    valid = (handleFieldValues(prereq.fieldValues)) ? valid : false;
                }
                if (!_.isEmpty(prereq.stepsCompleted)) {
                    valid = (handleStepsComplete(prereq.stepsCompleted)) ? valid : false;
                    // console.log('handleStepsComplete', valid);
                }
                if (!_.isEmpty(prereq.sectionsCompleted)) {
                    valid = (handleSectionsComplete(prereq.sectionsCompleted)) ? valid : false;
                    // console.log('handleSectionsComplete', valid);
                }
                if (!_.isEmpty(prereq.fieldsCompleted)) {
                    valid = (handleFieldsCompleted(prereq.fieldsCompleted)) ? valid : false;
                    // console.log('handleFieldsComplete', valid);
                }

                entity._prerequisitesMatch = valid;

                // if this method is called from a loop of more entities being tested,
                // skip the reindex (it will be done there)
                if (!loopIndex) {
                    indexSteps();
                }
            }

            function indexSteps() {
                var index = 0;
                _.forEach(_cache.step, function (step, i) {
                    step._originalStepIndex = i;
                    if (step._prerequisitesMatch) {
                        step._stepIndex = index;
                        index += 1;
                    } else {
                        step._stepIndex = undefined;
                    }
                });
            }

            function getField(fieldName) {
                return _cache.fieldById[fieldName];
            }

            function handleFieldValues(fieldsReq) {
                var fieldsToCheck = fieldsReq.operands;
                var shouldTestLogical = fieldsToCheck.length > 1;

                var checkField = function checkField(fieldReq) {
                    var field = utils.deepGet(_cache.formModel, fieldReq.name);
                    return testOperator(fieldReq.operator, field, fieldReq.value);
                };

                if (shouldTestLogical) {
                    var map = _.map(fieldsToCheck, function (field) {
                        return checkField(field);
                    });

                    if (fieldsReq.logical === 'AND') {
                        var notAllTrue = _.contains(map, false);
                        return !notAllTrue;
                    } else {
                        var hasATrue = _.contains(map, true);
                        return hasATrue;
                    }
                } else {
                    return checkField(fieldsToCheck[0]);
                }
            }

            function handleStepsComplete(stepsReq) {
                // TODO: implement handleStepsComplete test
                return true;
            }

            function handleSectionsComplete(sectionsReq) {
                // TODO: implement handleSectionsComplete test
                return true;
            }

            function handleFieldsCompleted(fieldsReq) {
                // TODO: implement handleFieldsCompleted test
                return true;
            }

            function testOperator (operator, fieldValue, value) {
                var fv = _.cloneDeep(fieldValue);
                fv = _.contains([null, undefined], fv) ? "" : fv;
                return operators[operator](fv, value);
            }

            var operators = {
                '==': function equals(value, test) {
                    return String(value) === String(test);
                },
                '!=': function notEquals(value, test) {
                    return String(value) !== String(test);
                },
                '<': function lowerThan(value, test) {
                    return value < test;
                },
                '>': function higherThan(value, test) {
                    return value > test;
                },
                '<=': function lowerThanOrEquals(value, test) {
                    return value <= test;
                },
                '>=': function higherThanOrEquals(value, test) {
                    return value >= test;
                },
                'in': function contains(value, test) {
                    return _.contains(value, test);
                },
                '!in': function contains(value, test) {
                    return !_.contains(value, test);
                }
            };

            return {
                parse: parseSchema,
                testMultiple: testMultipleEntities,
                test: testEntity,
                indexSteps: indexSteps
            };
        }
    ]);
})(window.angular, window._);
