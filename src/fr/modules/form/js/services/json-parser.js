(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').service('frSchemaParser', [
        'frFieldConfig',
        'frUtils',
        'frConfig',
        function frParseSchema(fieldConfig, utils, config) {
            // Default types -----------------------------------

            var defaultFormSchema = config.defaultEntities.form;
            var defaultStepSchema = config.defaultEntities.step;
            var defaultSectionSchema = config.defaultEntities.section;
            var defaultFieldSchema = config.defaultEntities.field;

            // this should not be using the config.js entity names,
            // yes these are constants, but if someone renames an entity in the config
            // it will look for other properties than the 4 below, in the json Schema
            // this will not work as the schema only uses these specs.
            var entityNames = {
                FIELD: 'field',
                STEP: 'step',
                SECTION: 'section',
                FORM: 'form'
            };

            /**
             * @name resetCache
             * @private
             * @description reset the internal cache before every schema parse
             * @param  {Object} schema : the schema to recieve cache
             * @return {Object} schema : the spiked schema
             */
            function resetCache(schema) {
                schema._internalCache = {};

                schema._internalCache[entityNames.FIELD] = [];
                schema._internalCache[entityNames.FIELD + 'ById'] = {};
                schema._internalCache[entityNames.STEP] = [];
                schema._internalCache[entityNames.STEP + 'ById'] = {};
                schema._internalCache[entityNames.SECTION] = [];
                schema._internalCache[entityNames.SECTION + 'ById'] = {};

                return schema;
            }

            /**
             * @name  parseSchema -------------------------
             * @description parse the given json object and extend it to include all necessary properties
             * @param  {String} formSchema : formSchema to parse
             * @return {Object} the parsed schema;
             */
            function parseSchema(formSchema) {
                resetCache(formSchema);
                var cache = formSchema._internalCache;

                // extend form
                var schema = utils.extendDeep(_.cloneDeep(defaultFormSchema), formSchema);
                // Fix Array property type when it was not given as an array
                schema = parseArrayProperties(entityNames.FORM, schema);
                // extend steps
                schema.steps = extendSubs(entityNames.STEP, defaultStepSchema, schema.steps, cache);
                // extend sections
                schema.sections = extendSubs(entityNames.SECTION, defaultSectionSchema, schema.sections, cache);
                // extend fields
                schema.fields = extendSubs(entityNames.FIELD, defaultFieldSchema, schema.fields, cache);

                // parse editmode, editable and showEditButton states
                schema = parseStates(schema);

                parseFieldValueOptions(cache);

                schema.steps = _.map(schema.steps,
                    _.curry(setDefaultButtonTexts)(schema.navigationTexts));

                return schema;
            }

            /**
             * @function
             * @name parseFieldValueOptions ------------------
             * @description - Convert each key property inside the valueOptions to a string
             * NOTE!: This is not something we want to do inside the formRenderer.
             *        It is not possible to use integers as a value for the key property inside the valueOptions Array.
             *        Check this example for more information : http://jsfiddle.net/vft3L1tb/29/
             * @param  {Object} cache
             */
            function parseFieldValueOptions(cache) {
                _.forEach(cache.field, function (field) {
                    if (_.isArray(field.spec.options.valueOptions)) {
                        field.spec.options.valueOptions = _.map(field.spec.options.valueOptions, function (option) {
                            option.key = _(option.key).toString();
                            return option;
                        });
                    }
                });
            }

            /**
             * @function
             * @name  parseStates ----------------------------
             * @description - parse all state objects
             * @param  {Object} schema - form schema
             * @return {Object} - parsed schema
             */
            function parseStates(schema) {
                // check steps
                checkEditableMode(entityNames.STEP, schema.steps);
                // check sections
                checkEditableMode(entityNames.SECTION, schema.sections);
                // check fields
                checkEditableMode(entityNames.FIELD, schema.fields);

                return schema;
            }

           /**
            * @function
            * @name  setDefaultButtonTexts -----------------------
            * @description - check and set the correct default button texts if not given
            * @param  {Object} step  - step to which we set button texts
            // */
            function setDefaultButtonTexts(defaults, step) {
                // save given texts over defaults
                step.navigationTexts = _.extend(_.clone(defaults), step.navigationTexts);
                return step;
            }

           /**
            * @function
            * @name  checkEditableMode -----------------------
            * @description - check if all sections and fields have a valid editmode
            * and editable state. If not, make sure the states are valid before we give
            * the form schema to the frForm directive
            * @param  {String} entityType   - type : step, section and field
            * @param  {array} items         - steps, sections, fields
            * @param  {Object} parentState  - parent state object
            */
            function checkEditableMode(entityType, items, parentState) {
                _.forEach(items, function(item, index) {

                    // handle defaults: when a step is passed into this method, parentState is undefined
                    parentState = parentState || {
                        editable: true,
                        editMode: true,
                        showEditButton: false
                    };

                    // set state
                    item.state.editable = (parentState.editable) ? item.state.editable : false;
                    item.state.editMode = (parentState.editMode && item.state.editable) ? item.state.editMode : false;
                    item.state.showEditButton = (item.state.editable && !item.state.editMode && !parentState.showEditButton);
                    // if item has children (any type), process them too
                    item.sections && checkEditableMode(entityNames.SECTION, item.sections, item.state);
                    item.fields && checkEditableMode(entityNames.FIELD, item.fields, item.state);
                    if (item.spec && item.spec.options && item.spec.options.fields) {
                        // fields in fields should use the same parent state as the parentField.
                        checkEditableMode(entityNames.FIELD, item.spec.options.fields, parentState);
                    }
                });
            }

            /**
             * @name  extendSubs -------------------------
             * @private
             * @description recursivly run over this step, extend it on the default object and check it's children if they need to be parsed.
             * @param  {String} entityType : The entity's type in order do something specific with a certain type.
             * @param  {String} defaultSchema : default Object to be extended by the incomming items.
             * @param  {Array} items : items to be looped over, each item will extended by a defaultSchema, and checked if it has children that need extension.
             * @param  {Object} cache : the easy access cache readily available on the form for easy access to steps sections and fields
             * @return {Array} items : the extended array list.
             */
            function extendSubs(entityType, defaultSchema, items, cache) {
                for (var i = 0; i < items.length; i += 1) {
                    var item = utils.extendDeep(_.cloneDeep(defaultSchema), items[i]);

                    // Fix Array property type when it was not given as an array
                    item = parseArrayProperties(entityType, item);

                    item = parsePrerequisites(item);

                    if (entityType === entityNames.FIELD) {
                        setFieldLayout(item);
                    }

                    if (entityType === entityNames.STEP && item.sections) {
                        item.sections = extendSubs(entityNames.SECTION, defaultSectionSchema, item.sections, cache);
                    }

                    if (item.fields) {
                        item.fields = extendSubs(entityNames.FIELD, defaultFieldSchema, item.fields, cache);
                        setFieldLayout(item.fields);
                    }

                    if (entityType === entityNames.FIELD && item.spec && item.spec.options && item.spec.options.fields) {
                        item.spec.options.fields = extendSubs(entityNames.FIELD, defaultFieldSchema, item.spec.options.fields, cache);
                        setFieldLayout(item.spec.options.fields);
                    }

                    cache[entityType].push(item);
                    // use only item name when entity type is a field
                    if (entityType !== entityNames.FIELD) {
                        cache[entityType + 'ById'][item.id] = item;
                    } else {
                        cache[entityType + 'ById'][item.name] = item;
                    }

                    items[i] = item;
                }
                return items;
            }

            /**
             * @function
             * @name  setFieldLayout ----------------------------
             * @description - get the fieldLayout config when there is one set.
             * concat the fieldClass properties.
             * @param {Array|object} field - field object or fields array
             */
            function setFieldLayout(field) {
                if (_.isArray(field)) {
                    _.forEach(field, function (item) {
                        setFieldLayout(item);
                    });
                } else {
                    field.spec.options.layout = field.spec.options.layout || {
                        fieldClass: ''
                    };
                    var confFieldLayout,
                        layout = field.spec.options.layout;

                    confFieldLayout = fieldConfig.getFieldLayout(layout.fieldLayout);

                    // if we can find the field layout in the config
                    // concat fieldClass string with the configFieldClass
                    if (confFieldLayout && _.isString(confFieldLayout.fieldClass)) {
                        layout.fieldClass = layout.fieldClass + ' ' + confFieldLayout.fieldClass;
                    }
                }
            }

            function parsePrerequisites(item) {
                if (item.prerequisites === null || item.prerequisites === undefined) {
                    item.prerequisites = {};
                }
                return item;
            }

            /**
             * @function
             * @name  parseArrayProperties ----------------------------
             * @description - Check array properties for their type, if not an array wrap them in one.
             * @param  {String} entityType : The entity's type in order do something specific with a certain type.
             * @param  {Object} entity : the entity in which properties need to be checked for proper array type.
             */
            function parseArrayProperties(entityType, entity) {
                // TODO: fix arrays
                var propertyMap = config.defaultArrayPropertiesMap[entityType];

                if (!propertyMap) {
                    return entity;
                }

                _.forEach(propertyMap, function checkArrayProperties(propertyPath) {
                    // find path in entity
                    if (utils.deepIn(entity, propertyPath) && !Array.isArray(utils.deepGet(entity, propertyPath))) {
                        utils.deepSet(entity, propertyPath, [utils.deepGet(entity, propertyPath)]);
                    }
                });

                return entity;
            }

            return {
                parse: parseSchema
            };
        }
    ]);
})(window.angular, window._);
