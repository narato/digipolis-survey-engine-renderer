(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').provider('frFieldConfig', [
        function frConfigProvider() {

            /**
             * @name  get ------------------------------------
             * @description this function will return an object
             * that will be used as a public api
             * @return {Object} Public api
             */
            this.$get = function(frUtils, $timeout) {

                var typeMap = {},
                    tempWrapperMap = {},
                    fieldLayoutMap = {},
                    defaultTempWrapperName = 'defaultWrapper',
                    defaultFieldLayoutName = 'defaultFieldLayout';

                /**
                 * @name Set type -----------------------------
                 * @param {Object|Array} options type object
                 */
                function setType(options) {
                    if (_.isArray(options)) {
                        var allTypes = [];
                        _.forEach(options, function(item) {
                            allTypes.push(setType(item));
                        });
                        return allTypes;
                    } else if (_.isPlainObject(options)) {

                        if (options.extend) {
                            extendFieldTypeOptions(options);
                            typeMap[options.name] = options;
                            return typeMap[options.name];
                        }

                        if (!checkOverwrite(options.name, typeMap, options, 'types')) {
                            typeMap[options.name] = options;
                            return typeMap[options.name];
                        }
                    } else {
                        throw 'You must provide an object or array for setType. You provide:' + JSON.stringify(arguments);
                    }
                }

                /**
                 * @function
                 * @name  setTemplateWrapper ---------------------------
                 * @description set a template wrapper
                 * @param {object|array} options - template wrapper options
                 */
                function setTempWrapper(options) {
                    // check if array
                    if (_.isArray(options)) {
                        return options.map(function(wrapperOptions) {
                            return setTempWrapper(wrapperOptions);
                        });
                    } else if (_.isPlainObject(options)) {
                        options.fieldTypes = getOptionsFieldTypes(options);
                        options.name = getOptionsWrapperName(options);
                        checkTempWrapperTypes(options);
                        tempWrapperMap[options.name] = options;
                        return options;
                    }
                }

                /**
                 * @function
                 * @name  setFieldLayout -------------------------------
                 * @description - set a fieldLayout
                 * @param {Object} options - fieldLayout options
                 */
                function setFieldLayout(options) {
                    if (_.isArray(options)) {
                        var allLayouts = [];
                        _.forEach(options, function (item) {
                            allLayouts.push(setFieldLayout(item));
                        });
                        return allLayouts;
                    } else if (_.isPlainObject(options)) {
                        if (!checkOverwrite(options.name, fieldLayoutMap, options, 'fieldLayout')) {
                            options.name = getOptionsFieldLayoutName(options);
                            fieldLayoutMap[options.name] = options;
                            return fieldLayoutMap[options.name];
                        }
                    } else {
                        throw 'You must provide an object or array for setFieldLayout. You provide:' + JSON.stringify(arguments);
                    }
                }

                /**
                 * @function
                 * @name getTempWrapper
                 * @description get tempWrapper object by name
                 * if no name was provided the default wrapper will be
                 * returned
                 * @param  {string} name - wrapper name
                 * @return {Object} - tempWrapper object
                 */
                function getTempWrapper(name) {
                    return tempWrapperMap[name || defaultTempWrapperName];
                }

                /**
                 * @function
                 * @name  getWrapperByFieldType -------------------------
                 * @description get all wrappers by type
                 * @param  {String} fieldType - field type name
                 * @return {array} - array of field wrappers
                 */
                function getTempWrapperByType(fieldType) {
                    var wrappers = [];
                    for (var name in tempWrapperMap) {
                        if (tempWrapperMap.hasOwnProperty(name)) {
                            // check if there are fieldTypes set
                            // if so check if there is an index that has this fieldType
                            if (tempWrapperMap[name].fieldTypes && tempWrapperMap[name].fieldTypes.indexOf(fieldType) !== -1) {
                                wrappers.push(tempWrapperMap[name]);
                            }
                        }
                    }
                    return wrappers;
                }

                /**
                 * @function
                 * @name getOptionsWrapperName ----------------------------
                 * @description get wrapper name (fallback defaultTempWrapperName)
                 * @param  {object|array} options - wrapper options
                 * @return {string} - tempWrapperName
                 */
                function getOptionsWrapperName(options) {
                    return options.name || options.fieldTypes.join(' ') || defaultTempWrapperName;
                }

                /**
                 * @function
                 * @name  getOptionsFieldLayoutName ------------------------
                 * @description - get field layout name (fallback defaultFieldLayoutName)
                 * @param  {object} options - fieldLayout options
                 * @return {String} - fieldLayoutName
                 */
                function getOptionsFieldLayoutName(options) {
                    return options.name || defaultFieldLayoutName;
                }

                 /**
                 * @name  Get type object ----------------------------------
                 * @description searcg for a typeMap by a given name
                 * and return it
                 * @return {object}
                 */
                function getType(name, throwError, errorContext) {
                    if (!name) {
                        return undefined;
                    }
                    var type = typeMap[name];
                    if (!type && throwError === true) {
                        throw 'There is no type by the name of' + name;
                    } else {
                        return type;
                    }
                }

                /**
                 * @function
                 * @name  getFieldLayout ------------------------------------
                 * @description - get field layout by name
                 * @param  {String} name - fieldLayout name
                 * @return {Object} - fieldLayout
                 */
                function getFieldLayout(name) {
                    return fieldLayoutMap[name || defaultFieldLayoutName];
                }

                /**
                 * @function
                 * @name  extendTypeOptions --------------------------------
                 * @param  {Object} options - type options
                 */
                function extendFieldTypeOptions(options) {
                    var extendType = getType(options.extend, true, options);
                    extendFieldTypeDefaultOptions(options, extendType);
                    extendFieldTypeController(options, extendType);
                    extendFieldTypeLink(options, extendType);
                    frUtils.reverseDeepMerge(options, extendType);
                }

                /**
                 * @function
                 * @name  extendFieldTypeController ------------------------
                 * @description check if we have to extend any controller function
                 * on the extend object.
                 * @param  {Object} options - field type options
                 * @param  {Object} extendType - extend field type options
                 */
                function extendFieldTypeController(options, extendType) {
                    var extendCtrl = extendType.controller;
                    // return when the extendType object has no controller
                    if (!ng.isDefined(extendCtrl)) {
                        return;
                    }
                    var optionsCtrl = options.controller;
                    // when we have a controller in the options object
                    // and on the extendObject -> we have to call both
                    // controllers
                    if (ng.isDefined(optionsCtrl)) {
                        options.controller = function($scope, $controller) {
                            $controller(extendCtrl, {$scope: $scope});
                            $controller(optionsCtrl, {$scope: $scope});
                        };
                        // inject scope and controller service
                        options.controller.$inject = ['$scope', '$controller'];
                    } else {
                        // if there is no controller on the options object
                        // take the on one from the extend object
                        options.controller = extendCtrl;
                    }
                }

                /**
                 * @function
                 * @name  extendFieldTypeLink -------------------------------
                 * @description check if we have to extend any link function
                 * on the extend object
                 */
                function extendFieldTypeLink(options, extendType) {
                    var extendLink = extendType.link;
                    // return when there is no extend function
                    // available on the extend type
                    if (!ng.isDefined(extendLink)) {
                        return;
                    }
                    var optionsLink = options.link;
                    // if there is a link function on the
                    // options object we have to overwrite
                    // the link function. call both link functions
                    if (ng.isDefined(optionsLink)) {
                        options.link = function() {
                            // call extendLink and optionsLink
                            // and spread the arguments
                            extendLink.apply(null, _toArray(arguments));
                            optionsLink.apply(null, _toArray(arguments));
                        };
                    } else {
                        // call original extendLink function
                        options.link = extendLink;
                    }
                }

                /**
                 * @function
                 * @name  _toArray ------------------------------------------
                 * @description convert to array
                 * @param  {array} arr
                 * @return {array}
                 */
                var _toArray = function (arr) {
                    return Array.isArray(arr) ? arr : [].slice.call(arr);
                };

                /**
                 * @function
                 * @name  extendFieldTypeDefaultOptions ---------------------
                 * @description - extend all default options from the base field Type
                 * check if the defaultOptions property on the extend type
                 * or the new field type is a function, if so we have to make sure
                 * the options propperty in the callback has all extended properties
                 * and that is should merge the two when we call (defaultOptions) on the
                 * field type that extends
                 * @param  {Object} options - Field type options
                 * @param  {Object} extendsType - extend field type options
                 * @return {Function} - It returns a function if options.defaultOptions or
                 * extendsType.defaultOptions is a function
                 */
                function extendFieldTypeDefaultOptions(options, extendsType) {
                    var extendDefaultOptions = extendsType.defaultOptions;
                    // if there are no default options defined
                    // do nothing
                    if (!ng.isDefined(extendDefaultOptions)) {
                        return;
                    }
                    var optionsDefaultOptions = options.defaultOptions;
                    var optionsDefaultOptionsIsFn = _.isFunction(optionsDefaultOptions);
                    var extendDefaultOptionsIsFn = _.isFunction(extendDefaultOptions);
                    // if the extend default options is a function
                    if (extendDefaultOptionsIsFn) {
                        // create new defaultOPtions function
                        options.defaultOptions = function defaultOptions(opts, scope) {
                            // get object from the baseType
                            var extendDO = extendDefaultOptions(opts, scope);
                            var mergedDO = {};
                            // merge all options + the default options on the
                            // extended object
                            frUtils.reverseDeepMerge(mergedDO, opts, extendDO);
                            // check if the defaultOptions on the new type is a function
                            var extenderOptionsDefaultOptions = optionsDefaultOptions;
                            if (optionsDefaultOptionsIsFn) {
                                // call the defaultOptions function from our new field type
                                extenderOptionsDefaultOptions = extenderOptionsDefaultOptions(mergedDO, scope);
                            }
                            // merge the field type default options on the base object with the
                            // default options from the new type
                            //frUtils.extendDeep(extendDO, extenderOptionsDefaultOptions);
                            frUtils.reverseDeepMerge(extendDO, extenderOptionsDefaultOptions);
                            return extendDO;
                        };
                    } else if (optionsDefaultOptionsIsFn) {
                        // all properties on the base field type should be available
                        // on the new field
                        options.defaultOptions = function defaultOptions(opts, scope) {
                            var newDefaultOptions = {};
                            //frUtils.extendDeep(newDefaultOptions, opts, extendDefaultOptions);
                            frUtils.reverseDeepMerge(newDefaultOptions, opts, extendDefaultOptions);
                            return optionsDefaultOptions(newDefaultOptions, scope);
                        };
                    }
                }

                /**
                 * @function
                 * @name  getOptionsFieldTypes -------------------------
                 * @description - set the fieltype param in the right format
                 * and return it
                 * @param  {string|array} options - wrapper options
                 * @return {Array}
                 */
                function getOptionsFieldTypes(options) {
                    // if string return array
                    if (_.isString(options.fieldTypes)) {
                        return [options.fieldTypes];
                    }
                    // if not defined return empty array else
                    // return field types
                    if (!ng.isDefined(options.fieldTypes)) {
                        return [];
                    } else {
                        return options.fieldTypes;
                    }
                }

                /**
                 * @function
                 * @name  checkTempWrapperTypes
                 * @description - check if the wrapper types have
                 * the correct format
                 * @param  {object|array} options - wrapper options
                 */
                function checkTempWrapperTypes(options) {
                    var error = !_.isArray(options.fieldTypes);
                    _.forEach(options.fieldTypes, function(fieldType) {
                        if (!error) {
                            if (!_.isString(fieldType)) {
                                error = true;
                            }
                        }
                    });
                    if (error) {
                        throw 'trying to create a template wrapper with fieldTypes that is not an array of strings';
                    }
                }

                /**
                 * @name check overwrite -----------------------
                 * @description check if a given object has already a property
                 * defined so we don't overwrite it!
                 * @param  {String} property : property to check
                 * @param  {Object} object
                 * @param  {Object} newValue
                 * @return {Boolean}
                 */
                function checkOverwrite(property, object, newValue, type) {
                    if (object && object.hasOwnProperty(property)) {
                        console.error([
                            'overwriting-' + type + ': Attempting to overwriting ' + property + ' on ' + type + ' wich in currently ',
                            JSON.stringify(object[property]) + ' with ' + JSON.stringify(newValue)
                        ].join(''));
                        return true;
                    }
                    return false;
                }

                return {
                    getType: getType,
                    setType: setType,
                    setTempWrapper: setTempWrapper,
                    getTempWrapper: getTempWrapper,
                    getTempWrapperByType: getTempWrapperByType,
                    setFieldLayout: setFieldLayout,
                    getFieldLayout: getFieldLayout
                };
            };
        }
    ]);
})(window.angular, window._);
