(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').provider('frFormConfig', [
        function frFormConfigProvider() {

            var config = {
                scrollToAndFocusFirstErrorOnSubmit: true,
                scrollAnimationTime: 200,
                scrollOffset: -100,
                formClass: '',

                modelOutput: {
                    onSubmit: {
                       PrerequisitesMatch: false,
                    },
                    onNavigate: {
                        PrerequisitesMatch: false,
                    }
                }
            };

            /**
             * @function
             * @name extendConfig ------------------------------
             * @description - extend newConfig with default config
             * @param {Object} newConfig Config object
             */
            function extendConfig(newConfig) {
                config = _.assign(config, newConfig);
            }

            return {
               $get: function() {
                   return {
                        extendConfig: extendConfig,
                        config: config
                   };
               }
            };
        }
    ]);
})(window.angular, window._);
