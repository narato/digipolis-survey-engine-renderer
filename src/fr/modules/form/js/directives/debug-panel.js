(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frDebug', [
        '$timeout',
        function frDebugDirective($timeout) {
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/debug-panel.html',
                replace: true,
                scope: {
                    data: '=',
                    active: '=',
                    activeStep: '=',
                    activeStepIndex: '=',
                    schema: '=',
                    form: '='
                },
                link: function(scope, element, attrs, ctrl) {
                    
                    scope.state = {
                        debug: true
                    };
                       
                    /**
                     * @function
                     * @name toggle state --------------------------------
                     * @description - toggle state debug panel
                     * @param {String} name - state name
                     */
                    scope.toggleState = function (name) {
                        scope.state = scope.state || {};
                        scope.state[name] = !scope.state[name];
                    };
                    
                    /**
                     * @function
                     * @name toggle state --------------------------------
                     * @description - creates a json string from the data and return it
                     * @param {Object} data - print data
                     * @return {String}
                     */
                    scope.printData = function (data) {
                        var returnvalue = JSON.stringify(data, null, 2);
                        return returnvalue;
                    };
                }
            };
        }
    ]);
})(window.angular);
