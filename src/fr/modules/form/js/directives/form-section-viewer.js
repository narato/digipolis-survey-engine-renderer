(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormSectionViewer', [
        function frFormSectionViewerDirective(){
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/form-section-viewer.html',
                scope: {
                    section: '=frFormSectionViewer',
                    model: '='
                },
                controller: 'frFormSectionController',
            };
        }
    ]);
})(window.angular);
