(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormNav', [
        'frConfig',
        function frFormNavDirective(config) {
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/form-nav.html',
                scope: {
                    steps: '=frFormNav',
                    active: '=',
                    activeStep: '=',
                    activeStepIndex: '=',
                    canSaveAsConcept: '='
                },
                require: '^frForm',
                link: function(scope, element, attrs, ctrl) {

                    function setVisibility () {
                        scope.showSaveDraft = scope.canSaveAsConcept || false;
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWSAVEDRAFT, scope.showSaveDraft);
                        
                        /** @type {Object} */
                        scope.buttonState = {
                            disabled: false
                        };

                        if (!scope.steps || scope.steps.length === 0) {
                            // no steps, entire form on the page at once. show save button below.
                            scope.showNext = false;
                            scope.showPrevious = false;
                            scope.showSave = true;

                            scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWNEXT, scope.showNext);
                            scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWPREVIOUS, scope.showPrevious);
                            scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWSAVE, scope.showSave);

                            scope.navigation = config.defaultEntities.form.navigationTexts;

                            // get out, we're done here
                            return;
                        }

                        // there are steps, decide whether to show next and or previous buttons.
                        var steps = _.cloneDeep(scope.steps || []);

                        // test if current step is outro (then hide every button)
                        var current = scope.activeStepIndex || 0;
                        var currentStep = ctrl.getStepByIndex(current);
                        scope.navigation = currentStep.navigationTexts;

                        // filter out steps that do not have prerequisites met
                        // navigation should not factor in steps that are hidden because of their prerequisites
                        steps = _.filter(steps, function (step) {
                            return step._prerequisitesMatch;
                        });

                        // filter out steps that are outro
                        steps = _.filter(steps, function (step) {
                            return step.type !== config.enums.stepTypes.OUTRO;
                        });

                        // calculate the firstStep lastStep and the current.
                        var lastIndex = (steps.length === 0) ? 0 : steps.length - 1;
                        var firstIndex = 0;
                        var notOutro = currentStep.type !== config.enums.stepTypes.OUTRO;
                        var notIntro = currentStep.type !== config.enums.stepTypes.INTRO;

                        // assign the proper state values
                        scope.showNext = notOutro && (current < lastIndex);
                        scope.showPrevious = notOutro && (current !== firstIndex);
                        scope.showSave = notOutro && (current === lastIndex);
                        scope.showSaveDraft = scope.canSaveAsConcept && notOutro && notIntro;

                        // log them to the debug window
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWNEXT, scope.showNext);
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWPREVIOUS, scope.showPrevious);
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWSAVE, scope.showSave);
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWSAVEDRAFT, scope.showSaveDraft);
                    }

                    function isNextStep(currentStep) {
                        var steps = _.cloneDeep(scope.steps);
                        // TODO: don't just take index
                        return !!steps[currentStep + 1];
                    }

                    function isPreviousStep(currentStep) {
                        var steps = _.cloneDeep(scope.steps);
                        // TODO: don't just take index
                        return !!steps[currentStep - 1];
                    }

                    scope.save = function save (form) {
                        scope.$emit(config.enums.events.SUBMIT, {});
                    };

                    scope.saveDraft = function saveDraft (form) {
                        scope.$emit(config.enums.events.SUBMIT, {
                            draft: true
                        });
                    };

                    scope.next = function next () {
                        if (isNextStep(scope.activeStepIndex)) {
                            // Make the new step active
                            scope.$emit(config.enums.events.NAVIGATE, {
                                stepId: ctrl.getStepByIndex(scope.activeStepIndex + 1).id,
                                direction: config.enums.navigation.NEXT
                            });
                        }
                    };

                    scope.previous = function previous () {
                        // go back
                        if (isPreviousStep(scope.activeStepIndex)) {
                            // Make the new step active
                            scope.$emit(config.enums.events.NAVIGATE, {
                                stepId: ctrl.getStepByIndex(scope.activeStepIndex - 1).id,
                                direction: config.enums.navigation.PREVIOUS
                            });
                        }
                    };

                    /** on navigation started */
                    scope.$on(config.enums.events.NAVIGATE_STARTED, function navigateStarted() {
                        // disable submit button
                        scope.buttonState.disabled =  true;
                    });

                    /** on navigation completed */
                    scope.$on(config.enums.events.NAVIGATE_COMPLETED, function navigateCompleted() {
                        // enable submit button
                        scope.buttonState.disabled =  false;
                    });

                    /** on submit started */
                    scope.$on(config.enums.events.SUBMIT_STARTED, function submitStarted() {
                        // disable submit button
                        scope.buttonState.disabled =  true;
                    });

                    /** on submit completed */
                    scope.$on(config.enums.events.SUBMIT_COMPLETED, function submitCompleted(event, isResolved) {
                        // enable submit button
                        scope.buttonState.disabled =  false;
                        // only check if there is a next step when the SUBMIT is resolved
                        // Stay on the current step when te submit promise is rejected
                        if (isResolved) {
                            scope.next();
                        }
                    });

                    /** on redraw navigation completed */
                    scope.$on(config.enums.events.NAVIGATION_REDRAW, function submitCompleted(event, isResolved) {
                        setVisibility();
                    });

                    scope.$watch('activeStepIndex', setVisibility);
                    scope.$watch('steps', setVisibility);
                }
            };
        }
    ]);
})(window.angular, window._);
