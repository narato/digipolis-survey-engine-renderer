(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').directive('frField', [
        '$interpolate',
        '$q',
        '$http',
        '$compile',
        '$templateCache',
        'frFieldConfig',
        'frUtils',
        function frInputfieldDirective($interpolate, $q, $http, $compile, $templateCache, frFieldConfig, frUtils) {
            return {
                restrict: 'AE',
                scope: {
                    options: '=',
                    ngModel: '=',
                    fields: '=?'
                },
                require: ['^frForm', '^ngModel'],
                replace: true,
                controller: 'frFieldController',
                link: function linkInputfield(scope, element, attrs, ctrl) {

                    var type;
                    var args = arguments;
                    var fieldCount = 0;
                    var fr = this;

                    /**
                     * @function
                     * @name  initialize ---------------------------
                     * @description - initialize field directive
                     */
                    function initialize() {
                        type = frFieldConfig.getType(scope.options.spec.attributes.type);
                        var fieldTemplate;
                        getFieldTemplate(scope.options.spec).then(function (template) {
                            fieldTemplate = template;
                            //console.log('transclude in temp wrappers');
                            return transcludeInTempWrappers(scope.options.spec);
                        }).then(function (transcludeTemplate) {
                            //console.log('set element template');
                            return transcludeTemplate(fieldTemplate).then(function(data) {
                                return setElementTemplate(data);
                            });
                        }).then(function (templateString) {
                            return watchFormControl(templateString);
                        }).then(function() {
                            //console.log('call link functions');
                            return callLinkFunctions();
                        }).catch(function error(err) {
                            throw new Error('there was a problem setting the template for this field');
                        });
                    }

                    /**
                     * @function
                     * @name  getFieldTemplates --------------------
                     * @description check if a templateUrl was set
                     * then load the template
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function getFieldTemplate(options) {
                        var type = frFieldConfig.getType(options.attributes.type);
                        var templateUrl = type.templateUrl;
                        var template = type.template;
                        if (_.isUndefined(template) && !templateUrl) {
                            throw 'no template found for ' + options.attributes.type + ' field type';
                        }

                        return getTemplate(templateUrl || template, _.isUndefined(template), options);
                    }

                    /**
                     * @function
                     * @name  getTemplate --------------------------
                     * @description get the field template
                     * @param  {Function|string} template - the template url
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function getTemplate(template, isUrl, options) {
                        var templatePromise;

                        if(_.isFunction(template)) {
                            templatePromise = $q.when(template(options));
                        } else {
                            templatePromise = $q.when(template);
                        }

                        if (!isUrl) {
                            return templatePromise;
                        } else {
                            var httpOptions = { cache: $templateCache };
                            return templatePromise.then(function(url) {
                                return $http.get(url, httpOptions);
                            }).then(function(response) {
                                return response.data;
                            }).catch(function handlingErrorGettingATemplate(error) {
                                console.warn('problem loading template');
                            });
                        }
                    }

                    /**
                     * @function
                     * @name  transcludeInTempWrappers ----------------
                     * @description tansclude all wrapper templates and the
                     * field template (wrapper 0 -> wrappers 1 -> field template)
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function transcludeInTempWrappers(options) {
                        var wrappers = getTempWrapperOptions(options);

                        return function transcludeTemplate(template) {
                            if (!wrappers.length) {
                                // nothing to transclude
                                // return field template promise
                                return $q.when(template);
                            }

                            var promises = [];
                            _.forEach(wrappers, function(wrapper) {
                                promises.push(getTemplate(wrapper.templateUrl, true));  
                            });

                            return $q.all(promises).then(function(wrappersTemplates) {
                                wrappersTemplates.forEach(function(wrapperTemplate, index) {
                                    checkTempWrapper(wrapperTemplate, wrappers[index]);
                                });
                                // wrapper 0 is wrapped in wrapper 1...
                                wrappersTemplates.reverse();
                                var totalWrapper = wrappersTemplates.shift();
                                _.forEach(wrappersTemplates, function(wrapperTemplate) {
                                    totalWrapper = doTransclusion(totalWrapper, wrapperTemplate);
                                });
                                return doTransclusion(totalWrapper, template);
                            });
                        };
                    }

                    /**
                     * @function
                     * @name  doTransclusion --------------------------
                     * @description search for 'fr-transclude' and replace it with
                     * the given template
                     * @param  {String} wrapper - template wrapper
                     * @param  {String} template - template that will replace the 'fr-transclude' tag
                     * @return {String} - return replaced html
                     */
                    function doTransclusion(wrapper, template) {
                        var superWrapper = ng.element('<a></a>'); // allow single root in wrappers
                        superWrapper.append(wrapper);
                        var transcludeEl = superWrapper.find('fr-transclude');
                        transcludeEl.replaceWith(template);
                        return superWrapper.html();
                    }

                    /**
                     * @function
                     * @name checkTempWrapper ------------------------
                     * @description check if there is a '<fr-transclude>' tag
                     * available in the wrapper
                     * @param  {string} template - wrapper template
                     * @param  {object} info - field info
                     */
                    function checkTempWrapper(template, info) {
                        var frTransclude = '<fr-transclude></fr-transclude>';
                        if (template.indexOf(frTransclude) === -1) {
                            throw new Error('could not find <fr-transclude></fr-transclude> in the wrapper template. info: ' + JSON.stringify(info));
                        }
                    }

                    /**
                     * @function
                     * @name  setElementTemplate ----------------------
                     * @description set element html and $compile it with
                     * the scope
                     * @param {string} templateString - html string
                     */
                    function setElementTemplate(templateString) {
                        element.html(asHtml(templateString));
                        $compile(element.contents())(scope);
                        return templateString;
                    }

                    /**
                     * @function
                     * @name  getTempWrapperOptions --------------------
                     * @description get all wrappers that should be rendered
                     * before we render the field
                     * @param  {object} options - field options
                     * @return {Array} - all wrappers
                     */
                    function getTempWrapperOptions(options) {
                        // get all wrappers by type
                        var wrapper = frUtils.arrayify(frFieldConfig.getTempWrapperByType(options.attributes.type));

                        // get all wrappers specified in the type object
                        var type = frFieldConfig.getType(options.attributes.type);
                        if (type && type.wrapper) {
                            var newTypeWrappers = [];
                            var typeWrappers = frUtils.arrayify(type.wrapper);
                            _.forEach(typeWrappers, function(wrapperName) {
                                newTypeWrappers.push(frFieldConfig.getTempWrapper(wrapperName));
                            });
                            _.forEach(newTypeWrappers, function(newWrapper) {
                                wrapper.push(newWrapper);
                            });
                            typeWrappers = newTypeWrappers;
                        } else if (type.wrapper === null) {
                            // explicit null on in field config means no wrapper
                            return [];
                        }

                        // add the default wrapper
                        var defaultTempWrapper = frFieldConfig.getTempWrapper();
                        if (defaultTempWrapper) {
                            wrapper.push(defaultTempWrapper);
                        }

                        return wrapper;
                    }

                    /**
                     * @function
                     * @name  watchFormControl --------------------------------
                     * @description find all models inside the field template
                     * check if the model exists and watch for changes
                     * set the formControl on the field options object when a change
                     * has occurred
                     * @param  {String} templateString
                     */
                    function watchFormControl(templateString) {
                        var templateElement = ng.element(templateString);
                        var models = templateElement[0].querySelectorAll('[ng-model], [data-ng-model]');

                        _.forEach(models, function (model) {
                            fieldCount = fieldCount + 1;
                            watchFieldNameOrExistence(model.getAttribute('name'));
                        });
                    }

                    /**
                     * @function
                     * @name  watchFieldNameOrExistence ------------------------
                     * @description check with a regex if the field name
                     * needs to be interpolated with the current scope
                     * @param  {String} name - field name
                     */
                    function watchFieldNameOrExistence(name) {
                        var nameRegex = /\{\{(.*?)}}/;
                        var nameExpression = nameRegex.exec(name);
                        if (nameExpression) {
                            name = $interpolate(name)(scope);
                        }
                        watchFieldExistence(name);
                    }

                    /**
                     * @function
                     * @name  watchFieldExistence -----------------------------
                     * @description watch the fieldproperty on the form object
                     * NOTE: Angular create a form object which will hold all the
                     * fiels inside that form.
                     * @param  {string} name - field name
                     */
                    function watchFieldExistence(name) {
                        var watchName = "form[" +"'"+ name +"'"+ "]";
                        scope.$watch(watchName, function formControlChange(formControl) {
                            if (formControl) {
                                // if there is more then one field inside a template
                                // create an array with all formControls
                                if (fieldCount > 1) {
                                    if (!scope.options.formControl) {
                                        scope.options.formControl = [];
                                    }
                                    scope.options.formControl.push(formControl);
                                } else {
                                    scope.options.formControl = formControl;
                                }
                                scope.fc = scope.options.formControl; // formControl shortcut for template
                            }
                        });
                    }

                    /**
                     * @function
                     * @name callLinkFunctions ---------------------
                     * @description call the function on the field type
                     * object
                     */
                    function callLinkFunctions() {
                        if (type && type.link) {
                            type.link.apply(fr, args);
                        }
                    }

                    /**
                     * @function
                     * @name  asHtml -------------------------------
                     * @description create angular element with wrapper and
                     * return the html
                     * @param  {string} el - element template
                     * @return {string} - html
                     */
                    function asHtml(el) {
                        var wrapper = ng.element('<a></a>');
                        return wrapper.append(el).html();
                    }

                    initialize();
                }
            };
        }
    ]);
})(window.angular, window._);
