(function(ng, $, _) {
    'use strict';
    ng.module('formRenderer.form').directive('frForm', [
        '$q',
        'frConfig',
        'frFormConfig',
        'frPrerequisitesParser',
        'frUtils',
        function formDirective($q, config, formConfig, prerequisitesParser, utils) {
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl: 'fr/modules/form/views/form.html',
                scope: {
                    active: '=?',
                    activeStep: '=?',
                    schema: '=',
                    onSubmit: '=?',
                    onNavigate: '=?',
                    debug: '='
                },
                controller: 'frFormController',
                link: function(scope, element, attrs, ctrl) {

                    /**
                     * @function
                     * @name initilaize ---------------------------------------------
                     * @description - initialize form directive
                     */
                    function initialize() {
                        if(scope.active === undefined) {
                            scope.active = true;
                        }

                        if (!_.isEmpty(scope.frSchema.steps)) {
                            /** set a default step when active step was not defined */
                            if(scope.activeStep === undefined) {
                                scope.activeStep = _.first(scope.frSchema.steps).id;
                            }
                            scope.activeIndex = ctrl.getStepById(scope.activeStep)._stepIndex;
                        }

                        scope.formClass = formConfig.config.formClass ? formConfig.config.formClass : '';
                    }

                    /** @type {Object} */
                    scope.debugData = {
                        model: scope.model
                    };

                    /** @type {Int} */
                    scope.activeIndex = 0;

                    /**
                     * @function
                     * @name scrollToAndFocusFirstError ---------------------------------
                     * @description - Find and scroll to the first field that has an error
                     * @param {Object} element Form element
                     * @param {object} ctrl Form controller
                     * @param {Int} scrollAnimationTime Indicate how long the animation will take.
                     * @param {Int} scrollOffset offset for scroll to element
                     */
                    function scrollToAndFocusFirstError(element, ctrl, scrollAnimationTime, scrollOffset) {
                        var targetForm = element[0].querySelector('form');
                        var targetElement = $(targetForm)[0].querySelector('.ng-invalid');
                        if (targetElement) {
                            ctrl.scrollTo(targetElement, parseInt(scrollAnimationTime), scrollOffset);
                        }
                    }

                    /**
                     * @function
                     * @name onSubmit -------------------------------------------------------
                     * @description - handle onSubmit
                     * @param {Boolean} saveDraft Save as draft
                     */
                    function onSubmit(saveDraft) {
                        var modelPrerequisitesMatch = utils.deepGet(formConfig.config, 'modelOutput.onSubmit.PrerequisitesMatch');
                        var frSubmit = $q.defer(),
                            modelCopy = modelPrerequisitesMatch ? ctrl.getPrerequisitesMatchFormModel() : ctrl.getFormModel(),
                            isValidForm = _.cloneDeep(scope.theForm.$valid);

                        modelCopy.draft = saveDraft || false;

                        if (scope.onSubmit) {
                            scope.onSubmit(modelCopy, isValidForm, frSubmit);
                        } else {
                            // resolve promise when we can't find the onSubmit hook
                            isValidForm && frSubmit.resolve();
                            !isValidForm && frSubmit.reject();
                        }

                        frSubmit.promise.then(function success(data) {
                            // check if we have te re evaluate the form
                            if (data && data.reEvaluateSchema) {
                                ctrl.reEvaluateSchema(true);
                            }
                            // when the form isn't valid, still override it's navigation
                            var shouldResolve = !modelCopy.draft && isValidForm;
                            scope.$broadcast(config.enums.events.SUBMIT_COMPLETED, shouldResolve);
                        }, function error(err) {
                            scope.$broadcast(config.enums.events.SUBMIT_COMPLETED, false);
                            // scroll to the firts field with an error
                            if (formConfig.config.scrollToAndFocusFirstErrorOnSubmit) {
                                scrollToAndFocusFirstError(element, ctrl, formConfig.config.scrollAnimationTime, formConfig.config.scrollOffset);
                            }
                        });
                    }

                    /**
                     * @function
                     * @name navigateToNextStep ---------------------------------------
                     * @description - validate the form then check if the user has defined
                     * a onNavigate hook. We call this function when we can find one
                     * Wait till the promise 'frContinue' is resolved or rejected
                     * move to the next step when the form is valid
                     * @param  {Object} params - navigation parameters
                     */
                   function navigateToNextStep(params) {
                        // call form validation
                        ctrl.validateForm().finally(function whenCheckIsDone() {
                            // don't use an object -> spread all properties
                            // do some stuff even if the form is inValid
                            var isValidForm = _.cloneDeep(scope.theForm.$valid),
                                frContinue = $q.defer(),
                                activeStep = _.cloneDeep(scope.activeStep);
                            var stepModelPrerequisitesMatch = utils.deepGet(formConfig.config, 'modelOutput.onNavigate.PrerequisitesMatch');
                            var modelCopy = stepModelPrerequisitesMatch ? ctrl.getPrerequisitesMatchStepModel(activeStep) : ctrl.getFormModel();

                            frContinue.promise.then(
                                function navigateReady(data) {
                                    // check if we have te re evaluate the form
                                    if (data && data.reEvaluateSchema) {
                                        ctrl.reEvaluateSchema(true);
                                    }
                                    // move to the next step when the form is valid
                                    isValidForm && navigateToStep(params.stepId);
                                },
                                function notNavigateReady(err) {
                                    // scroll to the firts field with an error
                                    if (formConfig.config.scrollToAndFocusFirstErrorOnSubmit) {
                                        scrollToAndFocusFirstError(element, ctrl, formConfig.config.scrollAnimationTime, formConfig.config.scrollOffset);
                                    }
                                }
                            );
                            // Broadcast event when the frContinue promise was resolved or rejected
                            frContinue.promise.finally(function success() {
                                scope.$broadcast(config.enums.events.NAVIGATE_COMPLETED, params);
                            });
                            // check if the onNavigate hook is available for us
                            if (scope.onNavigate) {
                                scope.onNavigate(modelCopy, activeStep, isValidForm, frContinue);
                            } else {
                                // resolve promise when we can't find the onNavigate hook
                                isValidForm && frContinue.resolve();
                                !isValidForm && frContinue.reject();
                            }
                        });
                    }

                    /**
                     * @function
                     * @name  navigateToStep ---------------------------------------------
                     * @description - navigate to a step by stepId
                     * @param  {int|string} stepId - step id
                     */
                    function navigateToStep(stepId) {
                        console.log('stepId', stepId);
                        var step = ctrl.getStepById(stepId);
                        scope.activeIndex = step._stepIndex;
                        scope.activeStep = step.id;
                    }

                    scope.getStep = ctrl.getStepById;

                    /**
                     * @event - on Navigate
                     * description - this function will be executed when the user navigates
                     * to the previous or next step
                     * TODO: what with direct event?
                     */
                    scope.$on(config.enums.events.NAVIGATE, function (event, params) {
                        var direction = params.direction;
                        // broadcast navigate started
                        scope.$broadcast(config.enums.events.NAVIGATE_STARTED, params);
                        // check direction
                        if (direction === config.enums.navigation.NEXT) {
                            navigateToNextStep(params);
                        } else if (direction === config.enums.navigation.PREVIOUS) {
                            navigateToStep(params.stepId);
                        }
                    });

                    /**
                     * description - Update the activeIndex when the activeStep has changed
                     */
                    scope.$watch('activeStep', function (newValue, oldValue) {
                        if (newValue !== oldValue) {
                            scope.activeIndex = ctrl.getStepById(scope.activeStep)._stepIndex;
                        }
                    });

                    /**
                     * @event - on Submit
                     * description - the function will be executed when the user submits
                     * the form
                     * TODO: $broadcast submit started event
                     *       $broadcast submit completed event
                     */
                    scope.$on(config.enums.events.SUBMIT, function (event, params) {
                        scope.$broadcast(config.enums.events.SUBMIT_STARTED, params);
                        if (params.draft) {
                           // don't trigger validation
                           onSubmit(true);
                        } else {
                           ctrl.validateForm().finally(onSubmit);
                        }
                    });

                    /**
                     * @events - Debug
                     * description - Handles any debug value passed to him, and gives it to the debug panel directive
                     */
                    scope.$on(config.enums.events.DEBUG, function (event, param, value) {
                        scope.debugData[param] = value;
                    });

                    /**
                     * @events - Field Changed
                     * description - when a field changes, and validation ran, this event is raised.
                     * we use it to tell the prerequisites parser, to check this field's dependencies
                     * and activate or deactivate the proper fields, steps or sections accordingly
                    */
                    scope.$on(config.enums.events.FIELD_CHANGED, function (event, fieldName) {
                        var field = scope.frSchema._internalCache.fieldById[fieldName];

                        if (!field) {
                            // no field with that name in the cache...
                            throw new Error(config.enums.errors.FIELD_NOT_FOUND);
                        }

                        if (!field._prerequisitesDependencies) {
                            // skip this field, it has no dependencies, so nothing needs to be parsed
                            return;
                        }

                        // run through the prereqDependencies and test it's prerequitites
                        prerequisitesParser.testMultiple(field._prerequisitesDependencies);

                        // redraw navigation
                        scope.$broadcast(config.enums.events.NAVIGATION_REDRAW);
                    });

                    initialize();
                }
            };
        }
    ]);
})(window.angular, window.jQuery, window._);
