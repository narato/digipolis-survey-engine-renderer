(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormStep', [
        function frFormStepDirective() {
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl: 'fr/modules/form/views/form-step.html',
                scope: {
                    step: '=frFormStep',
                    model: '='
                }
            };
        }
    ]);
})(window.angular);
