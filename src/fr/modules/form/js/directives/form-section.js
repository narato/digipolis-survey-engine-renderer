(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormSection', [
        function frFormSectionDirective(){
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/form-section.html',
                scope: {
                    section: '=frFormSection',
                    model: '='
                },
                controller: 'frFormSectionController',
            };
        }
    ]);
})(window.angular);
