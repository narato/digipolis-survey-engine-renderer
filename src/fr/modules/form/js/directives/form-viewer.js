(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormViewer', [
        '$q',
        'frConfig',
        'frPrerequisitesParser',
        function formDirective($q, config, prerequisitesParser) {
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl: 'fr/modules/form/views/form-viewer.html',
                scope: {
                    schema: '=',
                    debug: '='
                },
                controller: 'frFormController',
                link: function(scope, element, attrs, ctrl) {

                    scope.active = true;

                    /** @type {Object} */
                    scope.debugData = {
                        model: scope.model
                    };
                }
            };
        }
    ]);
})(window.angular);
