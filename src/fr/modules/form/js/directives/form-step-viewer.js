(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormStepViewer', [
        function frFormStepViewerDirective() {
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl: 'fr/modules/form/views/form-step-viewer.html',
                scope: {
                    step: '=frFormStepViewer',
                    model: '='
                }
            };
        }
    ]);
})(window.angular);
