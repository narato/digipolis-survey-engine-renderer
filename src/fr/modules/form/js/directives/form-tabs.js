(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormTabs', [
        'frConfig',
        function frFormNavDirective(config) {
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/form-tabs.html',
                scope: {
                    steps: '=frFormTabs',
                    active: '=',
                    activeIndex: '='
                },
                link: function(scope, element, attrs, ctrl) {

                    function initialize() {
                        scope.width = calcWidth(scope.steps);
                    }

                    function calcWidth(items) {
                        return 'calc((100% / ' + items.length + ') - ((49px * ' + (items.length - 1) + ') / ' + items.length + '))';
                    }

                    scope.navigateTo = function navigateTo (stepId) {
                        scope.$emit(config.enums.events.NAVIGATE, {
                            stepId: stepId,
                            direction: config.enums.navigation.DIRECT
                        });
                    };

                    scope.width = 0;

                    initialize();
                }
            };
        }
    ]);
})(window.angular);
