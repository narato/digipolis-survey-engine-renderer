(function(ng) {
    'use strict';
    ng.module('formRenderer.form').controller('frFormSectionController', [
        '$scope',
        'frConfig',
        '$element',
        function frFormSectionController($scope, config,$element) {
            var body = $element.find('.fr-form-section-body');
            var speed = 200;

            function init() {
                if ($scope.section.state.collapsed) {
                    body.css('display','none');
                }
            }

            /**
             * @function
             * @name  toggleCollapse -------------
             * @description - toggle the collapse state if a section is collapsible
             */
            $scope.toggleCollapse = function toggleCollapse() {
                if ($scope.section.state.collapsible) {
                    $scope.section.state.collapsed = !$scope.section.state.collapsed;
                    if ($scope.section.state.collapsed) {
                        body.slideUp(speed);
                    } else {
                        body.slideDown(speed);
                    }
                }
            };

            /**
             * @function
             * @name  toggleEditMode -------------
             * @description - toggle the editMode from a section.
             * Broadcast an event and let all fields subcribe on this event
             */
            $scope.section.toggleEditMode = function toggleEditMode() {
                $scope.section.state.editMode = !$scope.section.state.editMode;
                if ($scope.section.state.collapsed) {
                   $scope.toggleCollapse();
                }
                $scope.$broadcast(config.enums.events.SECTION_TOGGLE_EDITMODE, $scope.section.state.editMode);
            };

            init();
        }
    ]);
})(window.angular);
