(function(ng, _){
    'use strict';
    ng.module('formRenderer.form').controller('frFormController', [
        '$anchorScroll',
        '$location',
        '$q',
        '$scope',
        'frConfig',
        'frSchemaValidator',
        'frSchemaParser',
        'frPrerequisitesParser',
        'frModelParser',
        'frUtils',
        function frFormController($anchorScroll, $location, $q, $scope, config, schemaValidator, schemaParser, prerequisitesParser, modelParser, frUtils) {

            var frSchema;
            var frModel;

            /**
             * @function
             * @name Initialize ---------------------
             */
            function initialize () {
                evaluateSchema();
            }

            /**
             * @function
             * @name  evaluateSchema ----------------------------------------------------
             * @description - make sure the schema is ready to render
             * @param  {Boolean} updateModel - true if the model needs an update
             */
            function evaluateSchema(updateModel) {
                frSchema = $scope.schema;

                setMeta();

                frSchema = schemaValidator.validate(frSchema);
                frSchema = schemaParser.parse(frSchema);
                frModel = modelParser.createModel(frSchema);

                if (updateModel) {
                    var modelCopy = _.cloneDeep($scope.model);
                    frUtils.extendDeep(frModel, modelCopy);
                    // update model inside debug panel
                    $scope.$emit(config.enums.events.DEBUG, config.enums.debug.MODEL, frModel);
                }

                frSchema = prerequisitesParser.parse(frSchema);

                $scope.frSchema = frSchema;
                $scope.model = frModel;

                $scope.meta.schemaReady = true;
            }

            /**
             * @function
             * @name removeUnvisibleFieldsFromModel -------------------------------------
             * @description - Removes unvisible fields from the model (hidden fields will not be removed!)
             *                Only the fields that don't match his prerequisites will be removed from the model
             * @return {Object}
             */
            function removeUnvisibleFieldsFromModel(model, unvisibleFields) {
                function getVisibleFieldsModel(fieldValue, fieldKey) {
                    if (_.isPlainObject(fieldValue)) {
                        return _.reduce(fieldValue, function (result, innerFieldValue, innerFieldKey) {
                            result[innerFieldKey] = getVisibleFieldsModel(innerFieldValue, innerFieldKey);
                            return result;
                        }, {});
                    }
                    return (unvisibleFields.indexOf(fieldKey) === -1) ? fieldValue : undefined;
                }

                return _.reduce(model, function (newModel, value, key) {
                    var visibleFieldsModel = getVisibleFieldsModel(value, key);
                    if (visibleFieldsModel) {
                        newModel[key] = visibleFieldsModel;
                    }
                    return newModel;
                }, {});
            }

            /**
             * @function
             * @name getInnerFieldNames --------------------------------------------------
             * @description - return array that will include all fieldNames inside the formModel
             *                form the internalcache
             * @return {Array}
             */
            function getInnerFieldNames(items) {
                return _.reduce(items, function (result, item, key) {
                    result = [].concat(result, _.keys(item._internalCache.formModel));
                    return result;
                }, []);
            }

            /**
             * @function
             * @name setMeta -------------------------------------------------------------
             * @description - set meta data
             */
            function setMeta () {
                $scope.meta = {
                    schemaReady: true,
                    hasSteps: true,
                    showIntro: false
                };
            }

            /**
             * @function
             * @name  reEvaluateSchema ---------------------------------------------------
             * @description - re-evaluate the form schema and update the model
             */
            this.reEvaluateSchema = evaluateSchema;

            /**
             * @function
             * @name validateForm --------------------------------------------------------
             * @description validate the form by sending an event to all the fields
             * inside the form. wait till all the validation promisis are resolved
             * @return {object} - promise
             */
            this.validateForm = function validateForm() {
                var deferred = $q.defer();
                var fieldValidatorPromises = [];
                $scope.$broadcast(config.enums.events.VALIDATE_ON_SUBMIT, fieldValidatorPromises);
                $q.all(fieldValidatorPromises).then(function isValid() {
                    deferred.resolve(true);
                }, function notValid() {
                    deferred.reject(false);
                });
                return deferred.promise;
            };

            /**
             * @function
             * @name getFormModel --------------------------------------------------------
             * @description - get a form model copy
             * @return {Object} - form model
             */
            this.getFormModel = function getFormModel() {
                var modelCopy = _.cloneDeep($scope.model);
                return modelCopy || {};
            };

            /**
             * @function
             * @name getPrerequisitesMatchStepModel --------------------------------------
             * @description - generate a model that only includes fields visible inside
             *                the active step. The prerequisites are matching for each field
             *                inside the generated model
             * @return {object}
             */
            this.getPrerequisitesMatchStepModel = function getPrerequisitesMatchStepModel(stepId) {
                var activeStepIC = this.getStepById(stepId)._internalCache;
                var schemaIC = _.cloneDeep($scope.schema)._internalCache;
                var modelCopy = _.cloneDeep($scope.model);

                var ActiveStepFields = _.reduce(schemaIC.field, function (result, field, key) {
                    _.forEach(activeStepIC.formModel, function (value, activeStepModelKey) {
                        if (field.name === activeStepModelKey) {
                            result.push(field);
                        }
                    });
                   return result;
                }, []);

                // find all fields that don't match the prerequisites
                // These fields are not visible to the user
                var unvisibleFields = _.pluck(_.filter(ActiveStepFields, { _prerequisitesMatch: false }), 'name');
                var allActiveFieldNames = _.pluck(ActiveStepFields, 'name');
                // create step model
                var activeStepModel = _.reduce(_.keys(modelCopy), function (result, key) {
                   if (allActiveFieldNames.indexOf(key) !== -1) {
                       result[key] = modelCopy[key];
                   }
                   return result;
                }, {});
                return removeUnvisibleFieldsFromModel(activeStepModel, unvisibleFields);
            };

            /**
             * @function
             * @name getPrerequisitesMatchFormModel ---------------------------------------
             * @description - generate a model that only includes fields visible inside
             *                the form. The prerequisites are matching for each field
             *                inside the generated model.
             * @return {object}
             */
            this.getPrerequisitesMatchFormModel = function getPrerequisitesMatchFormModel() {
                var internalCache = _.cloneDeep($scope.schema)._internalCache;
                var filterStep = _.filter(internalCache.step, { _prerequisitesMatch: false });
                var filterSection = _.filter(internalCache.section, { _prerequisitesMatch: false });
                var filterField = _.filter(internalCache.field, { _prerequisitesMatch: false });

                var unvisibleFields = _.uniq([].concat(getInnerFieldNames(filterStep), getInnerFieldNames(filterSection), _.pluck(filterField, 'name')));

                return removeUnvisibleFieldsFromModel(_.cloneDeep($scope.model), unvisibleFields);
            };

            /**
             * @function
             * @name  getFormId ----------------------------------------------------------
             * @description - return the form id
             * @return {String/Int} - id
             */
            this.getFormId = function getFormId() {
                return $scope.frSchema.formId;
            };

            /**
             * @function
             * @name  getStepIndex -------------------------------------------------------
             * @description - return the step index of a given stepId
             * @param {String} stepId - the id of the step you want the index for
             * @return {Int} - index
             */
            this.getStepIndex = function getStepIndex(stepId) {
                var step = _.find($scope.frSchema.steps, { id: stepId });

                if (!step) {
                    return undefined;
                }

                return step._originalStepIndex;
            };

            /**
             * @function
             * @name  getStepById --------------------------------------------------------
             * @description - return the step of a given stepId
             * @param {String} stepId - the id of the step you want
             * @return {Object} - the step
             */
            this.getStepById = function getStepById(stepId) {
                return _.find($scope.frSchema.steps, { id: stepId });
            };

            /**
             * @function
             * @name  getStepByIndex -----------------------------------------------------
             * @description - return the step of a given index
             * @param {Int} index - the index of the step you want
             * @return {Object} - the step
             */
            this.getStepByIndex = function getStepByIndex(index) {
                return _.find($scope.frSchema.steps, { _stepIndex: index });
            };

            /**
             * @function
             * @name getDataStore --------------------------------------------------------
             * @description - returns a data store. A data store can be used by a
             *                range of custom input fields. (select, radio list, checkbox list)
             * @param  {String} name - Name of the data store
             * @return {Array} - A list of
             */
            this.getDataStore = function getDataStore(name) {
                var result;
                if ($scope.frSchema.dataStores) {
                    result = $scope.frSchema.dataStores[name];
                }
                return result;
            };

            /**
             * @function
             * @name scrollTo ------------------------------------------------------------
             * @description - scroll to element
             * @param {Object} element
             * @param {Int} duration Animation time
             * @param {Int} scrollOffset Offset
             */
            this.scrollTo = function(element, durationP, scrollOffset) {

                /**
                 * @function
                 * @name scaleTimeToDistance
                 * @description - Longer scroll duration for longer distances
                 * @param {Int} distance
                 * @param {Int} duration
                 */
                function scaleTimeToDistance(distance, duration) {
                    var baseDistance = 500;
                    var distanceAbs = Math.abs(distance);
                    var min = duration / 10;
                    return duration * distanceAbs / baseDistance + min;
                }

                /**
                 * @function
                 * @name easeIntOutQuadt
                 * @description - ease in out animation
                 * @param {Int} t Current time
                 * @param {Int} b Beginning value
                 * @param {Int} c Change in value
                 * @param {Int} d Duration
                 * NOTE: https://github.com/danro/jquery-easing/blob/master/jquery.easing.js
                 *       http://upshots.org/actionscript/jsas-understanding-easing
                 */
                function easeInOutQuadt(t, b, c, d) {
                     if ((t /= d / 2) < 1) {
                         return c / 2 * t * t + b;
                     }
                     return -c / 2 * ((--t) * (t - 2) - 1) + b; // jshint ignore:line
                }

                var targetY = element.getBoundingClientRect().top + parseInt(scrollOffset, 10),
                    targetX = element.getBoundingClientRect().left,
                    duration = scaleTimeToDistance(targetY, durationP);

                var start = window.pageYOffset,
                    increment = 20,
                    change = targetY,
                    currentTime = 0;

                /**
                 * @function
                 * @name animateScroll
                 * @description - create scroll to animation
                 */
                function animateScroll() {
                    currentTime = currentTime + increment;
                    var val = easeInOutQuadt(currentTime, start, change, duration);
                    window.scrollTo(targetX, val);

                    if (currentTime < duration) {
                        setTimeout(animateScroll, increment);
                    } else {
                        element.focus();
                    }
                }
                // return if no animation is required
                if (change === 0) {
                    element.focus();
                    return;
                }

                animateScroll();
            };

            initialize();
        }
    ]);
})(window.angular, window._);
