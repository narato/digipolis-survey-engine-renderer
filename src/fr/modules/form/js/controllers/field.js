(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').controller('frFieldController', [
        '$scope',
        '$q',
        '$controller',
        'frConfig',
        'frFieldConfig',
        'frUtils',
        function frFieldController($scope, $q, $controller, config, frFieldConfig, frUtils) {

            /** @type {Object} */
            $scope.field = {};

            /**
             * @name  initialize controller ----------------------
             */
            function initialize() {
                var fieldType,
                    fieldTypeConf;

                fieldType = $scope.options.spec.attributes.type;
                fieldTypeConf = frFieldConfig.getType(fieldType);
                createDefaults($scope.options);
                mergeFieldOptionsWithTypeDefaults($scope.options, fieldTypeConf);
                invokeControllers($scope, $scope.field, fieldTypeConf);
            }

            /**
             * @function
             * @name  createDefaults ----------------------------
             * @description fill options with default values
             * @param  {object} options - field options
             */
            function createDefaults(options) {
                options = options || {};
                frUtils.reverseDeepMerge(options, {
                    spec : {
                        attributes: {
                            id: $scope.options.name,
                            name: $scope.options.name
                        }
                    }
                });
                // Short template notation
                $scope.field.spec = $scope.options.spec;
                $scope.field.to = $scope.options.spec.options;
                $scope.field.attrs = $scope.options.spec.attributes;
                $scope.field.validation = $scope.options.inputFilter;
                $scope.field.state = $scope.options.state;
                // Create model copy on initialize to create a variabe ngModelInit
                // now we have a reference to the initial model
                $scope.ngModelInit = _.cloneDeep($scope.ngModel);
            }

            /**
             * @function
             * @name  mergeFieldOptionsWithTypeDefaults ------------
             * @description merge field options properties with the
             * type defaults
             * @param  {Object} options - field options
             * @param  {type} type - field config
             */
            function mergeFieldOptionsWithTypeDefaults(options, type) {
                if (type) {
                    mergeOptions(options, type.defaultOptions);
                    _.assign(options, {
                        templateUrl: type.templateUrl,
                        baseType: type.extend ? type.extend : type.name
                    });
                    // short template notation
                    frUtils.reverseDeepMerge($scope.field, {
                        templateUrl: options.templateUrl,
                        baseType: options.baseType
                    });
                }
            }

            /**
             * @function
             * @name  mergeOptions --------------------------
             * @description merge config default options with the field
             * options
             * @param  {Object} options - field options
             * @param  {Function|Object} extraOptions - defaultOptions
             */
            function mergeOptions(options, extraOptions) {
                if (extraOptions) {
                    if (_.isFunction(extraOptions)) {
                        extraOptions = extraOptions(options);
                    }
                    frUtils.reverseDeepMerge(options, extraOptions);
                }
            }

            /**
             * @function
             * @name  invokeController -----------------------------
             * @description invoke all controller function
             * @param  {Object} scope
             * @param  {Object} options
             * @param  {Object} type
             */
            function invokeControllers(scope, options, type) {
                options = options || {};
                type = type || {};
                _.forEach([type.controller, options.controller], function(controller) {
                    if (controller) {
                        $controller(controller, {$scope: scope});
                    }
                });
            }

            /**
             * @function
             * @name innerFieldExists ----------------------------
             * @description - checks if we can find an inner field inside an inner field array list
             * @param  {Array} innerFields - array of objects
             * @param  {String} innerFieldName - the name from the inner field we want to find
             * @return {Object} - returns undefined when no innerfield was found
             */
            function innerFieldExists(innerFields, innerFieldName) {
                return _.find(innerFields, function findInnerfield(innerField) {
                    return innerFieldName === innerField.name;
                });
            }

            /**
             * @function
             * @name  validateField --------------------------------
             * @description - broadcast a validateField event with paramaters.
             * - fieldName: name of a field
             * - validateSucces: succes callback function
             * - validateError: error callback function
             * @param  {String} innerFieldName - the field name
             * @return {Promise} - returns a promise object
             */
            $scope.field.validateInnerField = function validateInnerField(innerFieldName) {

                var deferred = $q.defer(),
                    innerFields = $scope.field.spec.options.fields;

                // only trigger the validation for true inner fields
                if (!innerFieldExists(innerFields, innerFieldName)) {
                    var error = [
                        'Error while trying to validate an inner field. ',
                        'Could not find an inner field with name: ' + innerFieldName
                    ].join('');
                    throw error;
                }

                $scope.$broadcast(config.enums.events.VALIDATE_FIELD, innerFieldName, function validateSucces(data) {
                    deferred.resolve(data);
                }, function validateError(data) {
                    deferred.reject(data);
                });

                return deferred.promise;
            };

            /**
             * @function
             * @name  toggleEditMode -----------------------------
             * @description - toggles the state property `editMode`
             */
            $scope.field.toggleEditMode = function toggleEditMode() {
                $scope.field.state.editMode = !$scope.field.state.editMode;
                // reset the model with the initial value
                $scope.ngModel = _.cloneDeep($scope.ngModelInit);
            };

            /**
             * @function
             * @name  getValueFromOptions -----------------------
             * @description - get a value from the value options by key
             * @param  {String} key - value option key
             * @return {String} - option value
             */
            $scope.field.getValueFormOptions = function getValueFromOptions(key) {
                var option = _.find($scope.field.to.valueOptions, function (option) {
                    return option.key === key;
                });
                return option && option.value;
            };

            /**
             * @description - when the section state `editMode` was toggled we need to change
             * the editMode for each field inside the section
             */
            $scope.$on(config.enums.events.SECTION_TOGGLE_EDITMODE, function (event, editMode) {
                if ($scope.field.state.editable) {
                    $scope.field.state.editMode = editMode;
                }
            });

            initialize();
        }
    ]);
})(window.angular, window._);
