(function (ng) {
    'use strict';
    ng.module('formRenderer.form').filter('unsafe', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    });
})(window.angular);
