# Form-Renderer

## About

The formRenderer is a generic AngularJS module to render forms in your application. You can find more information on the documentation page: [Documentation of the json schema](https://itvanatotz.wiki.antwerpen.be/Form+Renderer)

## Usage



To use the form renderer in your website, include the following dependencies in your html





```
 <script src="bower_components/jquery/dist/jquery.min.js"></script>

 <!-- general required files -->
 <script src="bower_components/angular/angular.js"></script>
 <script language="JavaScript" src="bower_components/angular-sanitize/angular-sanitize.js"></script>

 <!-- the form renderer itself -->
 <script language="JavaScript" src="bower_components/formrenderer/dist/formrenderer-x.x.x.min.js"></script>
```

and the styling

```
<link rel="stylesheet" href="/dest/formrenderer-x.x.x.min.css">

```

## Helping Out

### Setup

1. checkout this repository
1. install dependencies `npm install && bower install`
1. you can either continue with the local development steps, or host the formrenderer on your own server (Apache, IIS, ...)

### Local development

1. after installing all dependencies you can start up a development server with: `grunt -f`
1. you can view the local server at:  [http://localhost:35732](http://localhost:35732)

### Tryout your own form json

If you only want to test some form json or checkout the examples.
Please use this grunt task: `grunt server` instead of `grunt -f`.

open the `examples/your-own/data/` folder, and paste your form in the `form.json` file.  Now you can visit your own form in your hosted server (either local development via grunt, or your own apache, iis, or other server). There you can see your own form.

## API

* [The form schema](wiki/frSchema.md)
* [How to configure the formrenderer](wiki/frFormConfig.md)
* [How to create a custom field](wiki/frFieldConfig.md)
* [available Directives](wiki/frDirectives.md)
* [Validation](wiki/frValidationConfig.md)

## Template libraries

* [A-stad fields](https://stash.antwerpen.be/projects/ASTAD/repos/formrenderer-types-astad_tools.angularjs/browse)

## Integrations

* [A-stad custom fields](wiki/customfields.md)
