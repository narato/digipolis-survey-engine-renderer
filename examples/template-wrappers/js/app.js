'use strict';

var module = angular.module('fieldWrapperSample', ['ui.router', 'formRenderer']);

module.run([
    '$rootScope', '$timeout', '$state', '$stateParams', 'frFieldConfig',
    function run($rootScope, $timeout, $state, $stateParams, frFieldConfig) {

        frFieldConfig.setType([{
            name: 'custom-input',
            templateUrl: '/examples/template-wrappers/views/types/custom-input.html'
        }, {
            name: 'custom-input-no-wrapper',
            extend: 'custom-input',
            wrapper: null // don't use a wrapper
        }, {
            name: 'text-block',
            wrapper: null, // don't use a wrapper
            templateUrl: '/examples/template-wrappers/views/types/text-block.html'
        }, {
            name: 'custom-input-with-multiple-wrappers',
            extend: 'custom-input',
            wrapper: ['wrapper1', 'wrapper2', 'wrapper3'] // use default wrappers
        }, {
            name: 'custom-input-field-type-wrapper',
            extend: 'custom-input'
        }]);

        frFieldConfig.setTempWrapper([{
            // when there is no name and fieldType provided
            // this wrapper will be the default one for all field types
            templateUrl: '/examples/template-wrappers/views/wrappers/default.html'
        }, {
            name: 'wrapper1',
            templateUrl: '/examples/template-wrappers/views/wrappers/wrapper1.html'
        }, {
            name: 'wrapper2',
            templateUrl: '/examples/template-wrappers/views/wrappers/wrapper2.html'
        }, {
            name: 'wrapper3',
            templateUrl: '/examples/template-wrappers/views/wrappers/wrapper3.html'
        }, {
            templateUrl: '/examples/template-wrappers/views/wrappers/field-types.html',
            fieldTypes: ['custom-input-field-type-wrapper'] // specify which types should use this wrapper
        }]);
    }
]);

module.config(
    [
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/field-wrapper");
            $stateProvider
                .state('field-wrapper', {
                    url: '/field-wrapper',
                    templateUrl: '/examples/template-wrappers/views/field-wrapper.html',
                    controller: 'fieldWrapperForm',
                    controllerAs: 'form',
                    resolve: {
                        schema: ['$http', function($http) {
                            return $http.get('/examples/template-wrappers/data/field-wrapper.json');
                        }]
                    }
                });
        }
    ]
);

module.controller('fieldWrapperForm', ['schema', function exampleFormController(schema) {
    this.schema = schema.data || {};
    this.model = {};
}]);
