'use strict';

var module = angular.module('customTypesSample', ['ui.router', 'formRenderer']);

module.run([
    '$rootScope', '$timeout', '$state', '$stateParams', 'frFieldConfig',
    function run($rootScope, $timeout, $state, $stateParams, frFieldConfig) {
        // custom field example
        frFieldConfig.setType([{
            name: 'text',
            templateUrl: '/examples/custom-types/views/input/text.html'
        }, {
            name: 'extendFromText',
            extend: 'text'
        }]);
    }
]);

module.config(
    [
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/custom-field");
            $stateProvider
                .state('custom-field', {
                    url: '/custom-field',
                    templateUrl: '/examples/custom-types/views/custom-field.html',
                    controller: 'customFieldForm',
                    controllerAs: 'form',
                    resolve: {
                        schema: ['$http', function($http) {
                            return $http.get('/examples/custom-types/data/custom-field.json');
                        }]
                    }
                })
                .state('extend', {
                    url: '/extend',
                    templateUrl: '/examples/custom-types/views/extend.html',
                    controller: 'extendTypeForm',
                    controllerAs: 'form',
                    resolve: {
                        schema: ['$http', function($http) {
                            return $http.get('/examples/custom-types/data/extend-type.json');
                        }]
                    }
                });
        }
    ]
);

module.controller('customFieldForm', ['schema', function exampleFormController(schema) {
    this.schema = schema.data || {};
    this.model = {};
}]);


module.controller('extendTypeForm', ['schema', function extendTypeController(schema) {
    this.schema = schema.data || {};
    this.model = {};
}]);
