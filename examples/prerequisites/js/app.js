/**
 * navigation Module
 *
 * Show the power of navigation inbetween steps in a form
 */
var module = angular.module('navigationSample', ['ui.router', 'formRenderer']);

module.run(
    [
        '$rootScope', '$state', '$stateParams', 'frFieldConfig',
        function($rootScope, $state, $stateParams, frFieldConfig) {
            frFieldConfig.setType([{
                name: 'radio',
                templateUrl: '/examples/prerequisites/views/input/radio.html',
            }, {
                name: 'text',
                templateUrl: '/examples/prerequisites/views/input/text.html',
            }, {
                name: 'checkbox',
                templateUrl: '/examples/prerequisites/views/input/checkbox.html',
                controller: function($scope) {

                    var optionsWatcher;

                    /**
                     * @function
                     * @name  initialize -----------------------------
                     */
                    function initialize() {
                        if (!checkModel()) {
                            return;
                        }
                        $scope.field.to.multiple = checkMultiple();
                        if ($scope.field.to.multiple && !$scope.field.to.valueOptions) {
                            optionsWatcher = $scope.$watch('field.to.valueOptions', function(newValue, oldValue) {
                                if (newValue) {
                                    initialize();
                                    optionsWatcher();

                                }
                            });
                            return;
                        }
                        prepareValueOptions($scope.ngModelInit);
                    }
                    /**
                     * @function
                     * @name  checkModel ----------------------------
                     * @description check if the model is not undefined
                     * @return {Boolean}
                     */
                    var checkModel = function checkModel() {
                        if ($scope.ngModel === undefined) {
                            console.error('The fr-checkbox directive requires a model');
                            return false;
                        }
                        return true;
                    };

                    /**
                     * @function
                     * @name  checkMultiple ---------------------------
                     * @description check if it is multiple checkbox or not
                     * @return {Boolean}
                     */
                    var checkMultiple = function checkMultiple() {
                        if ($scope.field.to.valueOptions && $scope.field.to.valueOptions.length > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    };

                    /**
                     * @function
                     * @name  parseValueOptions -----------------------
                     * @description - en
                     * @param  {Array} modelInit - initial model value
                     */
                    function prepareValueOptions(modelInit) {
                        if (_.isArray(modelInit)) {
                            _.forEach(modelInit, function(modelOption) {
                                var option = _.find($scope.field.to.valueOptions, function(option) {
                                    return option.key === modelOption;
                                });
                                option.checked = option ? true : false;
                            });
                        }
                    }

                    /**
                     * @function
                     * @name  resetValueOptions ----------------------
                     * @description - reset value options with the initial
                     * model value
                     * @param  {Array} modelInit - initial model value
                     * @return {Boolean}
                     */
                    function resetValueOptions(modelInit) {
                        _.forEach($scope.field.to.valueOptions, function(option) {
                            option.checked = false;
                        });
                        prepareValueOptions(modelInit);
                        return true;
                    }

                    /**
                     * @function
                     * @name  toggleEditMode --------------------------
                     * @description - toggles the state property `editMode`
                     */
                    $scope.toggleEditMode = function toggleEditMode() {
                        $scope.field.state.editMode = !$scope.field.state.editMode;
                        // reset the model with the initial value
                        $scope.ngModel = _.cloneDeep($scope.ngModelInit);
                        resetValueOptions($scope.ngModelInit);
                    };

                    /**
                     * @function
                     * @name  updateModel ----------------------------
                     * @description update the model
                     * @param  {Object} option 'key value pair'
                     */
                    $scope.updateModel = function updateModel(option) {
                        if (typeof $scope.ngModel === 'string') {
                            $scope.ngModel = [];
                        }
                        if (option.checked === undefined) {
                            console.warn('can\'t update the checkbox list model, options checked = ' + option.checkbox);
                        }
                        var inModel = $scope.ngModel.indexOf(option.key);
                        if (option.checked) {
                            if (inModel < 0) {
                                $scope.ngModel.push(option.key);
                            }
                        } else if (!option.checked) {
                            if (inModel > -1) {
                                $scope.ngModel.splice(inModel, 1);
                            }
                        }
                        return $scope.ngModel;
                    };
                    initialize();
                }
            }]);
        }
    ]
);

module.config(
    [
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/form");
            $stateProvider
            // state for the form tab
                .state('form', {
                url: '/form',
                templateUrl: '/examples/prerequisites/views/form.htm',
                controller: 'exampleForm',
                controllerAs: 'form',
                resolve: {
                    schema: ['$http', function($http) {
                        return $http.get('/examples/prerequisites/data/prerequisites.json');
                    }]
                }
            });
        }
    ]
);

module.controller('exampleForm', [
    '$scope',
    '$rootScope',
    '$timeout',
    'schema',
    '$state',
    function exampleFormController($scope, $rootScope, $timeout, schema, $state) {
        var form = this;
        form.schema = schema.data || {};
        form.model = {};

        form.onSubmit = function(model, isValidForm, submitPromise) {
            $timeout(function() {
                submitPromise.resolve();
            }, 2000);
        };

        form.onNavigate = function(model, activeStep, isValidForm, frContinue) {
            if (isValidForm) {
                frContinue.resolve();
            } else {
                frContinue.reject();
            }
        };
    }
]);
