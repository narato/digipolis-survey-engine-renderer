/**
 * navigation Module
 *
 * Show the power of navigation inbetween steps in a form
 */
var module = angular.module('navigationSample', ['ui.router', 'formRenderer']);

module.run(
    [
        '$rootScope', '$state', '$stateParams', 'frFieldConfig',
        function($rootScope, $state, $stateParams, frFieldConfig) {
            // set debug to true, to see more details at the bottom of your screen...
            $rootScope.debug = false;

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            frFieldConfig.setType([{
                name: 'text',
                templateUrl: '/examples/step-navigation/views/input/text.html'
            }]);
        }
    ]
);

module.config(
    [
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/form");
            $stateProvider
            // state for the form tab
                .state('form', {
                url: '/form',
                templateUrl: '/examples/step-navigation/views/form.htm',
                controller: 'exampleForm',
                controllerAs: 'form',
                resolve: {
                    schema: ['$http', function($http) {
                        return $http.get('/examples/step-navigation/data/steps-navigation.json');
                    }]
                }
            })

            // the custom state needed for form steps
            .state('form.step', {
                url: '/:step',
            });
        }
    ]
);

module.controller('exampleForm', [
    '$scope',
    '$rootScope',
    '$timeout',
    'schema',
    '$state',
    function exampleFormController($scope, $rootScope, $timeout, schema, $state) {
        var form = this;
        form.schema = schema.data || {};
        form.model = {};
        form.activeStep;

        var setActiveStep = function setActiveStep(stepId) {
            form.activeStep = stepId;
        };

        var StateChangeHandler = $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            if (toState.name === 'form.step') {
                setActiveStep(toParams.step);
            }
        });

        form.onSubmit = function(model, isValidForm, submitPromise) {
            console.log('submit', model, isValidForm, submitPromise);
            $timeout(function() {
                submitPromise.resolve();
            }, 2000);
        };

        form.onNavigate = function(model, activeStep, isValidForm, frContinue) {
            console.log('navigating', model, activeStep, isValidForm, frContinue);
            frContinue.resolve();
        };

        $scope.$on('$destroy', function() {
            StateChangeHandler();
        })

        setActiveStep($state.params.step);
    }
]);
