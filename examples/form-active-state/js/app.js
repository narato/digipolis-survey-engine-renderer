/**
 * activation Module
 *
 * Show the power of activation or deactivation of a form
 */
var module = angular.module('activationSample', ['formRenderer']);

module.run(
    [
        '$rootScope',
        'frFieldConfig',
        function($rootScope, frFieldConfig) {
            frFieldConfig.setType([{
                name: 'text',
                templateUrl: '/examples/step-navigation/views/input/text.html'
            }]);
        }
    ]
);

module.config(
    [
        function() {

        }
    ]
);

module.controller('exampleForm', ['$http', function exampleFormController($http) {
    var form = this;

    function initialize() {
        $http
            .get('/examples/form-active-state/data/form.json')
            .success(function(data, status) {
                form.schema = data || {};
            });
    }

    form.active = false;
    form.schema = undefined;

    form.toggleActive = function toggleActive() {
        form.active = !form.active;
        return false;
    };

    initialize();
}]);
