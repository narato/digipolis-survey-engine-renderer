## 1.Editable and Editmode

### 1.1 steps
----------------------------------

If a step has a state `(editable === false)` all sections and fields should have a `falsy`editable state
The `editmode` state is only used to toggle between the edit mode and readonly mode.

| step editmode | Step editable | Section editmode  | Section editable  | Field editmode | Field editable |
| ------------- | ------------- | ----------------- |------------------ | -------------- | -------------- |
| false         | false         |  /                |  false            | /              | false          |
| true          | true          |  /                |  /                | /              | /              |
| false         | true          |  /                |  /                | /              | /              |
| true          | false         |  /                |  false            | /              | false          |



### 1.2 sections
----------------------------------


| Sections editmode | Sections editable | Fields editmode | Fields editable | Show edit button  |
| ----------------- | ----------------- | --------------- | --------------- | ----------------- |
| false             | false             | false           | false           | /                 |
| true              | true              | /               | /               | false             |
| false             | true              | /               | /               | false             |
| true              | false             | false           | false           | /                 |


### 1.3 Fields
----------------------------------

We only show `edit toggle buttons` next to the inputfield when there are no sections or steps

| Field editmode | Field editable | Show edit button |
| -------------- | -------------- | ---------------- |
| false          | false          | false            |
| true           | true           | true             |
| false          | true           | true             |
| true           | false          | false            |