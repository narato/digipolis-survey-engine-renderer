// Karma configuration
// Generated on Tue Jul 14 2015 12:40:06 GMT+0200 (CEST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha-debug', 'mocha', 'chai-sinon'],


    // list of files / patterns to load in the browser
    files: [
        // Include all needed files and libraries
        {
            pattern: 'bower_components/jquery/jquery.min.js',
            watched: false
        },
        {
            pattern: 'bower_components/angular/angular.min.js',
            watched: false
        },
        {
            pattern: 'bower_components/angular-mocks/angular-mocks.js',
            watched: false
        },
        {
            pattern: 'bower_components/angular-sanitize/angular-sanitize.js',
            watched: false
        },
        {
            pattern: 'bower_components/moment/min/moment-with-locales.min.js',
            watched: false
        },
        {
            pattern: 'bower_components/akit-service-date/dist/akit-service-date.js',
            watched: false
        },
        {
            pattern: 'src/tests/mocks/*.json',
            served: true,
            watched: true
        },
        'bower_components/lodash/dist/lodash.min.js',
        'node_modules/chai-datetime/chai-datetime.js',
        'node_modules/chai-as-promised/chai-as-promised.js',
        'node_modules/chai-things/lib/chai-things.js',
        'src/fr/modules.js',
        'src/fr/main.js',
        'src/fr/**/*.js',
        'src/fr/**/*.html',
        'src/tests/root.js',
        'src/tests/**/*.js'
    ],

    // list of files to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        'src/fr/modules/**/*.html': 'ng-html2js',
        'src/tests/mocks/*.json': 'ng-json2js'
        // 'src/fr/modules/form/js/controllers/*.js': ['coverage'],
        // 'src/fr/modules/form/js/directives/*.js': ['coverage'],
        // 'src/fr/modules/form/js/filters/*.js': ['coverage'],
        // 'src/fr/modules/form/js/providers/*.js': ['coverage'],
        // 'src/fr/modules/form/js/services/*.js': ['coverage'],
        // 'src/fr/modules/validation/js/directives/*.js': ['coverage'],
        // 'src/fr/modules/validation/js/factories/*.js': ['coverage'],
        // 'src/fr/modules/validation/js/providers/*.js': ['coverage']
    },

    coverageReporter:{
        type:'html',
        dir: '.coverage/'
    },

    ngHtml2JsPreprocessor: {
        // strip this from the file path
        stripPrefix: 'src/',
        // prepend this to the
        prependPrefix: '',

        // - setting this option will create only a single module that contains templates
        //   from all the files, so you can load them all with module('foo')
        // - you may provide a function(htmlPath, originalPath) instead of a string
        //   if you'd like to generate modules dynamically
        //   htmlPath is a originalPath stripped and/or prepended
        //   with all provided suffixes and prefixes
        moduleName: 'formTemplates'
    },

    ngJson2JsPreprocessor: {
        // strip this from the file path
        stripPrefix: 'src/tests/mocks/',
        // prepend this to the
        prependPrefix: 'data/'
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['mocha', 'coverage'],

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  })
}
