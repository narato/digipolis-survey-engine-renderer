/*
	Angular Form Renderer v2.0.14
*/
(function(ng) {
    'use strict';
    // child modules
    ng.module('formRenderer.config', []);
    ng.module('formRenderer.utils', ['formRenderer.config']);
    ng.module('formRenderer.form', ['formRenderer.config', 'formRenderer.utils']);
    ng.module('formRenderer.validation', ['formRenderer.config']);

    // main module
    ng.module('formRenderer', ['ngSanitize', 'akit.service.date', 'formRenderer.form', 'formRenderer.validation', 'formRenderer-templates']);

})(window.angular);
;(function(ng) {
    'use strict';
    var formRenderer = ng.module('formRenderer');

    formRenderer.run([function run() {}]);
    // Module config
    formRenderer.config(['$provide', function ($provide) {

        // Workaround for bug #1404
        // https://github.com/angular/angular.js/issues/1404
        // Source: http://plnkr.co/edit/hSMzWC?p=preview
        $provide.decorator('ngModelDirective', ['$delegate', function($delegate) {
            var ngModel = $delegate[0], controller = ngModel.controller;
            ngModel.controller = ['$scope', '$element', '$attrs', '$injector', function(scope, element, attrs, $injector) {
                var $interpolate = $injector.get('$interpolate');
                attrs.$set('name', $interpolate(attrs.name || '')(scope));
                $injector.invoke(controller, this, {
                    '$scope': scope,
                    '$element': element,
                    '$attrs': attrs
                });
            }];
            return $delegate;
        }]);
        $provide.decorator('formDirective', ['$delegate', function($delegate) {
            var form = $delegate[0], controller = form.controller;
            form.controller = ['$scope', '$element', '$attrs', '$injector', function(scope, element, attrs, $injector) {
                var $interpolate = $injector.get('$interpolate');
                attrs.$set('name', $interpolate(attrs.name || attrs.ngForm || '')(scope));
                $injector.invoke(controller, this, {
                    '$scope': scope,
                    '$element': element,
                    '$attrs': attrs
                });
            }];
            return $delegate;
        }]);
    }]);
})(window.angular);
;// ATTENTION!// DO NOT MODIFY THIS FILE BECAUSE IT WAS GENERATED AUTOMATICALLY// SO ALL YOUR CHANGES WILL BE LOST THE NEXT TIME THE FILE IS GENERATED
angular.module('formRenderer-templates', ['fr/modules/form/views/debug-panel.html', 'fr/modules/form/views/form-nav.html', 'fr/modules/form/views/form-section-viewer.html', 'fr/modules/form/views/form-section.html', 'fr/modules/form/views/form-step-viewer.html', 'fr/modules/form/views/form-step.html', 'fr/modules/form/views/form-tabs.html', 'fr/modules/form/views/form-viewer.html', 'fr/modules/form/views/form.html', 'fr/modules/validation/views/messages.html']);

angular.module("fr/modules/form/views/debug-panel.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/debug-panel.html",
    "<div class=\"fr-debug debug-output\">\n" +
    "    <div class=\"fr-debug-head\" ng-click=\"toggleState('debug')\">\n" +
    "        <strong>Debug</strong>\n" +
    "        <span class=\"fr-debug-close\" ng-if=\"state.debug\">x</span>\n" +
    "    </div>\n" +
    "    <div class=\"fr-debug-body\" ng-if=\"state.debug\">\n" +
    "        <div class=\"col\">\n" +
    "            <strong>General Debug Data</strong>\n" +
    "            <p>Some interesting fields.</p>\n" +
    "            <ul>\n" +
    "                <li><span>form active:</span> {{active}}</li>\n" +
    "                <li><span>form valid:</span> {{form.$valid}}</li>\n" +
    "                <li><span>form pristine:</span> {{form.$pristine}}</li>\n" +
    "                <li><span>form dirty:</span> {{form.$dirty}}</li>\n" +
    "                <li><span>form name:</span> {{schema.name}}</li>\n" +
    "                <li><span>active step id:</span> {{activeStep}}</li>\n" +
    "                <li><span>active step index:</span> {{activeStepIndex}}</li>\n" +
    "                <li><span>button next</span> {{data.showNext ? 'visible' : 'hidden'}}</li>\n" +
    "                <li><span>button previous</span> {{data.showPrevious ? 'visible' : 'hidden'}}</li>\n" +
    "                <li><span>button save</span> {{data.showSave ? 'visible' : 'hidden'}}</li>\n" +
    "                <li><span>button draft</span> {{data.showSaveDraft ? 'visible' : 'hidden'}}</li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "        <div class=\"col\">\n" +
    "            <strong>Form Output</strong>\n" +
    "            <p>This output is the output that would be sent out to services when saving.</p>\n" +
    "            <pre ng-bind-html=\"printData(data.model)\"></pre>\n" +
    "        </div>\n" +
    "        <div class=\"clear\"></div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("fr/modules/form/views/form-nav.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/form-nav.html",
    "<nav class=\"fr-form-nav\">\n" +
    "    <button\n" +
    "        type=\"button\"\n" +
    "        class=\"button\"\n" +
    "        role=\"button\"\n" +
    "        ng-show=\"showPrevious\"\n" +
    "        ng-disabled=\"!active || buttonState.disabled\"\n" +
    "        ng-click=\"previous()\">{{navigation.previous}}</button>\n" +
    "    <a\n" +
    "        class=\"button\"\n" +
    "        role=\"button\"\n" +
    "        ng-show=\"showSaveDraft\"\n" +
    "        ng-disabled=\"!active || buttonState.disabled\"\n" +
    "        ng-click=\"saveDraft()\">{{navigation.concept}}</a>\n" +
    "    <button\n" +
    "        type=\"button\"\n" +
    "        class=\"button\"\n" +
    "        role=\"button\"\n" +
    "        ng-show=\"showNext\"\n" +
    "        ng-disabled=\"!active || buttonState.disabled\"\n" +
    "        ng-click=\"next()\">{{navigation.next}}</button>\n" +
    "    <button\n" +
    "        type=\"submit\"\n" +
    "        class=\"button\"\n" +
    "        role=\"button\"\n" +
    "        ng-show=\"showSave\"\n" +
    "        ng-disabled=\"!active || buttonState.disabled\"\n" +
    "        ng-click=\"save()\">{{navigation.submit}}</button>\n" +
    "</nav>\n" +
    "");
}]);

angular.module("fr/modules/form/views/form-section-viewer.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/form-section-viewer.html",
    "<div class=\"fr-form-section\">\n" +
    "    <div class=\"fr-form-section-header\">\n" +
    "        <div>\n" +
    "            <span class=\"fr-form-section-title\" ng-if=\"section.title\" ng-class=\"{'fr-form-section-toggle': section.state.collapsible}\">\n" +
    "                <h2 class=\"header-small\">{{section.title}}</h2>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"fr-form-section-header-actions\">\n" +
    "            <div ng-click=\"toggleCollapse()\" class=\"fr-form-section-header-toggle\" ng-if=\"section.state.collapsible\">\n" +
    "                <button class=\"button icon small\" ng-click=\"section.toggleCollapse()\">\n" +
    "                    <span class=\"fa fa-chevron-up\" ng-if=\"section.state.collapsed\"></span>\n" +
    "                    <span class=\"fa fa-chevron-down\" ng-if=\"!section.state.collapsed\"></span>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"fr-form-section-body\">\n" +
    "        <div ng-repeat=\"field in section.fields | filter: {_prerequisitesMatch: true}\">\n" +
    "            <div\n" +
    "                fr-field-viewer\n" +
    "                ng-class=\"{'fr-field--first': $index === 0}\"\n" +
    "                data-ng-model=\"model[field.name]\"\n" +
    "                data-fields=\"section.fields\"\n" +
    "                data-options=\"field\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("fr/modules/form/views/form-section.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/form-section.html",
    "<div class=\"fr-form-section\">\n" +
    "    <div class=\"fr-form-section-header\">\n" +
    "        <div>\n" +
    "            <span class=\"fr-form-section-title\" ng-class=\"{'fr-form-section-toggle': section.state.collapsible}\" ng-if=\"section.title\">\n" +
    "                <h4 class=\"header-medium\">{{section.title}}</h4>\n" +
    "                <div class=\"fr-form-section-subtitle\" ng-if=\"section.subtitle\">\n" +
    "                    <h5 class=\"header-small plain\">{{section.subtitle}}</h5>\n" +
    "                </div>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"fr-form-section-header-actions\">\n" +
    "            <div class=\"fr-form-section-header-edit\" ng-if=\"section.state.showEditButton\">\n" +
    "                <button class=\"button warning small has-icon\" ng-click=\"section.toggleEditMode()\">\n" +
    "                    <span class=\"fa fa-pencil\"></span>\n" +
    "                    {{ !section.state.editMode ? 'Wijzigen' : 'Ongedaan maken'}}\n" +
    "                </button>\n" +
    "            </div>\n" +
    "            <div class=\"fr-form-section-header-toggle\" ng-if=\"section.state.collapsible\" ng-click=\"toggleCollapse()\">\n" +
    "                <button class=\"button icon small\" ng-click=\"section.toggleCollapse()\">\n" +
    "                    <span class=\"fa fa-chevron-up\" ng-if=\"section.state.collapsed\"></span>\n" +
    "                    <span class=\"fa fa-chevron-down\" ng-if=\"!section.state.collapsed\"></span>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"fr-form-section-body\">\n" +
    "        <div ng-repeat=\"field in section.fields | filter: {_prerequisitesMatch: true}\">\n" +
    "            <div\n" +
    "                fr-field\n" +
    "                ng-class=\"{'fr-field--first': $index === 0}\"\n" +
    "                ng-model=\"model[field.name]\"\n" +
    "                data-fields=\"section.fields\"\n" +
    "                data-options=\"field\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("fr/modules/form/views/form-step-viewer.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/form-step-viewer.html",
    "<div class=\"fr-form-step\">\n" +
    "    <div ng-transclude></div>\n" +
    "    <div ng-if=\"step.state.showTitle\" class=\"fr-form-step-title\">{{step.title}}</div>\n" +
    "    <div\n" +
    "        fr-form-section-viewer=\"section\"\n" +
    "        model=\"model\"\n" +
    "        ng-repeat=\"section in step.sections | filter: {_prerequisitesMatch: true}\">\n" +
    "    </div>\n" +
    "    <div ng-repeat=\"field in step.fields | filter: {_prerequisitesMatch: true}\">\n" +
    "        <div\n" +
    "            fr-field-viewer\n" +
    "            ng-class=\"{'fr-field--first': $index === 0}\"\n" +
    "            ng-model=\"model[field.name]\"\n" +
    "            fields=\"step.fields\"\n" +
    "            options=\"field\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("fr/modules/form/views/form-step.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/form-step.html",
    "<div class=\"fr-form-step\">\n" +
    "    <div ng-transclude></div>\n" +
    "    <div ng-if=\"step.state.showTitle\" class=\"fr-form-step-title\">\n" +
    "        <h2 class=\"header-large\">{{step.title}}</h2>\n" +
    "    </div>\n" +
    "    <div ng-if=\"step.subtitle\" class=\"fr-form-step-subtitle\">\n" +
    "        <h3 class=\"header-medium plain\">{{step.subtitle}}</h3>\n" +
    "    </div>\n" +
    "    <div ng-if=\"step.body\" class=\"fr-form-step-body\" ng-bind-html=\"step.body\"></div>\n" +
    "\n" +
    "    <div\n" +
    "        fr-form-section=\"section\"\n" +
    "        model=\"model\"\n" +
    "        ng-repeat=\"section in step.sections | filter: {_prerequisitesMatch: true}\">\n" +
    "    </div>\n" +
    "    <div ng-repeat=\"field in step.fields | filter: {_prerequisitesMatch: true}\">\n" +
    "        <div\n" +
    "            fr-field\n" +
    "            ng-class=\"{'fr-field--first': $index === 0}\"\n" +
    "            ng-model=\"model[field.name]\"\n" +
    "            fields=\"step.fields\"\n" +
    "            options=\"field\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("fr/modules/form/views/form-tabs.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/form-tabs.html",
    "<div class=\"step-indicator\">\n" +
    "    <div\n" +
    "        class=\"step\"\n" +
    "        data-ng-style=\"{width: width}\"\n" +
    "        data-ng-class=\"{'active': activeIndex === $index}\"\n" +
    "        data-ng-repeat=\"step in steps | filter: {_prerequisitesMatch: true}\">\n" +
    "        <div class=\"truncate\" data-ng-class=\"{'text-center': !step.title}\">\n" +
    "            {{$index + 1}}.<span class=\"step-label\"> {{step.title}}</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("fr/modules/form/views/form-viewer.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/form-viewer.html",
    "<div class=\"fr-form fr-form-viewer list-navigation\">\n" +
    "    <h1 class=\"fr-form-title\">{{frSchema.info.title}}</h1>\n" +
    "    <div ng-transclude></div>\n" +
    "    <form name=\"theform\" novalidate>\n" +
    "        <div ng-repeat=\"step in frSchema.steps | filter: {_prerequisitesMatch: true, type: 'regular'}\">\n" +
    "            <div fr-form-step-viewer=\"step\"\n" +
    "                model=\"model\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div ng-repeat=\"section in frSchema.sections | filter: {_prerequisitesMatch: true}\">\n" +
    "            <div fr-form-section-viewer=\"section\"\n" +
    "                model=\"model\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div ng-repeat=\"field in frSchema.fields | filter: {_prerequisitesMatch: true}\">\n" +
    "            <div\n" +
    "                fr-field-viewer\n" +
    "                ng-class=\"{'fr-field--first': $index === 0}\"\n" +
    "                ng-model=\"model[field.name]\"\n" +
    "                fields=\"frSchema.fields\"\n" +
    "                options=\"field\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>\n" +
    "");
}]);

angular.module("fr/modules/form/views/form.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/form/views/form.html",
    "<div class=\"fr-form {{formClass}} list-navigation\">\n" +
    "    <div fr-form-tabs=\"frSchema.steps\" active=\"active\" active-index=\"activeIndex\" ng-if=\"meta.hasSteps\"></div>\n" +
    "    <h1 class=\"fr-form-title\">{{frSchema.info.title}}</h1>\n" +
    "    <div ng-transclude></div>\n" +
    "    <div ng-if=\"frSchema.info.body\" class=\"fr-form-body\" ng-bind-html=\"frSchema.info.body\"></div>\n" +
    "    <form  name=\"theForm\" novalidate>\n" +
    "        <div\n" +
    "            ng-if=\"meta.hasSteps\"\n" +
    "            model=\"model\"\n" +
    "            fr-form-step=\"getStep(activeStep)\">\n" +
    "        </div>\n" +
    "        <div ng-repeat=\"section in frSchema.sections | filter: {_prerequisitesMatch: true}\">\n" +
    "            <div fr-form-section=\"section\"\n" +
    "                model=\"model\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div ng-repeat=\"field in frSchema.fields | filter: {_prerequisitesMatch: true}\">\n" +
    "            <div\n" +
    "                fr-field\n" +
    "                ng-class=\"{'fr-field--first': $index === 0}\"\n" +
    "                ng-model=\"model[field.name]\"\n" +
    "                fields=\"frSchema.fields\"\n" +
    "                options=\"field\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div\n" +
    "            fr-form-nav=\"frSchema.steps\"\n" +
    "            active=\"active\"\n" +
    "            active-step=\"activeStep\"\n" +
    "            active-step-index=\"activeIndex\"\n" +
    "            can-save-as-concept=\"frSchema.canSaveDraft\"\n" +
    "            ng-if=\"meta.hasSteps\">\n" +
    "        </div>\n" +
    "    </form>\n" +
    "    <div\n" +
    "        fr-debug\n" +
    "        ng-if=\"debug\"\n" +
    "        data=\"debugData\"\n" +
    "        form=\"theForm\"\n" +
    "        schema=\"frSchema\"\n" +
    "        active=\"active\"\n" +
    "        active-step=\"activeStep\"\n" +
    "        active-step-index=\"activeIndex\">\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("fr/modules/validation/views/messages.html", []).run(["$templateCache", function($templateCache) {
  "use strict";
  $templateCache.put("fr/modules/validation/views/messages.html",
    "<div ng-if=\"messages && messages.length > 0\" class=\"fr-field-problem\">\n" +
    "    <p>\n" +
    "        <span ng-repeat=\"message in messages\" >\n" +
    "            {{message.text}}\n" +
    "        </span>\n" +
    "    </p>\n" +
    "</div>");
}]);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.config').constant('frConfig', {
        enums: {
            navigation: {
                NEXT: 'next',
                PREVIOUS: 'previous',
                DIRECT: 'direct'
            },
            entities: {
                FORM: 'form',
                STEP: 'step',
                SECTION: 'section',
                FIELD: 'field'
            },
            stepTypes: {
                INTRO: 'intro',
                OUTRO: 'outro',
                REGULAR: 'regular'
            },
            events: {
                NAVIGATE: 'fr.navigateTo',
                NAVIGATE_STARTED: 'fr.navigateTo.started',
                NAVIGATE_COMPLETED: 'fr.navigateTo.completed',
                NAVIGATION_REDRAW: 'fr.navigation.redraw',
                DEBUG: 'fr.debug',
                SUBMIT: 'fr.onSubmit',
                SUBMIT_STARTED: 'fr.submit.started',
                SUBMIT_COMPLETED: 'fr.submit.completed',
                VALIDATE_ON_SUBMIT: 'validate_field_on_submit',
                VALIDATE_FIELD: 'validate_field',
                DATE_PICKED: 'frDatepicker.picketDate',
                FIELD_CHANGED: 'fr.field.changed',
                SECTION_TOGGLE_EDITMODE: 'fr.section.toggle.editMode'
            },
            errors: {
                FIELD_NOT_FOUND: 'The ID given does not match with any field in our form.'
            },
            debug: {
                SHOWNEXT: 'showNext',
                SHOWPREVIOUS: 'showPrevious',
                SHOWSAVE: 'showSave',
                SHOWSAVEDRAFT: 'showSaveDraft',
                MODEL: 'model'
            }
        },
        fieldConfig: {
            modelTypes: {
                OBJECT: 'object',
                ARRAY: 'array'
            }
        },
        defaultRequiredValidator: {
            "name": "required",
            "type": "required",
            "errorMessage": "this is a required field"
        },
        defaultEntities: {
            form: {
                "formId": "MissingFormId",
                "info": {
                    "title": "Missing form title"
                },
                "name": "Missing Form Name",
                "rendererVersion": "1",
                "canSaveDraft": false,
                "steps": [],
                "sections": [],
                "fields": [],
                "validators": [],
                "navigationTexts": {
                    "next": "Ga verder",
                    "previous": "Vorige",
                    "concept": "Opslaan als concept",
                    "submit": "Verzenden"
                }
            },
            field: {
                "name": "MissingFieldName",
                "spec": {
                    "attributes": {
                        "type": "text",
                        "value": null
                    },
                    "options": {
                        "label": "Missing Label Text",
                        "displayLabel": true,
                        "layout": {
                            "fieldClass": ""
                        }
                    }
                },
                "state": {
                    "editable": true,
                    "editMode": true,
                    "disabled": false,
                    "extraInfoCollapsed": true
                },
                "inputFilter": {
                    "validators": []
                },
                "prerequisites": {}
            },
            section: {
                "id": "MissingSectionId",
                "title": "Missing Section Title",
                "state": {
                    "editable": true,
                    "editMode": true,
                    "collapsible": false,
                    "collapsed": false
                },
                "fields": [],
                "prerequisites": {}
            },
            step: {
                "id": "MissingStepId",
                "title": "Missing Step Title",
                "type": "regular",
                "state": {
                    "showTitle": true,
                    "editable": true,
                    "editMode": true
                },
                "prerequisites": {},
                "sections": []
            }
        },
        defaultArrayPropertiesMap: {
            form: [
                "steps",
                "sections",
                "fields"
            ],
            step: [
                "sections",
                "fields",
                "prerequisites.sectionsCompleted.sections",
                "prerequisites.stepsCompleted.steps",
                "prerequisites.fieldsCompleted.fields",
                "prerequisites.fieldValues.operands"
            ],
            section: [
                "fields",
                "prerequisites.sectionsCompleted.sections",
                "prerequisites.stepsCompleted.steps",
                "prerequisites.fieldsCompleted.fields",
                "prerequisites.fieldValues.operands"
            ],
            field: [
                "prerequisites.sectionsCompleted.sections",
                "prerequisites.stepsCompleted.steps",
                "prerequisites.fieldsCompleted.fields",
                "prerequisites.fieldValues.operands",
                "spec.options.fileinfo.allowedExtensions",
                "spec.options.valueOptions",
                "spec.options.fields",
                "inputFilter.validators"
            ]
        }
    });
})(window.angular);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').controller('frFieldController', [
        '$scope',
        '$q',
        '$controller',
        'frConfig',
        'frFieldConfig',
        'frUtils',
        function frFieldController($scope, $q, $controller, config, frFieldConfig, frUtils) {

            /** @type {Object} */
            $scope.field = {};

            /**
             * @name  initialize controller ----------------------
             */
            function initialize() {
                var fieldType,
                    fieldTypeConf;

                fieldType = $scope.options.spec.attributes.type;
                fieldTypeConf = frFieldConfig.getType(fieldType);
                createDefaults($scope.options);
                mergeFieldOptionsWithTypeDefaults($scope.options, fieldTypeConf);
                invokeControllers($scope, $scope.field, fieldTypeConf);
            }

            /**
             * @function
             * @name  createDefaults ----------------------------
             * @description fill options with default values
             * @param  {object} options - field options
             */
            function createDefaults(options) {
                options = options || {};
                frUtils.reverseDeepMerge(options, {
                    spec : {
                        attributes: {
                            id: $scope.options.name,
                            name: $scope.options.name
                        }
                    }
                });
                // Short template notation
                $scope.field.spec = $scope.options.spec;
                $scope.field.to = $scope.options.spec.options;
                $scope.field.attrs = $scope.options.spec.attributes;
                $scope.field.validation = $scope.options.inputFilter;
                $scope.field.state = $scope.options.state;
                // Create model copy on initialize to create a variabe ngModelInit
                // now we have a reference to the initial model
                $scope.ngModelInit = _.cloneDeep($scope.ngModel);
            }

            /**
             * @function
             * @name  mergeFieldOptionsWithTypeDefaults ------------
             * @description merge field options properties with the
             * type defaults
             * @param  {Object} options - field options
             * @param  {type} type - field config
             */
            function mergeFieldOptionsWithTypeDefaults(options, type) {
                if (type) {
                    mergeOptions(options, type.defaultOptions);
                    _.assign(options, {
                        templateUrl: type.templateUrl,
                        baseType: type.extend ? type.extend : type.name
                    });
                    // short template notation
                    frUtils.reverseDeepMerge($scope.field, {
                        templateUrl: options.templateUrl,
                        baseType: options.baseType
                    });
                }
            }

            /**
             * @function
             * @name  mergeOptions --------------------------
             * @description merge config default options with the field
             * options
             * @param  {Object} options - field options
             * @param  {Function|Object} extraOptions - defaultOptions
             */
            function mergeOptions(options, extraOptions) {
                if (extraOptions) {
                    if (_.isFunction(extraOptions)) {
                        extraOptions = extraOptions(options);
                    }
                    frUtils.reverseDeepMerge(options, extraOptions);
                }
            }

            /**
             * @function
             * @name  invokeController -----------------------------
             * @description invoke all controller function
             * @param  {Object} scope
             * @param  {Object} options
             * @param  {Object} type
             */
            function invokeControllers(scope, options, type) {
                options = options || {};
                type = type || {};
                _.forEach([type.controller, options.controller], function(controller) {
                    if (controller) {
                        $controller(controller, {$scope: scope});
                    }
                });
            }

            /**
             * @function
             * @name innerFieldExists ----------------------------
             * @description - checks if we can find an inner field inside an inner field array list
             * @param  {Array} innerFields - array of objects
             * @param  {String} innerFieldName - the name from the inner field we want to find
             * @return {Object} - returns undefined when no innerfield was found
             */
            function innerFieldExists(innerFields, innerFieldName) {
                return _.find(innerFields, function findInnerfield(innerField) {
                    return innerFieldName === innerField.name;
                });
            }

            /**
             * @function
             * @name  validateField --------------------------------
             * @description - broadcast a validateField event with paramaters.
             * - fieldName: name of a field
             * - validateSucces: succes callback function
             * - validateError: error callback function
             * @param  {String} innerFieldName - the field name
             * @return {Promise} - returns a promise object
             */
            $scope.field.validateInnerField = function validateInnerField(innerFieldName) {

                var deferred = $q.defer(),
                    innerFields = $scope.field.spec.options.fields;

                // only trigger the validation for true inner fields
                if (!innerFieldExists(innerFields, innerFieldName)) {
                    var error = [
                        'Error while trying to validate an inner field. ',
                        'Could not find an inner field with name: ' + innerFieldName
                    ].join('');
                    throw error;
                }

                $scope.$broadcast(config.enums.events.VALIDATE_FIELD, innerFieldName, function validateSucces(data) {
                    deferred.resolve(data);
                }, function validateError(data) {
                    deferred.reject(data);
                });

                return deferred.promise;
            };

            /**
             * @function
             * @name  toggleEditMode -----------------------------
             * @description - toggles the state property `editMode`
             */
            $scope.field.toggleEditMode = function toggleEditMode() {
                $scope.field.state.editMode = !$scope.field.state.editMode;
                // reset the model with the initial value
                $scope.ngModel = _.cloneDeep($scope.ngModelInit);
            };

            /**
             * @function
             * @name  getValueFromOptions -----------------------
             * @description - get a value from the value options by key
             * @param  {String} key - value option key
             * @return {String} - option value
             */
            $scope.field.getValueFormOptions = function getValueFromOptions(key) {
                var option = _.find($scope.field.to.valueOptions, function (option) {
                    return option.key === key;
                });
                return option && option.value;
            };

            /**
             * @description - when the section state `editMode` was toggled we need to change
             * the editMode for each field inside the section
             */
            $scope.$on(config.enums.events.SECTION_TOGGLE_EDITMODE, function (event, editMode) {
                if ($scope.field.state.editable) {
                    $scope.field.state.editMode = editMode;
                }
            });

            initialize();
        }
    ]);
})(window.angular, window._);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').controller('frFormSectionController', [
        '$scope',
        'frConfig',
        '$element',
        function frFormSectionController($scope, config,$element) {
            var body = $element.find('.fr-form-section-body');
            var speed = 200;

            function init() {
                if ($scope.section.state.collapsed) {
                    body.css('display','none');
                }
            }

            /**
             * @function
             * @name  toggleCollapse -------------
             * @description - toggle the collapse state if a section is collapsible
             */
            $scope.toggleCollapse = function toggleCollapse() {
                if ($scope.section.state.collapsible) {
                    $scope.section.state.collapsed = !$scope.section.state.collapsed;
                    if ($scope.section.state.collapsed) {
                        body.slideUp(speed);
                    } else {
                        body.slideDown(speed);
                    }
                }
            };

            /**
             * @function
             * @name  toggleEditMode -------------
             * @description - toggle the editMode from a section.
             * Broadcast an event and let all fields subcribe on this event
             */
            $scope.section.toggleEditMode = function toggleEditMode() {
                $scope.section.state.editMode = !$scope.section.state.editMode;
                if ($scope.section.state.collapsed) {
                   $scope.toggleCollapse();
                }
                $scope.$broadcast(config.enums.events.SECTION_TOGGLE_EDITMODE, $scope.section.state.editMode);
            };

            init();
        }
    ]);
})(window.angular);
;(function(ng, _){
    'use strict';
    ng.module('formRenderer.form').controller('frFormController', [
        '$anchorScroll',
        '$location',
        '$q',
        '$scope',
        'frConfig',
        'frSchemaValidator',
        'frSchemaParser',
        'frPrerequisitesParser',
        'frModelParser',
        'frUtils',
        function frFormController($anchorScroll, $location, $q, $scope, config, schemaValidator, schemaParser, prerequisitesParser, modelParser, frUtils) {

            var frSchema;
            var frModel;

            /**
             * @function
             * @name Initialize ---------------------
             */
            function initialize () {
                evaluateSchema();
            }

            /**
             * @function
             * @name  evaluateSchema ----------------------------------------------------
             * @description - make sure the schema is ready to render
             * @param  {Boolean} updateModel - true if the model needs an update
             */
            function evaluateSchema(updateModel) {
                frSchema = $scope.schema;

                setMeta();

                frSchema = schemaValidator.validate(frSchema);
                frSchema = schemaParser.parse(frSchema);
                frModel = modelParser.createModel(frSchema);

                if (updateModel) {
                    var modelCopy = _.cloneDeep($scope.model);
                    frUtils.extendDeep(frModel, modelCopy);
                    // update model inside debug panel
                    $scope.$emit(config.enums.events.DEBUG, config.enums.debug.MODEL, frModel);
                }

                frSchema = prerequisitesParser.parse(frSchema);

                $scope.frSchema = frSchema;
                $scope.model = frModel;

                $scope.meta.schemaReady = true;
            }

            /**
             * @function
             * @name removeUnvisibleFieldsFromModel -------------------------------------
             * @description - Removes unvisible fields from the model (hidden fields will not be removed!)
             *                Only the fields that don't match his prerequisites will be removed from the model
             * @return {Object}
             */
            function removeUnvisibleFieldsFromModel(model, unvisibleFields) {
                function getVisibleFieldsModel(fieldValue, fieldKey) {
                    if (_.isPlainObject(fieldValue)) {
                        return _.reduce(fieldValue, function (result, innerFieldValue, innerFieldKey) {
                            result[innerFieldKey] = getVisibleFieldsModel(innerFieldValue, innerFieldKey);
                            return result;
                        }, {});
                    }
                    return (unvisibleFields.indexOf(fieldKey) === -1) ? fieldValue : undefined;
                }

                return _.reduce(model, function (newModel, value, key) {
                    var visibleFieldsModel = getVisibleFieldsModel(value, key);
                    if (visibleFieldsModel) {
                        newModel[key] = visibleFieldsModel;
                    }
                    return newModel;
                }, {});
            }

            /**
             * @function
             * @name getInnerFieldNames --------------------------------------------------
             * @description - return array that will include all fieldNames inside the formModel
             *                form the internalcache
             * @return {Array}
             */
            function getInnerFieldNames(items) {
                return _.reduce(items, function (result, item, key) {
                    result = [].concat(result, _.keys(item._internalCache.formModel));
                    return result;
                }, []);
            }

            /**
             * @function
             * @name setMeta -------------------------------------------------------------
             * @description - set meta data
             */
            function setMeta () {
                $scope.meta = {
                    schemaReady: true,
                    hasSteps: true,
                    showIntro: false
                };
            }

            /**
             * @function
             * @name  reEvaluateSchema ---------------------------------------------------
             * @description - re-evaluate the form schema and update the model
             */
            this.reEvaluateSchema = evaluateSchema;

            /**
             * @function
             * @name validateForm --------------------------------------------------------
             * @description validate the form by sending an event to all the fields
             * inside the form. wait till all the validation promisis are resolved
             * @return {object} - promise
             */
            this.validateForm = function validateForm() {
                var deferred = $q.defer();
                var fieldValidatorPromises = [];
                $scope.$broadcast(config.enums.events.VALIDATE_ON_SUBMIT, fieldValidatorPromises);
                $q.all(fieldValidatorPromises).then(function isValid() {
                    deferred.resolve(true);
                }, function notValid() {
                    deferred.reject(false);
                });
                return deferred.promise;
            };

            /**
             * @function
             * @name getFormModel --------------------------------------------------------
             * @description - get a form model copy
             * @return {Object} - form model
             */
            this.getFormModel = function getFormModel() {
                var modelCopy = _.cloneDeep($scope.model);
                return modelCopy || {};
            };

            /**
             * @function
             * @name getPrerequisitesMatchStepModel --------------------------------------
             * @description - generate a model that only includes fields visible inside
             *                the active step. The prerequisites are matching for each field
             *                inside the generated model
             * @return {object}
             */
            this.getPrerequisitesMatchStepModel = function getPrerequisitesMatchStepModel(stepId) {
                var activeStepIC = this.getStepById(stepId)._internalCache;
                var schemaIC = _.cloneDeep($scope.schema)._internalCache;
                var modelCopy = _.cloneDeep($scope.model);

                var ActiveStepFields = _.reduce(schemaIC.field, function (result, field, key) {
                    _.forEach(activeStepIC.formModel, function (value, activeStepModelKey) {
                        if (field.name === activeStepModelKey) {
                            result.push(field);
                        }
                    });
                   return result;
                }, []);

                // find all fields that don't match the prerequisites
                // These fields are not visible to the user
                var unvisibleFields = _.pluck(_.filter(ActiveStepFields, { _prerequisitesMatch: false }), 'name');
                var allActiveFieldNames = _.pluck(ActiveStepFields, 'name');
                // create step model
                var activeStepModel = _.reduce(_.keys(modelCopy), function (result, key) {
                   if (allActiveFieldNames.indexOf(key) !== -1) {
                       result[key] = modelCopy[key];
                   }
                   return result;
                }, {});
                return removeUnvisibleFieldsFromModel(activeStepModel, unvisibleFields);
            };

            /**
             * @function
             * @name getPrerequisitesMatchFormModel ---------------------------------------
             * @description - generate a model that only includes fields visible inside
             *                the form. The prerequisites are matching for each field
             *                inside the generated model.
             * @return {object}
             */
            this.getPrerequisitesMatchFormModel = function getPrerequisitesMatchFormModel() {
                var internalCache = _.cloneDeep($scope.schema)._internalCache;
                var filterStep = _.filter(internalCache.step, { _prerequisitesMatch: false });
                var filterSection = _.filter(internalCache.section, { _prerequisitesMatch: false });
                var filterField = _.filter(internalCache.field, { _prerequisitesMatch: false });

                var unvisibleFields = _.uniq([].concat(getInnerFieldNames(filterStep), getInnerFieldNames(filterSection), _.pluck(filterField, 'name')));

                return removeUnvisibleFieldsFromModel(_.cloneDeep($scope.model), unvisibleFields);
            };

            /**
             * @function
             * @name  getFormId ----------------------------------------------------------
             * @description - return the form id
             * @return {String/Int} - id
             */
            this.getFormId = function getFormId() {
                return $scope.frSchema.formId;
            };

            /**
             * @function
             * @name  getStepIndex -------------------------------------------------------
             * @description - return the step index of a given stepId
             * @param {String} stepId - the id of the step you want the index for
             * @return {Int} - index
             */
            this.getStepIndex = function getStepIndex(stepId) {
                var step = _.find($scope.frSchema.steps, { id: stepId });

                if (!step) {
                    return undefined;
                }

                return step._originalStepIndex;
            };

            /**
             * @function
             * @name  getStepById --------------------------------------------------------
             * @description - return the step of a given stepId
             * @param {String} stepId - the id of the step you want
             * @return {Object} - the step
             */
            this.getStepById = function getStepById(stepId) {
                return _.find($scope.frSchema.steps, { id: stepId });
            };

            /**
             * @function
             * @name  getStepByIndex -----------------------------------------------------
             * @description - return the step of a given index
             * @param {Int} index - the index of the step you want
             * @return {Object} - the step
             */
            this.getStepByIndex = function getStepByIndex(index) {
                return _.find($scope.frSchema.steps, { _stepIndex: index });
            };

            /**
             * @function
             * @name getDataStore --------------------------------------------------------
             * @description - returns a data store. A data store can be used by a
             *                range of custom input fields. (select, radio list, checkbox list)
             * @param  {String} name - Name of the data store
             * @return {Array} - A list of
             */
            this.getDataStore = function getDataStore(name) {
                var result;
                if ($scope.frSchema.dataStores) {
                    result = $scope.frSchema.dataStores[name];
                }
                return result;
            };

            /**
             * @function
             * @name scrollTo ------------------------------------------------------------
             * @description - scroll to element
             * @param {Object} element
             * @param {Int} duration Animation time
             * @param {Int} scrollOffset Offset
             */
            this.scrollTo = function(element, durationP, scrollOffset) {

                /**
                 * @function
                 * @name scaleTimeToDistance
                 * @description - Longer scroll duration for longer distances
                 * @param {Int} distance
                 * @param {Int} duration
                 */
                function scaleTimeToDistance(distance, duration) {
                    var baseDistance = 500;
                    var distanceAbs = Math.abs(distance);
                    var min = duration / 10;
                    return duration * distanceAbs / baseDistance + min;
                }

                /**
                 * @function
                 * @name easeIntOutQuadt
                 * @description - ease in out animation
                 * @param {Int} t Current time
                 * @param {Int} b Beginning value
                 * @param {Int} c Change in value
                 * @param {Int} d Duration
                 * NOTE: https://github.com/danro/jquery-easing/blob/master/jquery.easing.js
                 *       http://upshots.org/actionscript/jsas-understanding-easing
                 */
                function easeInOutQuadt(t, b, c, d) {
                     if ((t /= d / 2) < 1) {
                         return c / 2 * t * t + b;
                     }
                     return -c / 2 * ((--t) * (t - 2) - 1) + b; // jshint ignore:line
                }

                var targetY = element.getBoundingClientRect().top + parseInt(scrollOffset, 10),
                    targetX = element.getBoundingClientRect().left,
                    duration = scaleTimeToDistance(targetY, durationP);

                var start = window.pageYOffset,
                    increment = 20,
                    change = targetY,
                    currentTime = 0;

                /**
                 * @function
                 * @name animateScroll
                 * @description - create scroll to animation
                 */
                function animateScroll() {
                    currentTime = currentTime + increment;
                    var val = easeInOutQuadt(currentTime, start, change, duration);
                    window.scrollTo(targetX, val);

                    if (currentTime < duration) {
                        setTimeout(animateScroll, increment);
                    } else {
                        element.focus();
                    }
                }
                // return if no animation is required
                if (change === 0) {
                    element.focus();
                    return;
                }

                animateScroll();
            };

            initialize();
        }
    ]);
})(window.angular, window._);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frDebug', [
        '$timeout',
        function frDebugDirective($timeout) {
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/debug-panel.html',
                replace: true,
                scope: {
                    data: '=',
                    active: '=',
                    activeStep: '=',
                    activeStepIndex: '=',
                    schema: '=',
                    form: '='
                },
                link: function(scope, element, attrs, ctrl) {
                    
                    scope.state = {
                        debug: true
                    };
                       
                    /**
                     * @function
                     * @name toggle state --------------------------------
                     * @description - toggle state debug panel
                     * @param {String} name - state name
                     */
                    scope.toggleState = function (name) {
                        scope.state = scope.state || {};
                        scope.state[name] = !scope.state[name];
                    };
                    
                    /**
                     * @function
                     * @name toggle state --------------------------------
                     * @description - creates a json string from the data and return it
                     * @param {Object} data - print data
                     * @return {String}
                     */
                    scope.printData = function (data) {
                        var returnvalue = JSON.stringify(data, null, 2);
                        return returnvalue;
                    };
                }
            };
        }
    ]);
})(window.angular);
;
(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').directive('frFieldViewer', [
        '$interpolate',
        '$q',
        '$http',
        '$compile',
        '$templateCache',
        'frFieldConfig',
        'frUtils',
        'frConfig',
        function frInputfieldViewerDirective($interpolate, $q, $http, $compile, $templateCache, frFieldConfig, frUtils, frConfig) {
            return {
                restrict: 'AE',
                scope: {
                    options: '=',
                    ngModel: '=',
                    fields: '=?'
                },
                require: ['^frFormViewer', '^ngModel'],
                replace: true,
                controller: 'frFieldController',
                link: function linkInputfield(scope, element, attrs, ctrl) {

                    var type;
                    var args = arguments;
                    var fieldCount = 0;
                    var fr = this;

                    /**
                     * @function
                     * @name  initialize -----------------------------------------
                     * @description - initialize field viewer directive
                     */
                    function initialize() {
                        type = frFieldConfig.getType(scope.options.spec.attributes.type);
                        var fieldTemplate;
                        getFieldTemplate(scope.options.spec).then(function (template) {
                            fieldTemplate = template;
                            return transcludeInTempWrappers(scope.options.spec);
                        }).then(function (transcludeTemplate) {
                            return transcludeTemplate(fieldTemplate).then(function(data) {
                                return setElementTemplate(data);
                            });
                        }).then(function (templateString) {
                            return watchFormControl(templateString);
                        }).then(function() {
                            return callLinkFunctions();
                        }).catch(function error(err) {
                            throw new Error('there was a problem setting the template for this field');
                        });
                    }

                    /**
                     * @function
                     * @name  getFieldTemplate ----------------------------------
                     * @description check if a templateUrl was set
                     * then load the template
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function getFieldTemplate(options) {
                        var type = frFieldConfig.getType(options.attributes.type);
                        var template;
                        var templateUrl = type.viewTemplateUrl;
                        if (!templateUrl) {
                            template = '<label>{{field.to.label}}: </label><span>{{ngModelInit}}</span>';
                        }

                        return getTemplate(templateUrl || template, angular.isUndefined(template) ,options);
                    }

                    /**
                     * @function
                     * @name  getTemplate ---------------------------------------
                     * @description get the field template
                     * @param  {Function|String} template - the template url
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function getTemplate(template, isUrl, options) {
                        var templatePromise;

                        if(_.isFunction(template)) {
                            templatePromise = $q.when(template(options));
                        } else {
                            templatePromise = $q.when(template);
                        }

                        if (!isUrl) {
                            return templatePromise;
                        } else {
                            var httpOptions = { cache: $templateCache };
                            return templatePromise.then(function(url) {
                                return $http.get(url, httpOptions);
                            }).then(function(response) {
                                return response.data;
                            }).catch(function handlingErrorGettingATemplate(error) {
                                console.warn('problem loading template');
                            });
                        }
                    }

                    /**
                     * @function
                     * @name  transcludeInTempWrappers --------------------------
                     * @description tansclude all wrapper templates and the
                     * field template (wrapper 0 -> wrappers 1 -> field template)
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function transcludeInTempWrappers(options) {
                        var wrappers = getTempWrapperOptions(options);

                        return function transcludeTemplate(template) {
                            if (!wrappers.length) {
                                // nothing to transclude
                                // return field template promise
                                return $q.when(template);
                            }

                            var promises = [];
                            _.forEach(wrappers, function(wrapper) {
                                promises.push(getTemplate(wrapper.templateUrl, true));
                            });

                            return $q.all(promises).then(function(wrappersTemplates) {
                                wrappersTemplates.forEach(function(wrapperTemplate, index) {
                                    checkTempWrapper(wrapperTemplate, wrappers[index]);
                                });
                                // wrapper 0 is wrapped in wrapper 1...
                                wrappersTemplates.reverse();
                                var totalWrapper = wrappersTemplates.shift();
                                _.forEach(wrappersTemplates, function(wrapperTemplate) {
                                    totalWrapper = doTransclusion(totalWrapper, wrapperTemplate);
                                });
                                return doTransclusion(totalWrapper, template);
                            });
                        };
                    }

                    /**
                     * @function
                     * @name  doTransclusion --------------------------------
                     * @description search for 'fr-transclude' and replace it with
                     * the given template
                     * @param  {String} wrapper - template wrapper
                     * @param  {String} template - template that will replace the 'fr-transclude' tag
                     * @return {String} - return replaced html
                     */
                    function doTransclusion(wrapper, template) {
                        var superWrapper = ng.element('<a></a>'); // allow single root in wrappers
                        superWrapper.append(wrapper);
                        var transcludeEl = superWrapper.find('fr-transclude');
                        transcludeEl.replaceWith(template);
                        return superWrapper.html();
                    }

                    /**
                     * @function
                     * @name checkTempWrapper ------------------------------
                     * @description check if there is a '<fr-transclude>' tag
                     * available in the wrapper
                     * @param  {string} template - wrapper template
                     * @param  {object} info - field info
                     */
                    function checkTempWrapper(template, info) {
                        var frTransclude = '<fr-transclude></fr-transclude>';
                        if (template.indexOf(frTransclude) === -1) {
                            throw new Error('could not find <fr-transclude></fr-transclude> in the wrapper template. info: ' + JSON.stringify(info));
                        }
                    }

                    /**
                     * @function
                     * @name  setElementTemplate --------------------------
                     * @description set element html and $compile it with
                     * the scope
                     * @param {string} templateString - html string
                     */
                    function setElementTemplate(templateString) {
                        element.html(asHtml(templateString));
                        $compile(element.contents())(scope);
                        return templateString;
                    }

                    /**
                     * @function
                     * @name  getTempWrapperOptions -----------------------
                     * @description get all wrappers that should be rendered
                     * before we render the field
                     * @param  {object} options - field options
                     * @return {Array} - all wrappers
                     */
                    function getTempWrapperOptions(options) {
                        // get all wrappers by type
                        var wrapper = frUtils.arrayify(frFieldConfig.getTempWrapperByType(options.attributes.type));

                        // get all wrappers specified in the type object
                        var type = frFieldConfig.getType(options.attributes.type);
                        if (type && type.wrapper) {
                            var newTypeWrappers = [];
                            var typeWrappers = frUtils.arrayify(type.viewWrapper);
                            _.forEach(typeWrappers, function(wrapperName) {
                                newTypeWrappers.push(frFieldConfig.getTempWrapper(wrapperName));
                            });
                            _.forEach(newTypeWrappers, function(newWrapper) {
                                wrapper.push(newWrapper);
                            });
                            typeWrappers = newTypeWrappers;
                        } else if (type.wrapper === null) {
                            // explicit null on in field config means no wrapper
                            return [];
                        }

                        // add the default wrapper
                        var defaultTempWrapper = frFieldConfig.getTempWrapper();
                        if (defaultTempWrapper) {
                            wrapper.push(defaultTempWrapper);
                        }

                        return wrapper;
                    }

                    /**
                     * @function
                     * @name  watchFormControl ---------------------------
                     * @description find all models inside the field template
                     * check if the model exists and watch for changes
                     * set the formControl on the field options object when a change
                     * has occurred
                     * @param  {String} templateString
                     */
                    function watchFormControl(templateString) {
                        var templateElement = ng.element(templateString);
                        var models = templateElement[0].querySelectorAll('[ng-model], [data-ng-model]');

                        _.forEach(models, function (model) {
                            fieldCount = fieldCount + 1;
                            watchFieldNameOrExistence(model.getAttribute('name'));
                        });
                    }

                    /**
                     * @function
                     * @name  watchFieldNameOrExistence ------------------
                     * @description check with a regex if the field name
                     * needs to be interpolated with the current scope
                     * @param  {String} name - field name
                     */
                    function watchFieldNameOrExistence(name) {
                        var nameRegex = /\{\{(.*?)}}/;
                        var nameExpression = nameRegex.exec(name);
                        if (nameExpression) {
                            name = $interpolate(name)(scope);
                        }
                        watchFieldExistence(name);
                    }

                    /**
                     * @function
                     * @name  watchFieldExistence ------------------------
                     * @description watch the fieldproperty on the form object
                     * NOTE: Angular create a form object which will hold all the
                     * fiels inside that form.
                     * @param  {string} name - field name
                     */
                    function watchFieldExistence(name) {
                        var watchName = "form[" +"'"+ name +"'"+ "]";
                        scope.$watch(watchName, function formControlChange(formControl) {
                            if (formControl) {
                                // if there is more then one field inside a template
                                // create an array with all formControls
                                if (fieldCount > 1) {
                                    if (!scope.options.formControl) {
                                        scope.options.formControl = [];
                                    }
                                    scope.options.formControl.push(formControl);
                                } else {
                                    scope.options.formControl = formControl;
                                }
                                scope.fc = scope.options.formControl; // formControl shortcut for template
                            }
                        });
                    }

                    /**
                     * @function
                     * @name callLinkFunctions --------------------------
                     * @description call the function on the field type
                     */
                    function callLinkFunctions() {
                        if (type && type.link) {
                            type.link.apply(fr, args);
                        }
                    }

                    /**
                     * @function
                     * @name  asHtml ------------------------------------
                     * @description create angular element with wrapper and
                     * return the html
                     * @param  {string} el - element template
                     * @return {string} - html
                     */
                    function asHtml(el) {
                        var wrapper = ng.element('<a></a>');
                        return wrapper.append(el).html();
                    }

                    initialize();
                }
            };
        }
    ]);
})(window.angular, window._);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').directive('frField', [
        '$interpolate',
        '$q',
        '$http',
        '$compile',
        '$templateCache',
        'frFieldConfig',
        'frUtils',
        function frInputfieldDirective($interpolate, $q, $http, $compile, $templateCache, frFieldConfig, frUtils) {
            return {
                restrict: 'AE',
                scope: {
                    options: '=',
                    ngModel: '=',
                    fields: '=?'
                },
                require: ['^frForm', '^ngModel'],
                replace: true,
                controller: 'frFieldController',
                link: function linkInputfield(scope, element, attrs, ctrl) {

                    var type;
                    var args = arguments;
                    var fieldCount = 0;
                    var fr = this;

                    /**
                     * @function
                     * @name  initialize ---------------------------
                     * @description - initialize field directive
                     */
                    function initialize() {
                        type = frFieldConfig.getType(scope.options.spec.attributes.type);
                        var fieldTemplate;
                        getFieldTemplate(scope.options.spec).then(function (template) {
                            fieldTemplate = template;
                            //console.log('transclude in temp wrappers');
                            return transcludeInTempWrappers(scope.options.spec);
                        }).then(function (transcludeTemplate) {
                            //console.log('set element template');
                            return transcludeTemplate(fieldTemplate).then(function(data) {
                                return setElementTemplate(data);
                            });
                        }).then(function (templateString) {
                            return watchFormControl(templateString);
                        }).then(function() {
                            //console.log('call link functions');
                            return callLinkFunctions();
                        }).catch(function error(err) {
                            throw new Error('there was a problem setting the template for this field');
                        });
                    }

                    /**
                     * @function
                     * @name  getFieldTemplates --------------------
                     * @description check if a templateUrl was set
                     * then load the template
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function getFieldTemplate(options) {
                        var type = frFieldConfig.getType(options.attributes.type);
                        var templateUrl = type.templateUrl;
                        var template = type.template;
                        if (_.isUndefined(template) && !templateUrl) {
                            throw 'no template found for ' + options.attributes.type + ' field type';
                        }

                        return getTemplate(templateUrl || template, _.isUndefined(template), options);
                    }

                    /**
                     * @function
                     * @name  getTemplate --------------------------
                     * @description get the field template
                     * @param  {Function|string} template - the template url
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function getTemplate(template, isUrl, options) {
                        var templatePromise;

                        if(_.isFunction(template)) {
                            templatePromise = $q.when(template(options));
                        } else {
                            templatePromise = $q.when(template);
                        }

                        if (!isUrl) {
                            return templatePromise;
                        } else {
                            var httpOptions = { cache: $templateCache };
                            return templatePromise.then(function(url) {
                                return $http.get(url, httpOptions);
                            }).then(function(response) {
                                return response.data;
                            }).catch(function handlingErrorGettingATemplate(error) {
                                console.warn('problem loading template');
                            });
                        }
                    }

                    /**
                     * @function
                     * @name  transcludeInTempWrappers ----------------
                     * @description tansclude all wrapper templates and the
                     * field template (wrapper 0 -> wrappers 1 -> field template)
                     * @param  {object} options - field options
                     * @return {promise}
                     */
                    function transcludeInTempWrappers(options) {
                        var wrappers = getTempWrapperOptions(options);

                        return function transcludeTemplate(template) {
                            if (!wrappers.length) {
                                // nothing to transclude
                                // return field template promise
                                return $q.when(template);
                            }

                            var promises = [];
                            _.forEach(wrappers, function(wrapper) {
                                promises.push(getTemplate(wrapper.templateUrl, true));  
                            });

                            return $q.all(promises).then(function(wrappersTemplates) {
                                wrappersTemplates.forEach(function(wrapperTemplate, index) {
                                    checkTempWrapper(wrapperTemplate, wrappers[index]);
                                });
                                // wrapper 0 is wrapped in wrapper 1...
                                wrappersTemplates.reverse();
                                var totalWrapper = wrappersTemplates.shift();
                                _.forEach(wrappersTemplates, function(wrapperTemplate) {
                                    totalWrapper = doTransclusion(totalWrapper, wrapperTemplate);
                                });
                                return doTransclusion(totalWrapper, template);
                            });
                        };
                    }

                    /**
                     * @function
                     * @name  doTransclusion --------------------------
                     * @description search for 'fr-transclude' and replace it with
                     * the given template
                     * @param  {String} wrapper - template wrapper
                     * @param  {String} template - template that will replace the 'fr-transclude' tag
                     * @return {String} - return replaced html
                     */
                    function doTransclusion(wrapper, template) {
                        var superWrapper = ng.element('<a></a>'); // allow single root in wrappers
                        superWrapper.append(wrapper);
                        var transcludeEl = superWrapper.find('fr-transclude');
                        transcludeEl.replaceWith(template);
                        return superWrapper.html();
                    }

                    /**
                     * @function
                     * @name checkTempWrapper ------------------------
                     * @description check if there is a '<fr-transclude>' tag
                     * available in the wrapper
                     * @param  {string} template - wrapper template
                     * @param  {object} info - field info
                     */
                    function checkTempWrapper(template, info) {
                        var frTransclude = '<fr-transclude></fr-transclude>';
                        if (template.indexOf(frTransclude) === -1) {
                            throw new Error('could not find <fr-transclude></fr-transclude> in the wrapper template. info: ' + JSON.stringify(info));
                        }
                    }

                    /**
                     * @function
                     * @name  setElementTemplate ----------------------
                     * @description set element html and $compile it with
                     * the scope
                     * @param {string} templateString - html string
                     */
                    function setElementTemplate(templateString) {
                        element.html(asHtml(templateString));
                        $compile(element.contents())(scope);
                        return templateString;
                    }

                    /**
                     * @function
                     * @name  getTempWrapperOptions --------------------
                     * @description get all wrappers that should be rendered
                     * before we render the field
                     * @param  {object} options - field options
                     * @return {Array} - all wrappers
                     */
                    function getTempWrapperOptions(options) {
                        // get all wrappers by type
                        var wrapper = frUtils.arrayify(frFieldConfig.getTempWrapperByType(options.attributes.type));

                        // get all wrappers specified in the type object
                        var type = frFieldConfig.getType(options.attributes.type);
                        if (type && type.wrapper) {
                            var newTypeWrappers = [];
                            var typeWrappers = frUtils.arrayify(type.wrapper);
                            _.forEach(typeWrappers, function(wrapperName) {
                                newTypeWrappers.push(frFieldConfig.getTempWrapper(wrapperName));
                            });
                            _.forEach(newTypeWrappers, function(newWrapper) {
                                wrapper.push(newWrapper);
                            });
                            typeWrappers = newTypeWrappers;
                        } else if (type.wrapper === null) {
                            // explicit null on in field config means no wrapper
                            return [];
                        }

                        // add the default wrapper
                        var defaultTempWrapper = frFieldConfig.getTempWrapper();
                        if (defaultTempWrapper) {
                            wrapper.push(defaultTempWrapper);
                        }

                        return wrapper;
                    }

                    /**
                     * @function
                     * @name  watchFormControl --------------------------------
                     * @description find all models inside the field template
                     * check if the model exists and watch for changes
                     * set the formControl on the field options object when a change
                     * has occurred
                     * @param  {String} templateString
                     */
                    function watchFormControl(templateString) {
                        var templateElement = ng.element(templateString);
                        var models = templateElement[0].querySelectorAll('[ng-model], [data-ng-model]');

                        _.forEach(models, function (model) {
                            fieldCount = fieldCount + 1;
                            watchFieldNameOrExistence(model.getAttribute('name'));
                        });
                    }

                    /**
                     * @function
                     * @name  watchFieldNameOrExistence ------------------------
                     * @description check with a regex if the field name
                     * needs to be interpolated with the current scope
                     * @param  {String} name - field name
                     */
                    function watchFieldNameOrExistence(name) {
                        var nameRegex = /\{\{(.*?)}}/;
                        var nameExpression = nameRegex.exec(name);
                        if (nameExpression) {
                            name = $interpolate(name)(scope);
                        }
                        watchFieldExistence(name);
                    }

                    /**
                     * @function
                     * @name  watchFieldExistence -----------------------------
                     * @description watch the fieldproperty on the form object
                     * NOTE: Angular create a form object which will hold all the
                     * fiels inside that form.
                     * @param  {string} name - field name
                     */
                    function watchFieldExistence(name) {
                        var watchName = "form[" +"'"+ name +"'"+ "]";
                        scope.$watch(watchName, function formControlChange(formControl) {
                            if (formControl) {
                                // if there is more then one field inside a template
                                // create an array with all formControls
                                if (fieldCount > 1) {
                                    if (!scope.options.formControl) {
                                        scope.options.formControl = [];
                                    }
                                    scope.options.formControl.push(formControl);
                                } else {
                                    scope.options.formControl = formControl;
                                }
                                scope.fc = scope.options.formControl; // formControl shortcut for template
                            }
                        });
                    }

                    /**
                     * @function
                     * @name callLinkFunctions ---------------------
                     * @description call the function on the field type
                     * object
                     */
                    function callLinkFunctions() {
                        if (type && type.link) {
                            type.link.apply(fr, args);
                        }
                    }

                    /**
                     * @function
                     * @name  asHtml -------------------------------
                     * @description create angular element with wrapper and
                     * return the html
                     * @param  {string} el - element template
                     * @return {string} - html
                     */
                    function asHtml(el) {
                        var wrapper = ng.element('<a></a>');
                        return wrapper.append(el).html();
                    }

                    initialize();
                }
            };
        }
    ]);
})(window.angular, window._);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormNav', [
        'frConfig',
        function frFormNavDirective(config) {
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/form-nav.html',
                scope: {
                    steps: '=frFormNav',
                    active: '=',
                    activeStep: '=',
                    activeStepIndex: '=',
                    canSaveAsConcept: '='
                },
                require: '^frForm',
                link: function(scope, element, attrs, ctrl) {

                    function setVisibility () {
                        scope.showSaveDraft = scope.canSaveAsConcept || false;
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWSAVEDRAFT, scope.showSaveDraft);
                        
                        /** @type {Object} */
                        scope.buttonState = {
                            disabled: false
                        };

                        if (!scope.steps || scope.steps.length === 0) {
                            // no steps, entire form on the page at once. show save button below.
                            scope.showNext = false;
                            scope.showPrevious = false;
                            scope.showSave = true;

                            scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWNEXT, scope.showNext);
                            scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWPREVIOUS, scope.showPrevious);
                            scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWSAVE, scope.showSave);

                            scope.navigation = config.defaultEntities.form.navigationTexts;

                            // get out, we're done here
                            return;
                        }

                        // there are steps, decide whether to show next and or previous buttons.
                        var steps = _.cloneDeep(scope.steps || []);

                        // test if current step is outro (then hide every button)
                        var current = scope.activeStepIndex || 0;
                        var currentStep = ctrl.getStepByIndex(current);
                        scope.navigation = currentStep.navigationTexts;

                        // filter out steps that do not have prerequisites met
                        // navigation should not factor in steps that are hidden because of their prerequisites
                        steps = _.filter(steps, function (step) {
                            return step._prerequisitesMatch;
                        });

                        // filter out steps that are outro
                        steps = _.filter(steps, function (step) {
                            return step.type !== config.enums.stepTypes.OUTRO;
                        });

                        // calculate the firstStep lastStep and the current.
                        var lastIndex = (steps.length === 0) ? 0 : steps.length - 1;
                        var firstIndex = 0;
                        var notOutro = currentStep.type !== config.enums.stepTypes.OUTRO;
                        var notIntro = currentStep.type !== config.enums.stepTypes.INTRO;

                        // assign the proper state values
                        scope.showNext = notOutro && (current < lastIndex);
                        scope.showPrevious = notOutro && (current !== firstIndex);
                        scope.showSave = notOutro && (current === lastIndex);
                        scope.showSaveDraft = scope.canSaveAsConcept && notOutro && notIntro;

                        // log them to the debug window
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWNEXT, scope.showNext);
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWPREVIOUS, scope.showPrevious);
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWSAVE, scope.showSave);
                        scope.$emit(config.enums.events.DEBUG, config.enums.debug.SHOWSAVEDRAFT, scope.showSaveDraft);
                    }

                    function isNextStep(currentStep) {
                        var steps = _.cloneDeep(scope.steps);
                        // TODO: don't just take index
                        return !!steps[currentStep + 1];
                    }

                    function isPreviousStep(currentStep) {
                        var steps = _.cloneDeep(scope.steps);
                        // TODO: don't just take index
                        return !!steps[currentStep - 1];
                    }

                    scope.save = function save (form) {
                        scope.$emit(config.enums.events.SUBMIT, {});
                    };

                    scope.saveDraft = function saveDraft (form) {
                        scope.$emit(config.enums.events.SUBMIT, {
                            draft: true
                        });
                    };

                    scope.next = function next () {
                        if (isNextStep(scope.activeStepIndex)) {
                            // Make the new step active
                            scope.$emit(config.enums.events.NAVIGATE, {
                                stepId: ctrl.getStepByIndex(scope.activeStepIndex + 1).id,
                                direction: config.enums.navigation.NEXT
                            });
                        }
                    };

                    scope.previous = function previous () {
                        // go back
                        if (isPreviousStep(scope.activeStepIndex)) {
                            // Make the new step active
                            scope.$emit(config.enums.events.NAVIGATE, {
                                stepId: ctrl.getStepByIndex(scope.activeStepIndex - 1).id,
                                direction: config.enums.navigation.PREVIOUS
                            });
                        }
                    };

                    /** on navigation started */
                    scope.$on(config.enums.events.NAVIGATE_STARTED, function navigateStarted() {
                        // disable submit button
                        scope.buttonState.disabled =  true;
                    });

                    /** on navigation completed */
                    scope.$on(config.enums.events.NAVIGATE_COMPLETED, function navigateCompleted() {
                        // enable submit button
                        scope.buttonState.disabled =  false;
                    });

                    /** on submit started */
                    scope.$on(config.enums.events.SUBMIT_STARTED, function submitStarted() {
                        // disable submit button
                        scope.buttonState.disabled =  true;
                    });

                    /** on submit completed */
                    scope.$on(config.enums.events.SUBMIT_COMPLETED, function submitCompleted(event, isResolved) {
                        // enable submit button
                        scope.buttonState.disabled =  false;
                        // only check if there is a next step when the SUBMIT is resolved
                        // Stay on the current step when te submit promise is rejected
                        if (isResolved) {
                            scope.next();
                        }
                    });

                    /** on redraw navigation completed */
                    scope.$on(config.enums.events.NAVIGATION_REDRAW, function submitCompleted(event, isResolved) {
                        setVisibility();
                    });

                    scope.$watch('activeStepIndex', setVisibility);
                    scope.$watch('steps', setVisibility);
                }
            };
        }
    ]);
})(window.angular, window._);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormSectionViewer', [
        function frFormSectionViewerDirective(){
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/form-section-viewer.html',
                scope: {
                    section: '=frFormSectionViewer',
                    model: '='
                },
                controller: 'frFormSectionController',
            };
        }
    ]);
})(window.angular);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormSection', [
        function frFormSectionDirective(){
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/form-section.html',
                scope: {
                    section: '=frFormSection',
                    model: '='
                },
                controller: 'frFormSectionController',
            };
        }
    ]);
})(window.angular);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormStepViewer', [
        function frFormStepViewerDirective() {
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl: 'fr/modules/form/views/form-step-viewer.html',
                scope: {
                    step: '=frFormStepViewer',
                    model: '='
                }
            };
        }
    ]);
})(window.angular);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormStep', [
        function frFormStepDirective() {
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl: 'fr/modules/form/views/form-step.html',
                scope: {
                    step: '=frFormStep',
                    model: '='
                }
            };
        }
    ]);
})(window.angular);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormTabs', [
        'frConfig',
        function frFormNavDirective(config) {
            return {
                restrict: 'AE',
                templateUrl: 'fr/modules/form/views/form-tabs.html',
                scope: {
                    steps: '=frFormTabs',
                    active: '=',
                    activeIndex: '='
                },
                link: function(scope, element, attrs, ctrl) {

                    function initialize() {
                        scope.width = calcWidth(scope.steps);
                    }

                    function calcWidth(items) {
                        return 'calc((100% / ' + items.length + ') - ((49px * ' + (items.length - 1) + ') / ' + items.length + '))';
                    }

                    scope.navigateTo = function navigateTo (stepId) {
                        scope.$emit(config.enums.events.NAVIGATE, {
                            stepId: stepId,
                            direction: config.enums.navigation.DIRECT
                        });
                    };

                    scope.width = 0;

                    initialize();
                }
            };
        }
    ]);
})(window.angular);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').directive('frFormViewer', [
        '$q',
        'frConfig',
        'frPrerequisitesParser',
        function formDirective($q, config, prerequisitesParser) {
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl: 'fr/modules/form/views/form-viewer.html',
                scope: {
                    schema: '=',
                    debug: '='
                },
                controller: 'frFormController',
                link: function(scope, element, attrs, ctrl) {

                    scope.active = true;

                    /** @type {Object} */
                    scope.debugData = {
                        model: scope.model
                    };
                }
            };
        }
    ]);
})(window.angular);
;(function(ng, $, _) {
    'use strict';
    ng.module('formRenderer.form').directive('frForm', [
        '$q',
        'frConfig',
        'frFormConfig',
        'frPrerequisitesParser',
        'frUtils',
        function formDirective($q, config, formConfig, prerequisitesParser, utils) {
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl: 'fr/modules/form/views/form.html',
                scope: {
                    active: '=?',
                    activeStep: '=?',
                    schema: '=',
                    onSubmit: '=?',
                    onNavigate: '=?',
                    debug: '='
                },
                controller: 'frFormController',
                link: function(scope, element, attrs, ctrl) {

                    /**
                     * @function
                     * @name initilaize ---------------------------------------------
                     * @description - initialize form directive
                     */
                    function initialize() {
                        if(scope.active === undefined) {
                            scope.active = true;
                        }

                        if (!_.isEmpty(scope.frSchema.steps)) {
                            /** set a default step when active step was not defined */
                            if(scope.activeStep === undefined) {
                                scope.activeStep = _.first(scope.frSchema.steps).id;
                            }
                            scope.activeIndex = ctrl.getStepById(scope.activeStep)._stepIndex;
                        }

                        scope.formClass = formConfig.config.formClass ? formConfig.config.formClass : '';
                    }

                    /** @type {Object} */
                    scope.debugData = {
                        model: scope.model
                    };

                    /** @type {Int} */
                    scope.activeIndex = 0;

                    /**
                     * @function
                     * @name scrollToAndFocusFirstError ---------------------------------
                     * @description - Find and scroll to the first field that has an error
                     * @param {Object} element Form element
                     * @param {object} ctrl Form controller
                     * @param {Int} scrollAnimationTime Indicate how long the animation will take.
                     * @param {Int} scrollOffset offset for scroll to element
                     */
                    function scrollToAndFocusFirstError(element, ctrl, scrollAnimationTime, scrollOffset) {
                        var targetForm = element[0].querySelector('form');
                        var targetElement = $(targetForm)[0].querySelector('.ng-invalid');
                        if (targetElement) {
                            ctrl.scrollTo(targetElement, parseInt(scrollAnimationTime), scrollOffset);
                        }
                    }

                    /**
                     * @function
                     * @name onSubmit -------------------------------------------------------
                     * @description - handle onSubmit
                     * @param {Boolean} saveDraft Save as draft
                     */
                    function onSubmit(saveDraft) {
                        var modelPrerequisitesMatch = utils.deepGet(formConfig.config, 'modelOutput.onSubmit.PrerequisitesMatch');
                        var frSubmit = $q.defer(),
                            modelCopy = modelPrerequisitesMatch ? ctrl.getPrerequisitesMatchFormModel() : ctrl.getFormModel(),
                            isValidForm = _.cloneDeep(scope.theForm.$valid);

                        modelCopy.draft = saveDraft || false;

                        if (scope.onSubmit) {
                            scope.onSubmit(modelCopy, isValidForm, frSubmit);
                        } else {
                            // resolve promise when we can't find the onSubmit hook
                            isValidForm && frSubmit.resolve();
                            !isValidForm && frSubmit.reject();
                        }

                        frSubmit.promise.then(function success(data) {
                            // check if we have te re evaluate the form
                            if (data && data.reEvaluateSchema) {
                                ctrl.reEvaluateSchema(true);
                            }
                            // when the form isn't valid, still override it's navigation
                            var shouldResolve = !modelCopy.draft && isValidForm;
                            scope.$broadcast(config.enums.events.SUBMIT_COMPLETED, shouldResolve);
                        }, function error(err) {
                            scope.$broadcast(config.enums.events.SUBMIT_COMPLETED, false);
                            // scroll to the firts field with an error
                            if (formConfig.config.scrollToAndFocusFirstErrorOnSubmit) {
                                scrollToAndFocusFirstError(element, ctrl, formConfig.config.scrollAnimationTime, formConfig.config.scrollOffset);
                            }
                        });
                    }

                    /**
                     * @function
                     * @name navigateToNextStep ---------------------------------------
                     * @description - validate the form then check if the user has defined
                     * a onNavigate hook. We call this function when we can find one
                     * Wait till the promise 'frContinue' is resolved or rejected
                     * move to the next step when the form is valid
                     * @param  {Object} params - navigation parameters
                     */
                   function navigateToNextStep(params) {
                        // call form validation
                        ctrl.validateForm().finally(function whenCheckIsDone() {
                            // don't use an object -> spread all properties
                            // do some stuff even if the form is inValid
                            var isValidForm = _.cloneDeep(scope.theForm.$valid),
                                frContinue = $q.defer(),
                                activeStep = _.cloneDeep(scope.activeStep);
                            var stepModelPrerequisitesMatch = utils.deepGet(formConfig.config, 'modelOutput.onNavigate.PrerequisitesMatch');
                            var modelCopy = stepModelPrerequisitesMatch ? ctrl.getPrerequisitesMatchStepModel(activeStep) : ctrl.getFormModel();

                            frContinue.promise.then(
                                function navigateReady(data) {
                                    // check if we have te re evaluate the form
                                    if (data && data.reEvaluateSchema) {
                                        ctrl.reEvaluateSchema(true);
                                    }
                                    // move to the next step when the form is valid
                                    isValidForm && navigateToStep(params.stepId);
                                },
                                function notNavigateReady(err) {
                                    // scroll to the firts field with an error
                                    if (formConfig.config.scrollToAndFocusFirstErrorOnSubmit) {
                                        scrollToAndFocusFirstError(element, ctrl, formConfig.config.scrollAnimationTime, formConfig.config.scrollOffset);
                                    }
                                }
                            );
                            // Broadcast event when the frContinue promise was resolved or rejected
                            frContinue.promise.finally(function success() {
                                scope.$broadcast(config.enums.events.NAVIGATE_COMPLETED, params);
                            });
                            // check if the onNavigate hook is available for us
                            if (scope.onNavigate) {
                                scope.onNavigate(modelCopy, activeStep, isValidForm, frContinue);
                            } else {
                                // resolve promise when we can't find the onNavigate hook
                                isValidForm && frContinue.resolve();
                                !isValidForm && frContinue.reject();
                            }
                        });
                    }

                    /**
                     * @function
                     * @name  navigateToStep ---------------------------------------------
                     * @description - navigate to a step by stepId
                     * @param  {int|string} stepId - step id
                     */
                    function navigateToStep(stepId) {
                        console.log('stepId', stepId);
                        var step = ctrl.getStepById(stepId);
                        scope.activeIndex = step._stepIndex;
                        scope.activeStep = step.id;
                    }

                    scope.getStep = ctrl.getStepById;

                    /**
                     * @event - on Navigate
                     * description - this function will be executed when the user navigates
                     * to the previous or next step
                     * TODO: what with direct event?
                     */
                    scope.$on(config.enums.events.NAVIGATE, function (event, params) {
                        var direction = params.direction;
                        // broadcast navigate started
                        scope.$broadcast(config.enums.events.NAVIGATE_STARTED, params);
                        // check direction
                        if (direction === config.enums.navigation.NEXT) {
                            navigateToNextStep(params);
                        } else if (direction === config.enums.navigation.PREVIOUS) {
                            navigateToStep(params.stepId);
                        }
                    });

                    /**
                     * description - Update the activeIndex when the activeStep has changed
                     */
                    scope.$watch('activeStep', function (newValue, oldValue) {
                        if (newValue !== oldValue) {
                            scope.activeIndex = ctrl.getStepById(scope.activeStep)._stepIndex;
                        }
                    });

                    /**
                     * @event - on Submit
                     * description - the function will be executed when the user submits
                     * the form
                     * TODO: $broadcast submit started event
                     *       $broadcast submit completed event
                     */
                    scope.$on(config.enums.events.SUBMIT, function (event, params) {
                        scope.$broadcast(config.enums.events.SUBMIT_STARTED, params);
                        if (params.draft) {
                           // don't trigger validation
                           onSubmit(true);
                        } else {
                           ctrl.validateForm().finally(onSubmit);
                        }
                    });

                    /**
                     * @events - Debug
                     * description - Handles any debug value passed to him, and gives it to the debug panel directive
                     */
                    scope.$on(config.enums.events.DEBUG, function (event, param, value) {
                        scope.debugData[param] = value;
                    });

                    /**
                     * @events - Field Changed
                     * description - when a field changes, and validation ran, this event is raised.
                     * we use it to tell the prerequisites parser, to check this field's dependencies
                     * and activate or deactivate the proper fields, steps or sections accordingly
                    */
                    scope.$on(config.enums.events.FIELD_CHANGED, function (event, fieldName) {
                        var field = scope.frSchema._internalCache.fieldById[fieldName];

                        if (!field) {
                            // no field with that name in the cache...
                            throw new Error(config.enums.errors.FIELD_NOT_FOUND);
                        }

                        if (!field._prerequisitesDependencies) {
                            // skip this field, it has no dependencies, so nothing needs to be parsed
                            return;
                        }

                        // run through the prereqDependencies and test it's prerequitites
                        prerequisitesParser.testMultiple(field._prerequisitesDependencies);

                        // redraw navigation
                        scope.$broadcast(config.enums.events.NAVIGATION_REDRAW);
                    });

                    initialize();
                }
            };
        }
    ]);
})(window.angular, window.jQuery, window._);
;(function (ng) {
    'use strict';
    ng.module('formRenderer.form').filter('unsafe', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    });
})(window.angular);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').provider('frFieldConfig', [
        function frConfigProvider() {

            /**
             * @name  get ------------------------------------
             * @description this function will return an object
             * that will be used as a public api
             * @return {Object} Public api
             */
            this.$get = function(frUtils, $timeout) {

                var typeMap = {},
                    tempWrapperMap = {},
                    fieldLayoutMap = {},
                    defaultTempWrapperName = 'defaultWrapper',
                    defaultFieldLayoutName = 'defaultFieldLayout';

                /**
                 * @name Set type -----------------------------
                 * @param {Object|Array} options type object
                 */
                function setType(options) {
                    if (_.isArray(options)) {
                        var allTypes = [];
                        _.forEach(options, function(item) {
                            allTypes.push(setType(item));
                        });
                        return allTypes;
                    } else if (_.isPlainObject(options)) {

                        if (options.extend) {
                            extendFieldTypeOptions(options);
                            typeMap[options.name] = options;
                            return typeMap[options.name];
                        }

                        if (!checkOverwrite(options.name, typeMap, options, 'types')) {
                            typeMap[options.name] = options;
                            return typeMap[options.name];
                        }
                    } else {
                        throw 'You must provide an object or array for setType. You provide:' + JSON.stringify(arguments);
                    }
                }

                /**
                 * @function
                 * @name  setTemplateWrapper ---------------------------
                 * @description set a template wrapper
                 * @param {object|array} options - template wrapper options
                 */
                function setTempWrapper(options) {
                    // check if array
                    if (_.isArray(options)) {
                        return options.map(function(wrapperOptions) {
                            return setTempWrapper(wrapperOptions);
                        });
                    } else if (_.isPlainObject(options)) {
                        options.fieldTypes = getOptionsFieldTypes(options);
                        options.name = getOptionsWrapperName(options);
                        checkTempWrapperTypes(options);
                        tempWrapperMap[options.name] = options;
                        return options;
                    }
                }

                /**
                 * @function
                 * @name  setFieldLayout -------------------------------
                 * @description - set a fieldLayout
                 * @param {Object} options - fieldLayout options
                 */
                function setFieldLayout(options) {
                    if (_.isArray(options)) {
                        var allLayouts = [];
                        _.forEach(options, function (item) {
                            allLayouts.push(setFieldLayout(item));
                        });
                        return allLayouts;
                    } else if (_.isPlainObject(options)) {
                        if (!checkOverwrite(options.name, fieldLayoutMap, options, 'fieldLayout')) {
                            options.name = getOptionsFieldLayoutName(options);
                            fieldLayoutMap[options.name] = options;
                            return fieldLayoutMap[options.name];
                        }
                    } else {
                        throw 'You must provide an object or array for setFieldLayout. You provide:' + JSON.stringify(arguments);
                    }
                }

                /**
                 * @function
                 * @name getTempWrapper
                 * @description get tempWrapper object by name
                 * if no name was provided the default wrapper will be
                 * returned
                 * @param  {string} name - wrapper name
                 * @return {Object} - tempWrapper object
                 */
                function getTempWrapper(name) {
                    return tempWrapperMap[name || defaultTempWrapperName];
                }

                /**
                 * @function
                 * @name  getWrapperByFieldType -------------------------
                 * @description get all wrappers by type
                 * @param  {String} fieldType - field type name
                 * @return {array} - array of field wrappers
                 */
                function getTempWrapperByType(fieldType) {
                    var wrappers = [];
                    for (var name in tempWrapperMap) {
                        if (tempWrapperMap.hasOwnProperty(name)) {
                            // check if there are fieldTypes set
                            // if so check if there is an index that has this fieldType
                            if (tempWrapperMap[name].fieldTypes && tempWrapperMap[name].fieldTypes.indexOf(fieldType) !== -1) {
                                wrappers.push(tempWrapperMap[name]);
                            }
                        }
                    }
                    return wrappers;
                }

                /**
                 * @function
                 * @name getOptionsWrapperName ----------------------------
                 * @description get wrapper name (fallback defaultTempWrapperName)
                 * @param  {object|array} options - wrapper options
                 * @return {string} - tempWrapperName
                 */
                function getOptionsWrapperName(options) {
                    return options.name || options.fieldTypes.join(' ') || defaultTempWrapperName;
                }

                /**
                 * @function
                 * @name  getOptionsFieldLayoutName ------------------------
                 * @description - get field layout name (fallback defaultFieldLayoutName)
                 * @param  {object} options - fieldLayout options
                 * @return {String} - fieldLayoutName
                 */
                function getOptionsFieldLayoutName(options) {
                    return options.name || defaultFieldLayoutName;
                }

                 /**
                 * @name  Get type object ----------------------------------
                 * @description searcg for a typeMap by a given name
                 * and return it
                 * @return {object}
                 */
                function getType(name, throwError, errorContext) {
                    if (!name) {
                        return undefined;
                    }
                    var type = typeMap[name];
                    if (!type && throwError === true) {
                        throw 'There is no type by the name of' + name;
                    } else {
                        return type;
                    }
                }

                /**
                 * @function
                 * @name  getFieldLayout ------------------------------------
                 * @description - get field layout by name
                 * @param  {String} name - fieldLayout name
                 * @return {Object} - fieldLayout
                 */
                function getFieldLayout(name) {
                    return fieldLayoutMap[name || defaultFieldLayoutName];
                }

                /**
                 * @function
                 * @name  extendTypeOptions --------------------------------
                 * @param  {Object} options - type options
                 */
                function extendFieldTypeOptions(options) {
                    var extendType = getType(options.extend, true, options);
                    extendFieldTypeDefaultOptions(options, extendType);
                    extendFieldTypeController(options, extendType);
                    extendFieldTypeLink(options, extendType);
                    frUtils.reverseDeepMerge(options, extendType);
                }

                /**
                 * @function
                 * @name  extendFieldTypeController ------------------------
                 * @description check if we have to extend any controller function
                 * on the extend object.
                 * @param  {Object} options - field type options
                 * @param  {Object} extendType - extend field type options
                 */
                function extendFieldTypeController(options, extendType) {
                    var extendCtrl = extendType.controller;
                    // return when the extendType object has no controller
                    if (!ng.isDefined(extendCtrl)) {
                        return;
                    }
                    var optionsCtrl = options.controller;
                    // when we have a controller in the options object
                    // and on the extendObject -> we have to call both
                    // controllers
                    if (ng.isDefined(optionsCtrl)) {
                        options.controller = function($scope, $controller) {
                            $controller(extendCtrl, {$scope: $scope});
                            $controller(optionsCtrl, {$scope: $scope});
                        };
                        // inject scope and controller service
                        options.controller.$inject = ['$scope', '$controller'];
                    } else {
                        // if there is no controller on the options object
                        // take the on one from the extend object
                        options.controller = extendCtrl;
                    }
                }

                /**
                 * @function
                 * @name  extendFieldTypeLink -------------------------------
                 * @description check if we have to extend any link function
                 * on the extend object
                 */
                function extendFieldTypeLink(options, extendType) {
                    var extendLink = extendType.link;
                    // return when there is no extend function
                    // available on the extend type
                    if (!ng.isDefined(extendLink)) {
                        return;
                    }
                    var optionsLink = options.link;
                    // if there is a link function on the
                    // options object we have to overwrite
                    // the link function. call both link functions
                    if (ng.isDefined(optionsLink)) {
                        options.link = function() {
                            // call extendLink and optionsLink
                            // and spread the arguments
                            extendLink.apply(null, _toArray(arguments));
                            optionsLink.apply(null, _toArray(arguments));
                        };
                    } else {
                        // call original extendLink function
                        options.link = extendLink;
                    }
                }

                /**
                 * @function
                 * @name  _toArray ------------------------------------------
                 * @description convert to array
                 * @param  {array} arr
                 * @return {array}
                 */
                var _toArray = function (arr) {
                    return Array.isArray(arr) ? arr : [].slice.call(arr);
                };

                /**
                 * @function
                 * @name  extendFieldTypeDefaultOptions ---------------------
                 * @description - extend all default options from the base field Type
                 * check if the defaultOptions property on the extend type
                 * or the new field type is a function, if so we have to make sure
                 * the options propperty in the callback has all extended properties
                 * and that is should merge the two when we call (defaultOptions) on the
                 * field type that extends
                 * @param  {Object} options - Field type options
                 * @param  {Object} extendsType - extend field type options
                 * @return {Function} - It returns a function if options.defaultOptions or
                 * extendsType.defaultOptions is a function
                 */
                function extendFieldTypeDefaultOptions(options, extendsType) {
                    var extendDefaultOptions = extendsType.defaultOptions;
                    // if there are no default options defined
                    // do nothing
                    if (!ng.isDefined(extendDefaultOptions)) {
                        return;
                    }
                    var optionsDefaultOptions = options.defaultOptions;
                    var optionsDefaultOptionsIsFn = _.isFunction(optionsDefaultOptions);
                    var extendDefaultOptionsIsFn = _.isFunction(extendDefaultOptions);
                    // if the extend default options is a function
                    if (extendDefaultOptionsIsFn) {
                        // create new defaultOPtions function
                        options.defaultOptions = function defaultOptions(opts, scope) {
                            // get object from the baseType
                            var extendDO = extendDefaultOptions(opts, scope);
                            var mergedDO = {};
                            // merge all options + the default options on the
                            // extended object
                            frUtils.reverseDeepMerge(mergedDO, opts, extendDO);
                            // check if the defaultOptions on the new type is a function
                            var extenderOptionsDefaultOptions = optionsDefaultOptions;
                            if (optionsDefaultOptionsIsFn) {
                                // call the defaultOptions function from our new field type
                                extenderOptionsDefaultOptions = extenderOptionsDefaultOptions(mergedDO, scope);
                            }
                            // merge the field type default options on the base object with the
                            // default options from the new type
                            //frUtils.extendDeep(extendDO, extenderOptionsDefaultOptions);
                            frUtils.reverseDeepMerge(extendDO, extenderOptionsDefaultOptions);
                            return extendDO;
                        };
                    } else if (optionsDefaultOptionsIsFn) {
                        // all properties on the base field type should be available
                        // on the new field
                        options.defaultOptions = function defaultOptions(opts, scope) {
                            var newDefaultOptions = {};
                            //frUtils.extendDeep(newDefaultOptions, opts, extendDefaultOptions);
                            frUtils.reverseDeepMerge(newDefaultOptions, opts, extendDefaultOptions);
                            return optionsDefaultOptions(newDefaultOptions, scope);
                        };
                    }
                }

                /**
                 * @function
                 * @name  getOptionsFieldTypes -------------------------
                 * @description - set the fieltype param in the right format
                 * and return it
                 * @param  {string|array} options - wrapper options
                 * @return {Array}
                 */
                function getOptionsFieldTypes(options) {
                    // if string return array
                    if (_.isString(options.fieldTypes)) {
                        return [options.fieldTypes];
                    }
                    // if not defined return empty array else
                    // return field types
                    if (!ng.isDefined(options.fieldTypes)) {
                        return [];
                    } else {
                        return options.fieldTypes;
                    }
                }

                /**
                 * @function
                 * @name  checkTempWrapperTypes
                 * @description - check if the wrapper types have
                 * the correct format
                 * @param  {object|array} options - wrapper options
                 */
                function checkTempWrapperTypes(options) {
                    var error = !_.isArray(options.fieldTypes);
                    _.forEach(options.fieldTypes, function(fieldType) {
                        if (!error) {
                            if (!_.isString(fieldType)) {
                                error = true;
                            }
                        }
                    });
                    if (error) {
                        throw 'trying to create a template wrapper with fieldTypes that is not an array of strings';
                    }
                }

                /**
                 * @name check overwrite -----------------------
                 * @description check if a given object has already a property
                 * defined so we don't overwrite it!
                 * @param  {String} property : property to check
                 * @param  {Object} object
                 * @param  {Object} newValue
                 * @return {Boolean}
                 */
                function checkOverwrite(property, object, newValue, type) {
                    if (object && object.hasOwnProperty(property)) {
                        console.error([
                            'overwriting-' + type + ': Attempting to overwriting ' + property + ' on ' + type + ' wich in currently ',
                            JSON.stringify(object[property]) + ' with ' + JSON.stringify(newValue)
                        ].join(''));
                        return true;
                    }
                    return false;
                }

                return {
                    getType: getType,
                    setType: setType,
                    setTempWrapper: setTempWrapper,
                    getTempWrapper: getTempWrapper,
                    getTempWrapperByType: getTempWrapperByType,
                    setFieldLayout: setFieldLayout,
                    getFieldLayout: getFieldLayout
                };
            };
        }
    ]);
})(window.angular, window._);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').provider('frFormConfig', [
        function frFormConfigProvider() {

            var config = {
                scrollToAndFocusFirstErrorOnSubmit: true,
                scrollAnimationTime: 200,
                scrollOffset: -100,
                formClass: '',

                modelOutput: {
                    onSubmit: {
                       PrerequisitesMatch: false,
                    },
                    onNavigate: {
                        PrerequisitesMatch: false,
                    }
                }
            };

            /**
             * @function
             * @name extendConfig ------------------------------
             * @description - extend newConfig with default config
             * @param {Object} newConfig Config object
             */
            function extendConfig(newConfig) {
                config = _.assign(config, newConfig);
            }

            return {
               $get: function() {
                   return {
                        extendConfig: extendConfig,
                        config: config
                   };
               }
            };
        }
    ]);
})(window.angular, window._);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').service('frSchemaParser', [
        'frFieldConfig',
        'frUtils',
        'frConfig',
        function frParseSchema(fieldConfig, utils, config) {
            // Default types -----------------------------------

            var defaultFormSchema = config.defaultEntities.form;
            var defaultStepSchema = config.defaultEntities.step;
            var defaultSectionSchema = config.defaultEntities.section;
            var defaultFieldSchema = config.defaultEntities.field;

            // this should not be using the config.js entity names,
            // yes these are constants, but if someone renames an entity in the config
            // it will look for other properties than the 4 below, in the json Schema
            // this will not work as the schema only uses these specs.
            var entityNames = {
                FIELD: 'field',
                STEP: 'step',
                SECTION: 'section',
                FORM: 'form'
            };

            /**
             * @name resetCache
             * @private
             * @description reset the internal cache before every schema parse
             * @param  {Object} schema : the schema to recieve cache
             * @return {Object} schema : the spiked schema
             */
            function resetCache(schema) {
                schema._internalCache = {};

                schema._internalCache[entityNames.FIELD] = [];
                schema._internalCache[entityNames.FIELD + 'ById'] = {};
                schema._internalCache[entityNames.STEP] = [];
                schema._internalCache[entityNames.STEP + 'ById'] = {};
                schema._internalCache[entityNames.SECTION] = [];
                schema._internalCache[entityNames.SECTION + 'ById'] = {};

                return schema;
            }

            /**
             * @name  parseSchema -------------------------
             * @description parse the given json object and extend it to include all necessary properties
             * @param  {String} formSchema : formSchema to parse
             * @return {Object} the parsed schema;
             */
            function parseSchema(formSchema) {
                resetCache(formSchema);
                var cache = formSchema._internalCache;

                // extend form
                var schema = utils.extendDeep(_.cloneDeep(defaultFormSchema), formSchema);
                // Fix Array property type when it was not given as an array
                schema = parseArrayProperties(entityNames.FORM, schema);
                // extend steps
                schema.steps = extendSubs(entityNames.STEP, defaultStepSchema, schema.steps, cache);
                // extend sections
                schema.sections = extendSubs(entityNames.SECTION, defaultSectionSchema, schema.sections, cache);
                // extend fields
                schema.fields = extendSubs(entityNames.FIELD, defaultFieldSchema, schema.fields, cache);

                // parse editmode, editable and showEditButton states
                schema = parseStates(schema);

                parseFieldValueOptions(cache);

                schema.steps = _.map(schema.steps,
                    _.curry(setDefaultButtonTexts)(schema.navigationTexts));

                return schema;
            }

            /**
             * @function
             * @name parseFieldValueOptions ------------------
             * @description - Convert each key property inside the valueOptions to a string
             * NOTE!: This is not something we want to do inside the formRenderer.
             *        It is not possible to use integers as a value for the key property inside the valueOptions Array.
             *        Check this example for more information : http://jsfiddle.net/vft3L1tb/29/
             * @param  {Object} cache
             */
            function parseFieldValueOptions(cache) {
                _.forEach(cache.field, function (field) {
                    if (_.isArray(field.spec.options.valueOptions)) {
                        field.spec.options.valueOptions = _.map(field.spec.options.valueOptions, function (option) {
                            option.key = _(option.key).toString();
                            return option;
                        });
                    }
                });
            }

            /**
             * @function
             * @name  parseStates ----------------------------
             * @description - parse all state objects
             * @param  {Object} schema - form schema
             * @return {Object} - parsed schema
             */
            function parseStates(schema) {
                // check steps
                checkEditableMode(entityNames.STEP, schema.steps);
                // check sections
                checkEditableMode(entityNames.SECTION, schema.sections);
                // check fields
                checkEditableMode(entityNames.FIELD, schema.fields);

                return schema;
            }

           /**
            * @function
            * @name  setDefaultButtonTexts -----------------------
            * @description - check and set the correct default button texts if not given
            * @param  {Object} step  - step to which we set button texts
            // */
            function setDefaultButtonTexts(defaults, step) {
                // save given texts over defaults
                step.navigationTexts = _.extend(_.clone(defaults), step.navigationTexts);
                return step;
            }

           /**
            * @function
            * @name  checkEditableMode -----------------------
            * @description - check if all sections and fields have a valid editmode
            * and editable state. If not, make sure the states are valid before we give
            * the form schema to the frForm directive
            * @param  {String} entityType   - type : step, section and field
            * @param  {array} items         - steps, sections, fields
            * @param  {Object} parentState  - parent state object
            */
            function checkEditableMode(entityType, items, parentState) {
                _.forEach(items, function(item, index) {

                    // handle defaults: when a step is passed into this method, parentState is undefined
                    parentState = parentState || {
                        editable: true,
                        editMode: true,
                        showEditButton: false
                    };

                    // set state
                    item.state.editable = (parentState.editable) ? item.state.editable : false;
                    item.state.editMode = (parentState.editMode && item.state.editable) ? item.state.editMode : false;
                    item.state.showEditButton = (item.state.editable && !item.state.editMode && !parentState.showEditButton);
                    // if item has children (any type), process them too
                    item.sections && checkEditableMode(entityNames.SECTION, item.sections, item.state);
                    item.fields && checkEditableMode(entityNames.FIELD, item.fields, item.state);
                    if (item.spec && item.spec.options && item.spec.options.fields) {
                        // fields in fields should use the same parent state as the parentField.
                        checkEditableMode(entityNames.FIELD, item.spec.options.fields, parentState);
                    }
                });
            }

            /**
             * @name  extendSubs -------------------------
             * @private
             * @description recursivly run over this step, extend it on the default object and check it's children if they need to be parsed.
             * @param  {String} entityType : The entity's type in order do something specific with a certain type.
             * @param  {String} defaultSchema : default Object to be extended by the incomming items.
             * @param  {Array} items : items to be looped over, each item will extended by a defaultSchema, and checked if it has children that need extension.
             * @param  {Object} cache : the easy access cache readily available on the form for easy access to steps sections and fields
             * @return {Array} items : the extended array list.
             */
            function extendSubs(entityType, defaultSchema, items, cache) {
                for (var i = 0; i < items.length; i += 1) {
                    var item = utils.extendDeep(_.cloneDeep(defaultSchema), items[i]);

                    // Fix Array property type when it was not given as an array
                    item = parseArrayProperties(entityType, item);

                    item = parsePrerequisites(item);

                    if (entityType === entityNames.FIELD) {
                        setFieldLayout(item);
                    }

                    if (entityType === entityNames.STEP && item.sections) {
                        item.sections = extendSubs(entityNames.SECTION, defaultSectionSchema, item.sections, cache);
                    }

                    if (item.fields) {
                        item.fields = extendSubs(entityNames.FIELD, defaultFieldSchema, item.fields, cache);
                        setFieldLayout(item.fields);
                    }

                    if (entityType === entityNames.FIELD && item.spec && item.spec.options && item.spec.options.fields) {
                        item.spec.options.fields = extendSubs(entityNames.FIELD, defaultFieldSchema, item.spec.options.fields, cache);
                        setFieldLayout(item.spec.options.fields);
                    }

                    cache[entityType].push(item);
                    // use only item name when entity type is a field
                    if (entityType !== entityNames.FIELD) {
                        cache[entityType + 'ById'][item.id] = item;
                    } else {
                        cache[entityType + 'ById'][item.name] = item;
                    }

                    items[i] = item;
                }
                return items;
            }

            /**
             * @function
             * @name  setFieldLayout ----------------------------
             * @description - get the fieldLayout config when there is one set.
             * concat the fieldClass properties.
             * @param {Array|object} field - field object or fields array
             */
            function setFieldLayout(field) {
                if (_.isArray(field)) {
                    _.forEach(field, function (item) {
                        setFieldLayout(item);
                    });
                } else {
                    field.spec.options.layout = field.spec.options.layout || {
                        fieldClass: ''
                    };
                    var confFieldLayout,
                        layout = field.spec.options.layout;

                    confFieldLayout = fieldConfig.getFieldLayout(layout.fieldLayout);

                    // if we can find the field layout in the config
                    // concat fieldClass string with the configFieldClass
                    if (confFieldLayout && _.isString(confFieldLayout.fieldClass)) {
                        layout.fieldClass = layout.fieldClass + ' ' + confFieldLayout.fieldClass;
                    }
                }
            }

            function parsePrerequisites(item) {
                if (item.prerequisites === null || item.prerequisites === undefined) {
                    item.prerequisites = {};
                }
                return item;
            }

            /**
             * @function
             * @name  parseArrayProperties ----------------------------
             * @description - Check array properties for their type, if not an array wrap them in one.
             * @param  {String} entityType : The entity's type in order do something specific with a certain type.
             * @param  {Object} entity : the entity in which properties need to be checked for proper array type.
             */
            function parseArrayProperties(entityType, entity) {
                // TODO: fix arrays
                var propertyMap = config.defaultArrayPropertiesMap[entityType];

                if (!propertyMap) {
                    return entity;
                }

                _.forEach(propertyMap, function checkArrayProperties(propertyPath) {
                    // find path in entity
                    if (utils.deepIn(entity, propertyPath) && !Array.isArray(utils.deepGet(entity, propertyPath))) {
                        utils.deepSet(entity, propertyPath, [utils.deepGet(entity, propertyPath)]);
                    }
                });

                return entity;
            }

            return {
                parse: parseSchema
            };
        }
    ]);
})(window.angular, window._);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.form').service('frSchemaValidator', [
        'frUtils',
        'frConfig',
        function frParseSchema(utils, config) {

            /**
             * @name  validateSchema -------------------------
             * @description validate the given json object
             * @param  {String} formSchema : formSchema to validate
             * @return {Array}
             */
            function validateSchema(formSchema) {
                // TODO: not implemented, later this could validate the given object against a schema, and return errors to the user.
                var errorsAndWarnings = [];

                // if there are errors, push em into the errorsAndWarnings obj
                // dummy example
                if (formSchema.formId === undefined) {
                    throw new Error('INVALID SCHEMA: missing [form.formId]');
                }

                // and throw em to the top
                if (errorsAndWarnings.length > 0) {
                    var errors = errorsAndWarnings.join('\n');
                    throw new Error('INVALID SCHEMA: ' + errors);
                }

                return formSchema;
            }

            return {
                validate: validateSchema
            };
        }
    ]);
})(window.angular);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').service('frModelParser', [
        'frUtils',
        'frFieldConfig',
        'frConfig',
        function frParseModel(utils, frFieldConfig, frConfig) {

            /**
             * @function
             * @name createModel -----------------------------------
             * @description generate a model object that we pass to all
             * fields in the form
             * @param  {Object} formSchema
             * @return {Object} Model
             */
            function createModel(formSchema) {
                formSchema._internalCache = formSchema._internalCache || {};
                var model = formSchema._internalCache.formModel = {};

                if (ng.isDefined(formSchema.steps)) {
                    // go trough all steps and find fields or sections
                    _.forEach(formSchema.steps, function(step) {
                        var stepsModel = createModel(step);
                        for (var index in stepsModel) {
                            model[index] = stepsModel[index];
                        }
                    });
                }

                if (ng.isDefined(formSchema.sections)) {
                    _.forEach(formSchema.sections, function(section) {
                       var sectionModel = createModel(section);
                        for (var index in sectionModel) {
                            model[index] = sectionModel[index];
                        }
                    });
                }

                if (ng.isDefined(formSchema.fields)) {
                    _.forEach(formSchema.fields, function(field) {
                        var subFields = field.spec.options.fields,
                            fieldType = field.spec.attributes.type,
                            fieldOptions = field.spec.options;

                        // check if there are nested fields available
                        if (ng.isDefined(subFields)) {
                            var fieldConfig = frFieldConfig.getType(fieldType);
                            // modelType is required to render a correct model
                            if (!fieldConfig.modelType) {
                                throw 'You must provide a known modelType for field type: ' + fieldType;
                            }
                            switch(fieldConfig.modelType) {
                                case frConfig.fieldConfig.modelTypes.OBJECT:
                                    // create an object and assign it to the model
                                    model[field.name] = createModel(fieldOptions);
                                    break;
                                case frConfig.fieldConfig.modelTypes.ARRAY:
                                    // create an array and assign it to the model
                                    // so we can create repeatable field groups
                                    var value = field.spec.attributes.value;
                                    model[field.name] = [];
                                    if (_.isArray(value) && !_.isEmpty(value)) {
                                        model[field.name] = value;
                                    } else {
                                        model[field.name].push(createModel(fieldOptions));
                                    }
                                    break;
                                default:
                                    // throw an error when someone is trying to set a modelType we don't know about!
                                    var error = [
                                        'You must provide a known modelType for field type: ' + fieldType + '.\n',
                                        ' Known system types: ' + JSON.stringify(frConfig.fieldConfig.modelTypes) + '\n',
                                        ' You provide: ' + JSON.stringify(fieldConfig.modelType)
                                    ].join('');
                                    throw error;
                            }
                        } else {
                            model[field.name] = field.spec.attributes.value;
                        }
                    });
                }
                return model;
            }

            return {
                createModel: createModel
            };
        }
    ]);
})(window.angular, window._);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.form').service('frPrerequisitesParser', [
        'frUtils',
        'frConfig',
        function frParseSchema(utils, config) {
            // Preprocessare Prerequisites -----------------------------------

            // this should not be using the config.js entity names,
            // yes these are constants, but if someone renames an entity in the config
            // it will look for other properties than the 4 below, in the json Schema
            // this will not work as the schema only uses these specs.
            var entityNames = {
                FIELD: 'field',
                STEP: 'step',
                SECTION: 'section',
                FORM: 'form'
            };

            var _cache;

            /**
             * @name  parseSchema -------------------------
             * @description parse the given json object and extend it to include all necessary properties
             * @param  {String} schema : schema to parse
             * @return {Object} the parsed schema;
             */
            function parseSchema(schema) {
                _processEntities(schema);

                return schema;
            }

            /**
             * @name  _processEntities -------------------------
             * @private
             * @description itterate over all entities, and build their dependency cache
             * @param  {Object} schema : the formSchema to be parsed
             * @return {Object} item : the extended item.
             */
            function _processEntities(schema) {
                _cache = schema._internalCache;

                _.forEach([
                    entityNames.STEP,
                    entityNames.SECTION,
                    entityNames.FIELD
                ], function (entityType) {
                    for (var i = 0; i < _cache[entityType].length; i += 1) {
                        _prepEntity(_cache[entityType][i], _cache);
                    }
                });

                _.forEach([
                    entityNames.STEP,
                    entityNames.SECTION,
                    entityNames.FIELD
                ], function (entityType) {
                    testMultipleEntities(_cache[entityType]);
                });

                return schema;
            }

            /**
             * @name  _prepEntity -------------------------
             * @private
             * @description process an entity's prerequisites and add them to this item's dependency cache.
             * @param  {Object} item : the item to be parsed
             * @return {Object} item : the extended item.
             */
            function _prepEntity(item) {
                if (item.prerequisites) {
                    var prerequisites = item.prerequisites;

                    _.forEach([
                        entityNames.STEP,
                        entityNames.SECTION,
                        entityNames.FIELD
                    ], function (entityType) {
                        if (prerequisites[entityType + 'sCompleted'] && prerequisites[entityType + 'sCompleted'][entityType + 's']) {
                            _.forEach(prerequisites[entityType + 'sCompleted'][entityType + 's'], function (subjectId) {
                                var subject = _cache[entityType + 'ById'][subjectId];
                                // TODO throw error when subject is undefined
                                subject._prerequisitesDependencies = subject._prerequisitesDependencies || [];
                                subject._prerequisitesDependencies.push(item);
                            });
                        }
                    });

                    // cached for all completed prerequisites, now do it for the field values too...
                    if (prerequisites.fieldValues && prerequisites.fieldValues.operands) {
                        _.forEach(prerequisites.fieldValues.operands, function (operand) {
                            var lastProperyInPath = _.last(utils.getProperties(operand.name));
                            var subject = _cache[entityNames.FIELD + 'ById'][lastProperyInPath];
                            // TODO throw error when subject is undefined
                            subject._prerequisitesDependencies = subject._prerequisitesDependencies || [];
                            subject._prerequisitesDependencies.push(item);
                        });
                    }
                }
                return item;
            }

            function testMultipleEntities(entities) {
                _.forEach(entities, testEntity);
                indexSteps();
            }

            function testEntity(entity, loopIndex) {
                // take the entity's prerequisites
                var prereq = entity.prerequisites;
                var valid = true;


                if (!_.isEmpty(prereq.fieldValues)) {
                    valid = (handleFieldValues(prereq.fieldValues)) ? valid : false;
                }
                if (!_.isEmpty(prereq.stepsCompleted)) {
                    valid = (handleStepsComplete(prereq.stepsCompleted)) ? valid : false;
                    // console.log('handleStepsComplete', valid);
                }
                if (!_.isEmpty(prereq.sectionsCompleted)) {
                    valid = (handleSectionsComplete(prereq.sectionsCompleted)) ? valid : false;
                    // console.log('handleSectionsComplete', valid);
                }
                if (!_.isEmpty(prereq.fieldsCompleted)) {
                    valid = (handleFieldsCompleted(prereq.fieldsCompleted)) ? valid : false;
                    // console.log('handleFieldsComplete', valid);
                }

                entity._prerequisitesMatch = valid;

                // if this method is called from a loop of more entities being tested,
                // skip the reindex (it will be done there)
                if (!loopIndex) {
                    indexSteps();
                }
            }

            function indexSteps() {
                var index = 0;
                _.forEach(_cache.step, function (step, i) {
                    step._originalStepIndex = i;
                    if (step._prerequisitesMatch) {
                        step._stepIndex = index;
                        index += 1;
                    } else {
                        step._stepIndex = undefined;
                    }
                });
            }

            function getField(fieldName) {
                return _cache.fieldById[fieldName];
            }

            function handleFieldValues(fieldsReq) {
                var fieldsToCheck = fieldsReq.operands;
                var shouldTestLogical = fieldsToCheck.length > 1;

                var checkField = function checkField(fieldReq) {
                    var field = utils.deepGet(_cache.formModel, fieldReq.name);
                    return testOperator(fieldReq.operator, field, fieldReq.value);
                };

                if (shouldTestLogical) {
                    var map = _.map(fieldsToCheck, function (field) {
                        return checkField(field);
                    });

                    if (fieldsReq.logical === 'AND') {
                        var notAllTrue = _.contains(map, false);
                        return !notAllTrue;
                    } else {
                        var hasATrue = _.contains(map, true);
                        return hasATrue;
                    }
                } else {
                    return checkField(fieldsToCheck[0]);
                }
            }

            function handleStepsComplete(stepsReq) {
                // TODO: implement handleStepsComplete test
                return true;
            }

            function handleSectionsComplete(sectionsReq) {
                // TODO: implement handleSectionsComplete test
                return true;
            }

            function handleFieldsCompleted(fieldsReq) {
                // TODO: implement handleFieldsCompleted test
                return true;
            }

            function testOperator (operator, fieldValue, value) {
                var fv = _.cloneDeep(fieldValue);
                fv = _.contains([null, undefined], fv) ? "" : fv;
                return operators[operator](fv, value);
            }

            var operators = {
                '==': function equals(value, test) {
                    return String(value) === String(test);
                },
                '!=': function notEquals(value, test) {
                    return String(value) !== String(test);
                },
                '<': function lowerThan(value, test) {
                    return value < test;
                },
                '>': function higherThan(value, test) {
                    return value > test;
                },
                '<=': function lowerThanOrEquals(value, test) {
                    return value <= test;
                },
                '>=': function higherThanOrEquals(value, test) {
                    return value >= test;
                },
                'in': function contains(value, test) {
                    return _.contains(value, test);
                },
                '!in': function contains(value, test) {
                    return !_.contains(value, test);
                }
            };

            return {
                parse: parseSchema,
                testMultiple: testMultipleEntities,
                test: testEntity,
                indexSteps: indexSteps
            };
        }
    ]);
})(window.angular, window._);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.utils').service('frUtils', [
        function frParseSchema() {

            /**
             * @function
             * @name  extenDeep --------------------------------------------
             * @description - the destination object will extend all properties
             * from the objects given in the arguments
             * @param  {Object} dst - destination object
             * @arguments
             * @return {Object}
             */
            function extendDeep(dest) {
                _.forEach(arguments, function(obj, index) {
                    // the first time we go trough this loop
                    // obj and dest will be the same so we don't need to go further
                    if (!index) {
                        return;
                    }
                    // console.log('extending with:', obj);
                    _.forEach(obj, function(value, key) {
                        // if we deal with an object -> call deep extend
                        if (objAndSameType(dest[key], value)) {
                            extendDeep(dest[key], value);
                        } else {
                            dest[key] = value;
                        }
                    });
                });
                return dest;
            }

            /**
             * @function
             * @name reverseDeepMerge ------------------------------------------
             * @description - only copy properties formm the src object
             * when the properties are not already defined in the destination object
             * @param  {Objet} dest - destination object
             * @return {Object}
             */
            function reverseDeepMerge(dest) {
                _.forEach(arguments, function(src, index) {
                    // the first time we go trough this loop
                    // obj and dest will be the same so we don't need to go further
                    if (!index) {
                        return;
                    }
                    _.forEach(src, function(val, prop) {
                        //Determines if a reference is defined.
                        // if not copy the src to the dest
                        // else check if we have an object from the same type
                        // call the reverse deep merge again!
                        if (!ng.isDefined(dest[prop])) {
                            dest[prop] = _.cloneDeep(val);
                        } else if (objAndSameType(dest[prop], val)) {
                            reverseDeepMerge(dest[prop], val);
                        }
                    });
                });
              return dest;
            }

            /**
             * @function
             * @name  objAndSameType ------------------------------
             * @description - check if two object are plain object and share
             * the same type
             * @param  {Object} obj1 - js Object
             * @param  {Object} obj2 - js Object
             * @return {Boolean}
             */
            function objAndSameType(obj1, obj2) {
                return _.isPlainObject(obj1) && _.isPlainObject(obj2) &&
                Object.getPrototypeOf(obj1) === Object.getPrototypeOf(obj2);
            }

            /**
             * @function
             * @name  arrayify -----------------------------------
             * @description create array from object
             * if no object is given return empty array
             * @param  {Object|String|Int} obj
             * @return {Array}
             */
            function arrayify(obj) {
                if (obj && !_.isArray(obj)) {
                    // if not array create one
                    obj = [obj];
                } else if (!obj) {
                    // if no object is given
                    // set empty array
                    obj = [];
                }
                return obj;
            }

            /**
             * @function
             * @name  deepIn -----------------------------------
             * @description - check if we can find the propertyPath in a collection
             * @param  {Object|Array} collection - object where we must find the given propertyPath
             * @param  {String} propertyPath - a string that describes a property path
             * @return {Boolean} - true if the path was found
             */
            function deepIn(collection, propertyPath) {
                var properties = getProperties(propertyPath);
                for(var i = 0; i < properties.length; i = i + 1){
                    var property = properties[i];
                    if(_.has(collection, property) ||
                        _.isObject(collection) && property in collection){
                        collection = collection[property];
                    }
                    else{
                        return false;
                    }
                }
                return true;
            }

            /**
             * @function
             * @name  deepGet ----------------------------------
             * @description - get The value from a property in a collection
             * @param  {Object|Array} collection
             * @param  {String} propertyPath - a string that describes a property path
             * @return {String|Int|object|Array} - return the value
             */
            function deepGet(collection, propertyPath) {
                var properties = getProperties(propertyPath);
                if (deepIn(collection, propertyPath)) {
                    return _.reduce(properties, function(object, property){
                        return object[property];
                    }, collection);
                }
            }

            /**
             * @function
             * @name  deepSet -----------------------------------
             * @description - Sets a value of a property in an object tree. Any missing objects/arrays will be created
             * @param {Object|Array} collection - The root object/array of the tree.
             * @param {string|Array} propertyPath - The propertyPath.
             * @param {*} value - The value to set.
             * @returns {Object} The object.
             */
            function deepSet(collection, propertyPath, value){
                var properties = getProperties(propertyPath);
                var currentObject = collection;
                _.forEach(properties, function(property, index){
                    if(index + 1 === properties.length){
                        currentObject[property] = value;
                    }
                    else if(!_.isObject(currentObject[property])){
                        currentObject[property] = isValidArrayKey(properties[index + 1]) ? [] : {};
                    }
                    currentObject = currentObject[property];
                });

                return collection;
            }

            /**
             * @function
             * @name getProperties ------------------------------
             * @description - get all properties from a property path
             * Look @ the parseStringPropertyPath for more information
             * @param  {String} propertyPath - a string that describes a property path
             * @return {Array} - It returns an array of strings
             */
            function getProperties(propertyPath) {
                if (_.isArray(propertyPath)) {
                    return propertyPath;
                }

                if (!_.isString(propertyPath)) {
                    return [];
                }

                return parseStringPropertyPath(propertyPath);
            }

            /**
             * @function
             * @name  parseStringPropertyPath --------------------
             * @description - parse a string that describes a property path.
             * @param  {String} propertyPath - a string that describes a property path
             * @return {array} - It returns an array of strings
             *
             * @example
             * object.property -> [ 'object', 'property'],
             * array[0].property -> [ 'array', '0', 'property']
             */
            function parseStringPropertyPath(propertyPath) {
                var parsedPropertyPath = [],
                    parsedPropertyPathPart = '',
                    escapeNextCharacter = false,
                    isSpecialCharacter = false,
                    insideBrackets = false;

                _.forEach(propertyPath, function forEachCharacter(character, index) {
                    isSpecialCharacter = ( (character === '\\') || (character === '[') || (character === ']') || (character === '.') );

                    if (isSpecialCharacter && !escapeNextCharacter) {
                        // don't allow a special characters inside brackets!
                        if (insideBrackets && character !== ']') {
                            throw new SyntaxError('unexpected ' + character + ' within brackets at character' + index + ' in property path' + propertyPath);
                        }

                        switch(character) {
                        case '\\':
                            escapeNextCharacter = true;
                        break;
                        case ']':
                            insideBrackets = false;
                            break;
                        case '[':
                            insideBrackets = true;
                            // push parsedPropertyPathPart when we find [ ore .
                            /* falls through */
                        case '.':
                            parsedPropertyPath.push(parsedPropertyPathPart);
                            // clear parsedPropertyPart after push
                            parsedPropertyPathPart = '';
                        break;
                        }
                    } else {
                        parsedPropertyPathPart = parsedPropertyPathPart + character;
                        escapeNextCharacter = false;
                    }
                });

                // The first index could be an empty string when the first character is [ or .
                // Remove the first index because we can't do anything with an empty string!
                // example: [0] or .0
                if (parsedPropertyPath[0] === '') {
                    parsedPropertyPath.splice(0, 1);
                }

                // push the final parsedPropertyPathPart
                parsedPropertyPath.push(parsedPropertyPathPart);
                return parsedPropertyPath;
            }

            /**
             * @function
             * @name  isValidArryKey ---------------------------------
             * @description - Checks whether key is a valid array key
             * @param key
             * @returns {boolean}
             */
            function isValidArrayKey(key){
                var array = [];
                array[key] = null;
                return array.length > 0;
            }

            /**
             * @return {Object} - public api
             */
            return {
                extendDeep: extendDeep,
                reverseDeepMerge: reverseDeepMerge,
                objAndSameType: objAndSameType,
                getProperties: getProperties,
                deepGet: deepGet,
                deepSet: deepSet,
                deepIn: deepIn,
                arrayify: arrayify,
                isValidArrayKey: isValidArrayKey
            };
        }
    ]);
})(window.angular, window._);
;(function(ng) {
   'use strict';
   ng.module('formRenderer.validation').directive('frMessages', [
        function frMessagesDirective() {
            return {
                restrict: 'AE',
                scope: {
                    messages: '=frMessages'
                },
                templateUrl: 'fr/modules/validation/views/messages.html'
            };
        }
    ]);
})(window.angular);;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.validation').directive('frValidation', [
        '$q',
        '$timeout',
        'frValidationConfig',
        '$compile',
        '$parse',
        'frConfig',
        function frValidationDirective($q, $timeout, frValidationConfig, $compile, $parse, config) {

            var defaultRequiredValidator = config.defaultRequiredValidator;

            /**
             * @name inValidFunc --------------------------------------------------
             * @function
             * @description call when the field is invalid
             * @param  {Object} element
             * @param  {Object} scope
             * @param  {Object} ctrl
             */
            var inValidFunc = function validFunc(element, scope, ctrl) {
                ctrl.$setValidity(ctrl.$name, false);
                if (scope.invalidCallback) {
                    scope.invalidCallback(scope, {
                        message: scope.message
                    });
                }
            };

            /**
             * @name  validFunc ---------------------------------------------------
             * @function
             * @description call when the field is valid
             * @param  {Object} element
             * @param  {Object} scope
             * @param  {Object} ctrl
             */
            var validFunc = function validFunc(element, scope, ctrl) {
                ctrl.$setValidity(ctrl.$name, true);
                if (scope.validCallback) {
                    scope.validCallback(scope);
                }
            };

            /**
             * Check validators ---------------------------------------------------
             * @function
             * @param  {Object} scope
             * @param  {Object} element
             * @param  {Object} attrs
             * @param  {Object} ctrl
             * @param  {Object} validators
             * @param  {String} value
             * @return {Promise}
             */
            var checkValidators = function checkValidators(scope, element, attrs, ctrl, validation, value) {
                var validatorsConfig = [],
                    promises = [],
                    isValid = true,
                    defaultRequiredValidatorCP = _.cloneDeep(defaultRequiredValidator),
                    deferred = $q.defer();
                validation.error = [];

                prepareRequiredValidator(validation, defaultRequiredValidatorCP);
                // extend the validation configs with the validators on the field
                validatorsConfig = extendConfWithValidator(validation.validators);
                // push all promis functions in an array
                _.forEach(validatorsConfig, function(validatorConfig, index) {
                    promises.push(validatorConfig.expression(value, validatorConfig));
                });
                $q.all(promises).then(
                    function success(validatorConfig) {
                        isValid = true;
                        validFunc(element, scope, ctrl);
                        deferred.resolve(isValid);
                    },
                    function onError(validatorConfig) {
                        scope.message = validatorConfig.errorMessage ? validatorConfig.errorMessage : frValidationConfig.getErrorMessage(validatorConfig.type);
                        validation.error.push({
                            type: validatorConfig.type,
                            text: scope.message
                        });
                        // set error message
                        isValid = false;
                        inValidFunc(element, scope, ctrl);
                        deferred.reject(isValid);
                    }
                );

                return deferred.promise;
            };

            /**
             * @function
             * @name  prepareRequiredValidator --------------------------------------
             * @description - add a required validator to the validators lis when
             * the field is required and there isn't already defined one.
             * Remove the required validator when the field isn't required
             * @param  {Object} validation - validation object
             */
            function prepareRequiredValidator(validation, defaultRequiredValidator) {
                 if (validation.required ===  true) {
                    validation.validators = validation.validators || [];
                    var requiredValidatorAlreadyDefined = _.find(validation.validators, function(validator) { return validator.type === defaultRequiredValidator.type;});
                    if (validation.errorMessage && !requiredValidatorAlreadyDefined) {
                        defaultRequiredValidator.errorMessage = validation.errorMessage;
                    }
                    // chekc if there is a required validators
                    !requiredValidatorAlreadyDefined && validation.validators.push(defaultRequiredValidator);
                } else if (validation.required === false) {
                    var requiredValidatorIndex = _.findIndex(validation.validators, function(validator) {
                        return validator.type === defaultRequiredValidator.type;
                    });
                    if (requiredValidatorIndex > -1) {
                        validation.validators.splice(requiredValidatorIndex, 1);
                    }
                }
            }

            /**
             * @function
             * @name  extendConfWithValidators ------------------------------
             * @description extend validators config with the validators that
             * where set to the field
             * @param  {Array} validators - array of validator object
             * @return {Array} - extended array
             */
            function extendConfWithValidator(validators) {
                var validatorsConfig = [];
                _.forEach(validators, function(validator) {
                    if (frValidationConfig.getValidator(validator.type) === undefined) {
                        //throw 'there was no validator found with the type ' + validator.type;
                        // we return false because we only want to use the validators that we know about
                        return false;
                    }
                    var location = validatorsConfig.push(_.cloneDeep(frValidationConfig.getValidator(validator.type))); // make copy -> don't modify the config
                    _.assign(validatorsConfig[location -1], validator);
                });
                return validatorsConfig;
            }

            return {
                restrict: 'A',
                require: 'ngModel',
                scope: false,
                link: function frValidationLink(scope, element, attrs, ctrl) {

                    var model, validation;


                    /**
                     * @name initialize directive ------------------------
                     * @function
                     * @description check if all required attributes are available
                     * on the element
                     */
                    function initialize() {
                        if (!attrs.ngModel) {
                            console.error('The validation directive requires a ngModel attribute');
                            return false;
                        }
                        if (!attrs.validation) {
                            console.error('The validation directive requires a validation attribute');
                            return false;
                        }
                        model = $parse(attrs.ngModel);
                        validation = $parse(attrs.validation)(scope);
                        checkCallbacks();
                        watchModel();
                    }

                    /**
                     * @function
                     * @name  EVENT: validate_field_on_submit
                     */
                    scope.$on(config.enums.events.VALIDATE_ON_SUBMIT, function(event, fieldValidatorPromises) {
                        var value = ctrl.$modelValue;
                        var validatorPromise = checkValidators(scope, element, attrs, ctrl, validation, value);
                        validatorPromise.finally(function () {
                            scope.$emit(config.enums.events.FIELD_CHANGED, ctrl.$name);
                        });
                        fieldValidatorPromises.push(validatorPromise);
                    });

                    /**
                     * @function
                     * @name  EVENT: validate_field
                     */
                    scope.$on(config.enums.events.VALIDATE_FIELD, function validateField(event, fieldName, succes, error) {
                        if (ctrl.$name === fieldName) {
                            var value = ctrl.$viewValue;
                            var validatorPromise = checkValidators(scope, element, attrs, ctrl, validation, value);
                            validatorPromise.finally(function validationIsDone() {
                                scope.$emit(config.enums.events.FIELD_CHANGED, ctrl.$name);
                            });
                            validatorPromise.then(function validateSuccess(data) {
                                succes(data);
                            }, function validateError(data) {
                                error(data);
                            });
                        }
                    });

                    /**
                     * @name  checkCallbacks -----------------------------
                     * @function
                     * @description check if there are any callback defined
                     * on the attributes and make them available on the current
                     * scope
                     */
                    function checkCallbacks() {
                        if (attrs.validCallback) {
                            scope.validCallback =  $parse(attrs.validCallback);
                        }
                        if (attrs.invalidCallback) {
                            scope.invalidCallback = $parse(attrs.invalidCallback);
                        }
                    }

                    /**
                     * @name  watchModel --------------------------------
                     * @function
                     * @description watch the model and check if the model
                     * and check if the model is valid
                     */
                    function watchModel () {
                        scope.$watchCollection(model, function(value) {
                             // dirty, pristine, viewValue control here
                            // $pristine -> true when input is clean
                            if (ctrl.$pristine && !_.isEmpty(ctrl.$viewValue)) {
                                // model has value when initialized
                                ctrl.$setViewValue(ctrl.$viewValue);
                            } else if (ctrl.$pristine) {
                                // don't validate the form when the inut is clean
                                return;
                            }
                            // calling the validate function inside the controller
                            checkValidators(scope, element, attrs, ctrl, validation, value)
                                .finally(function () {
                                    scope.$emit(config.enums.events.FIELD_CHANGED, ctrl.$name);
                                });
                        });
                    }

                    initialize();

                }
            };
        }
    ]);
})(window.angular, window._);
;(function(ng) {
    'use strict';
    ng.module('formRenderer.validation').factory('frValidators', [
        '$q',
        'akit.DateService',
        function frValidatorsFactory($q, frDate) {

            /**
             * @name  Check required ------------------------------------
             * @function
             * @public
             * @param  {String} value - value inputfield
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkRequired(value, config) {
                var deferred = $q.defer();
                var result = true;

                if (value instanceof Array) {
                    if (value.length === 0) {
                        result = false;
                    }
                } else {
                    // 0 should be a valid value
                    result = value === 0 ? true : !!value;
                }

                result && deferred.resolve(config);
                !result && deferred.reject(config);

                return deferred.promise;
            }

            /**
             * @name  check match -------------------------------------
             * @description check if a value is matching a given pattern
             * The pattern can be a string or a RegExp javascript object
             * @function
             * @param  {String} value - value inputfield
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkMatch(value, config) {
                var regExp,
                    match,
                    deferred = $q.defer();

                if (!config.options) {
                    deferred.resolve(config);
                    return deferred.promise;
                }
                var pattern = config.options.pattern;
                if (pattern) {
                    // We can't check a regexp on a value when is is not set
                    // we skip the validator by reurning true
                    if (value === undefined || value === null || value === '') {
                        deferred.resolve(config);
                        return deferred.promise;
                    }
                    if (typeof value === 'object') {
                        console.warn('It is not possible to check a regExp on an object ' + JSON.stringify(value));
                        deferred.reject(config);
                        return deferred.promise;
                    }
                    if (typeof pattern === 'string') {
                        // if the pattern has a / on index 0
                        // remove it from the pattern
                        if (pattern.indexOf('\/') === 0) {
                            pattern = pattern.substring(1, pattern.length);
                        }
                        // if the pattern has a / on the last index
                        // remove it from the pattern
                        if (pattern.lastIndexOf('\/') === pattern.length - 1) {
                            pattern = pattern.substring(0, pattern.length - 1);
                        }
                        regExp = new RegExp(pattern);
                    }
                    else if (pattern instanceof RegExp) {
                        regExp = pattern;
                    }

                    match = String(value).match(regExp);
                    if ((match !== null)) {
                        deferred.resolve(config);
                    } else {
                        deferred.reject(config);
                    }
                    return deferred.promise;
                }
            }

            /**
             * Check Date --------------------------------------------
             * @description check if the date is valid
             * then the min/max properties
             *    compare: {
             *         addDays: '',
             *         Date: 'now/15/06/1988',
             *         operator: ''
             *      },
             *      before: '15/06/1988',
             *      after: '15/06/1988',
             *      hasPassed: true/false
             * @function
             * @param  {String} value - value inputfield
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkDate(value, config) {
                var result = false,
                    checked = false,
                    resultPassed,
                    deferred = $q.defer();

                if (!config.options) {
                    deferred.resolve(config);
                    return deferred.promise;
                }

                if (config.options.compare && !checked) {
                    checked = true;
                    result = checkDateCompare(value, config.options.compare);
                }

                if (config.options.before && !checked) {
                    checked = true;
                    result = frDate.isBefore(value, config.options.before);
                }

                if (config.options.after && !checked) {
                    checked = true;
                    result = frDate.isAfter(value, config.options.after);
                }

                if (config.options.hasPassed !== undefined && !checked) {
                    checked= true;
                    resultPassed = frDate.isPassed(value);
                    result = config.options.hasPassed ? resultPassed : !resultPassed;
                }

                if (result) {
                    deferred.resolve(config);
                } else {
                    deferred.reject(config);
                }
                return deferred.promise;
            }

            /**
             * @name  checkDateCompare ---------------------------------
             * @description Compare two dates with each other
             * @function
             * @param  {String} value - Date string
             * @param  {Objetc} compareObj - validation compare object
             * @return {Booelan}
             */
            function checkDateCompare(inputDate, compareObj) {
                // only check if there is a value
                if (inputDate === undefined || inputDate === null || inputDate === '') {
                    return true;
                }

                var compareDate, result = false;
                var options = {
                    addDays: parseInt(compareObj.addDays),
                    date: compareObj.date || 'now',
                    operator: compareObj.operator || '='
                };
                // parse a datestring to a JS object without time information -> if it fails return false
                compareDate = frDate.parseToDate(inputDate) ? frDate.parseDateYYMMDD(frDate.parseToDate(inputDate)) : false;
                if (options.date === 'now') {
                    options.date = frDate.parseDateYYMMDD(new Date(Date.now()));
                } else {
                    options.date = frDate.parseToDate(options.date) ? frDate.parseDateYYMMDD(frDate.parseToDate(options.date)) : false;
                }

                if (!compareDate || !options.date) {
                    return false;
                }

                if (options.addDays && frDate.validateDate(options.date)) {
                    options.date = new Date(options.date.getTime() + options.addDays * 24 * 60 * 60 * 1000);
                    options.date = frDate.parseDateYYMMDD(options.date);
                }

                switch (options.operator) {
                    case '>' :
                        result = (compareDate > options.date);
                        break;
                    case '>=' :
                        result = (compareDate >= options.date);
                        break;
                    case '<' :
                        result = (compareDate < options.date);
                        break;
                    case '<=' :
                        result = (compareDate <= options.date);
                        break;
                    case '=' :
                        // http://stackoverflow.com/questions/7606798/javascript-date-object-comparison
                        // comparing two objets in never the same, therefor we wrap them in a number function
                        result = (Number(compareDate) === Number(options.date));
                        break;
                }
                return result;
            }

            /**
             * Check Length --------------------------------------------
             * @description check if a value length is not smaler or higher
             * then the min/max properties
             * @function
             * @param  {String} value - value inputfield
             * @param  {Object} scope - the current scope
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkLength(value, config) {
                var min,
                    max,
                    length,
                    deferred = $q.defer();

                if (!config.options) {
                    deferred.resolve(config);
                    return deferred.promise;
                }

                min = config.options.min !== undefined ? config.options.min : 0;
                max = config.options.max !== undefined ? config.options.max : Number.POSITIVE_INFINITY;
                length = value ===  null ? 0 :value.length;

                if (length >= min && length <= max) {
                    deferred.resolve(config);
                } else {
                    deferred.reject(config);
                }
                return deferred.promise;
            }

            /**
             * @name  checkBetween -----------------------------------
             * @description Check if a given value is between a min
             * and max value. if min is undefined it will be set to zero
             * @function
             * @param  {String} value - value inputfield
             * @param  {Object} config - validation config
             * @return {Boolean}
             */
            function checkBetween(value, config) {
                var deferred = $q.defer();
                if (value === undefined || value === null || value === '') {
                    deferred.resolve(config);
                }
                if (!config.options) {
                    deferred.resolve(config);
                    return deferred.promise;
                }

                var min = config.options.min !== undefined ? config.options.min : 0;
                var max = config.options.max !== undefined ? config.options.max : Number.POSITIVE_INFINITY;
                if (max === Number.POSITIVE_INFINITY) {
                    if ((parseInt(value, 10) >= parseInt(min, 10) && parseInt(value, 10) <= max)) {
                        deferred.resolve(config);
                    } else {
                        deferred.reject(config);
                    }
                } else {
                    if ((parseInt(value, 10) >= parseInt(min, 10) && parseInt(value, 10) <= parseInt(max, 10))) {
                        deferred.resolve(config);
                    } else {
                        deferred.reject(config);
                    }
                }
                return deferred.promise;
            }

            /**
             * @name  Get default validators ---------------------------
             * @function
             * @return {Array} Return all validator defenitions
             */
            this.getDefaultValidators = function getDefaultValidators() {
                return DefaultValidators;
            };

             /**
             * Default validators
             * @type {Array}
             */
            var DefaultValidators = [
                {
                    type: 'required',
                    expression: checkRequired,
                    defaultMessage: 'Dit veld is verplicht'
                },
                {
                    type: 'regexp',
                    expression: checkMatch,
                    defaultMessage: 'Dit veld is niet correct'
                },
                {
                    type: 'length',
                    expression: checkLength,
                    defaultMessage: 'dit veld heeft te weinig karakters'
                },
                {
                    type: 'number',
                    expression: checkMatch,
                    options: {
                        pattern: /^[0-9]+$/
                    },
                    defaultMessage: 'dit veld is geen nummer'
                },
                {
                    type: 'url',
                    expression: checkMatch,
                    options: {
                        pattern: /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
                    },
                    defaultMessage: 'vul een geldig url formaat in'
                },
                {
                    type: 'email',
                    expression: checkMatch,
                    options: {
                        pattern : /\S+@\S+\.\S+/
                    },
                    defaultMessage: 'vul een geldig email adres in'
                },
                {
                    type: 'between',
                    expression: checkBetween,
                    defaultMessage: 'De input moet zich tussen twee waarden bevinden'
                },
                {
                    type: 'date',
                    expression: checkDate,
                    defaultMessage: 'De datum is niet geldig'
                }
            ];

            // @type {Object}
            return {
                getDefaultValidators: this.getDefaultValidators
            };
        }
    ]);
})(window.angular);
;(function(ng, _) {
    'use strict';
    ng.module('formRenderer.validation').provider('frValidationConfig', [
        function frValidationConfigWrapper() {

            /**
             * Define validation expressions and
             * default messages
             * @type {Object}
             */
            var validators = {};

            /**
             * @function
             * @author Glenn Verschooren
             * @name add validator ---------------------------------
             * @description Allow user to set a custom validator and
             * default error message
             * @param {object/array} data - validator object
             */
            this.setValidator = function setValidator(data) {
                if (_.isArray(data)) {
                    var allValidators = [];
                    _.forEach(data, function(item) {
                        allValidators.push(setValidator(item));
                    });
                    return allValidators;
                } else if (_.isPlainObject(data)) {
                    // check required fields
                    if (checkRequiredProperties(data)) {
                        validators[data.type] = data;
                        return validators[data.type];
                    }
                } else {
                    throw "You must provide an object or array to add a validator:" + JSON.stringify(arguments);
                }
            };

            /**
             * @function
             * @author Glenn Verschooren
             * @name  check required properties ---------------------
             * @description check if all properties are there
             * @param  {Object} data - validator object
             * @return {Boolan}
             */
            function checkRequiredProperties(data) {
                var isValidObject = !!(data.type && data.expression && data.defaultMessage);
                if (!isValidObject) {
                    throw 'You must provide all required properties when creating a new validator. ' +
                    'type: ' + data.type + ', expression: ' + data.expression + ', defaultMessage: ' + data.defaultMessage;
                }
                return isValidObject;
            }

            /**
             * Public api -----------------------------------------------
             * @function
             * This function will be executed in the run phase
             */
            this.$get = function($http, $templateCache, $compile, frValidators) {

                // include all default validators
                this.setValidator(frValidators.getDefaultValidators());

                /**
                 * @function
                 * @author Glenn Verschooren
                 * @name  check form valid ----------------------------
                 * @description Check if a form is valid
                 * @param  {Object} form - angular form object
                 * @return {Boolean}
                 */
                this.checkFormValid = function checkFormValid(form) {
                    if (form.$valid === undefined) {
                        return false;
                    }
                    return (form && form.$valid === true);
                };

                /**
                 * @function
                 * @author Glenn Verschooren
                 * @name  get error message ----------------------------
                 * @description If there isn't a errorMessage we return the default message
                 * @param  {String} name - validator name
                 * @return {String}  - Error message
                 */
                this.getErrorMessage = function getErrorMessage(type) {
                    var validator = validators[type];
                    if (validator && !validator.errorMessage) {
                        return validator.defaultMessage;
                    } else {
                        return validator.errorMessage;
                    }
                };

                /**
                 * @function
                 * @author Glenn Verschooren
                 * @name  get validator ----------------------------------
                 * @description get the validator by type
                 * @param  {String} type - type of the validator
                 * @return {Object} - validator object
                 */
                this.getValidator = function getValidator(type) {
                    return validators[type];
                };

                /*
                    Public api -------------------------------------------
                    @type {Object}
                 */
                return {
                    getValidator: this.getValidator,
                    setValidator: this.setValidator,
                    getErrorMessage: this.getErrorMessage
                };

            };

        }
    ]);

})(window.angular, window._);